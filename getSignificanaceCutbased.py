#!/usr/bin/env python
#
#
#
#  Created by Jay Chan
#
#  8.22.2018
#
#  Modified from ttHyy XGBosst anaylsis codes
#
#
#
#
import os
import ROOT
from argparse import ArgumentParser
from math import sqrt, log
import math
from monohbbml.parameters import *
import pandas as pd
import numpy as np

ROOT.gROOT.SetBatch(True)

def getArgs():
    """Get arguments from command line."""
    parser = ArgumentParser()
    #parser.add_argument('-r', '--region', action='store', choices=['m', 'r'], default='r', help='Region to process')
    parser.add_argument('--From', action='store')
    parser.add_argument('--Saveas',action='store',default='cutbased_sig')
    return  parser.parse_args()

def calc_sig(sig, bkg,s_err,b_err):
    ntot = sig + bkg
    if(sig <= 0): return 0, 0
    if(bkg <= 0): return 0, 0
    significance = sig/sqrt(bkg)
    #error on significance
    numer = sqrt((log(ntot/bkg)*s_err)**2 + ((log(1+(sig/bkg)) - (sig/bkg))*b_err)**2)
    uncert = (numer/significance)

    return significance, uncert

def sum_z(zs,us):
    sumu=0
    sumz=0
    for i in range(len(zs)):
        sumz+=zs[i]**2
        sumu+=(zs[i]*us[i])**2
    sumz=sqrt(sumz)
    sumu=sqrt(sumu)/sumz
    return sumz,sumu
        

def GetSigmas(inDict, inFile):
    
    BRHbb=0.571
    lines = open(inFile,"r").readlines()
    for line in [l for l in lines if l[0]!='#']:
        if not '2HDMa' in line:
            mzp   = line.split()[1]
            ma    = line.split()[2]
            sigma = float(line.split()[5])
            inDict['zp2hdmbbmzp'+mzp+"mA"+ma]=float(sigma)*BRHbb
        else:
            if '2HDMa_ggF' in line:
                name  = line.split()[1].replace('MGPy8EG_A14N30LO_', '').replace('monoHbb_', '')
            elif '2HDMa_bb' in line:
                name  = line.split()[1].replace('MGPy8EG_A14N30LO_', '').replace('monoHbb_', '')
            else: continue
            sigma = float(line.split()[2])
            fe    = float(line.split()[3])
            inDict[name] = sigma * fe
    return inDict

def gettingsig(CrossSections):
    args=getArgs()
    inputname=args.From
    if not inputname.endswith('.h5'): inputname+='.h5'
    print 'Evaluating significances for all of the signal models...'
    
    bkg=pd.read_hdf('data/yields/'+inputname,key='mbb_100_140/bkg').set_index('category')
    sig=pd.read_hdf('data/yields/'+inputname,key='mbb_100_140/sig')
    sig=sig[sig['category'].isin(CrossSections.keys())]
    significance=pd.DataFrame()
    significance['category']=sig['category']
    for region in list(sig.columns):
        if region=='category': continue
        significance[region]=sig.apply(lambda x: calc_sig( x[region] * CrossSections[x.category] , bkg.at['bkg_tot', region] ,sqrt(x[region] * CrossSections[x.category]) , sqrt(bkg.at['bkg_tot', region]))[0] , axis=1, result_type='expand')
    significance['Resolved']=significance.apply(lambda x: sqrt(np.sum([x[key]**2 for key in significance.keys() if 'Resolved' in key])), axis = 1)
    significance['Combined']=significance.apply(lambda x: sqrt(x.Merged ** 2 + x.Resolved ** 2), axis=1,result_type='expand')
    print significance.to_string()
    return significance
    
def main():
    args = getArgs()
    name = args.Saveas + '.csv' if '.csv' not in args.Saveas else args.Saveas
    CrossSections = GetSigmas({}, "data/sigma_FixedHiggsesTo300_08052017.txt")
    CrossSections = GetSigmas(CrossSections, "data/sigma_2HDMa.txt")

    significance=gettingsig(CrossSections).reset_index()
    if not os.path.isdir('significances'): os.mkdir('significances')

    significance.to_csv('significances/'+name, sep = ' ', index=False)

    return

if __name__ == '__main__':
    main()
