# Welcome to MonoHbbML

MonoHbbML is a package that can be used to study the XGBoost technique in Mono-H(bb) analysis.

## Setup

### First time setup on lxplus

First checkout the code:

```
git clone ssh://git@gitlab.cern.ch:7999/wisc_atlas/MonoHbbML.git [-b your_branch]
cd MonoHbbML
```

Then, setup the virtualenv:

```
setupATLAS
lsetup "root  6.12.06-x86_64-slc6-gcc62-opt"
lsetup python
virtualenv --python=python2.7 ve
source ve/bin/activate
```

Then, checkout necessary packages:

```
pip install pip --upgrade
pip install theano keras h5py sklearn matplotlib tabulate xgboost uproot root_pandas tables
pip install --upgrade https://github.com/rootpy/root_numpy/zipball/master
```

If this is the first time you are using keras, you will want to change the backend to theano instead of the default tensorflow.
To do this, edit the appropriate line in `~/.keras/keras.json`.

### Normal setup on lxplus

After setting up the environment for the first time, you can return to this setup by doing `source setup_lxplus.sh`

## Scripts to run the tasks

### Producing training inputs and reduced root-files which the trained model will be applied to

The core script is `process_arrays.py`. The script will apply the given preselection cuts to input samples and produce corresponding ntuples, numpy arrays files, and a text file containing the information of averaged weight of the sample. You can directly run it for any specified input files. Since there are too many samples in Mono-H(bb), it is more convinient and time-efficient to use `SubmitProcessArrays.py` which will find all of the input files and run the core script by submitting the condor jobs.


```
python SubmitProcessArrays.py
```
or
```
python process_arrays.py [-n INPUT_FILE_NAME] [-r REGION] [-v] [-t] [-c CATEGORY]

Note:
  -n, --name          The name of the input files containers. The mother folder ('inputs/') should not be included in the name.
e.g. group.phys-exotics.mc16_13TeV.364222.Znunu_Sh221_PTV.0L0700a0.EXOT24.e5626_e5984_s3126_r9364_r9315_p3563_XAMPP/group.phys-exotics.14337810.XAMPP._000001.root
  -r, --region        The region of interest. 'm' for merged region. 'r_350_500' for the first resolved region. 'r_200_350' for the second resolved. 'r_150_200' for the last resolved.
  -v, --val           Do validation sample.
  -t, --train         Do training sample.
  -c, --category      Define the category that the input sample belongs to. This will put the output files of every sample that has the sample category into the same container.
```

### Check if the training inpnuts and reduced root-files are complete

The script `CheckArraysComplete.py` will check if all of the outputs from last step exist.

```
python CheckArraysComplete.py
```

### Recover the missing files and bad files

There are several stages where you will need to recover the files. One is when there are missing files. Others are when there are bad files that can't be read. Both cases can be addressed by running `RecoverMissingFiles.py`.

After checking the completeness, if there are missing files:

```
python RecoverMissingFiles.py -m
```

When running the training scripts later on, errors might occur due to some bad files that cannot be loaded. When this happens, do the following to recover the bad files:

```
python RecoverMissingFiles.py -b
```
Update: Now the later scripts are able to automatically call the recovering script to recover the bad files, so there might not be need anymore for doing the above command by hand.

### Start XGBoost analysis!

The whole ML task consists of training, applying the weights, plotting BDT distribution, optimizing the BDT boundaries for categorization, and calculating the number counting significances. `doTrain.sh` is a wapper script that will run the task from training, applying the BDT models, optimizing the boundaries, and calculate the number counting significances.

```
sh doTrain.sh

```
#### Training a model

```
python train_bdt.py [-r REGION] [-m mZP_1 mA_1 mZP_2 mA_2 ...] [--save]
```
#### Applying the weights

Applying the trained model to the reduced ntuples to get BDT scores for each event can be done by doing:
```
python applyBDTWeight.py [-r REGION] [-m mZP_1 mA_1 mZP_2 mA_2 ...]
```
#### Optimizing the BDT boundaries

```
python categorization.py [-r REGION] [-m Train_Model_mZP_1 Train_Model_mA_1 Train_Model_mZP_2 Train_Model_mA_2 ...] [-s Target_Signal_mZP Target_Signal_mA]
```

#### Optimizing the MET bins (instead of BDT bins)

```
python categorization_MET.py [-r REGION] [-s Target_Signal_mZP Target_Signal_mA]
```

### Make a combined significance plot

You would need to modify to codes to use different models and BDT boundaries for each region.
```
python plotCombine.py
```
