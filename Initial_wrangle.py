#  Created by Tuan Pham
#  July 26th 2019
import os
import math
from argparse import ArgumentParser
import numpy as np
from time import time, sleep
import uproot
import pandas as pd
from root_pandas import *
from monohbbml.parameters import *
from array import array

def getArgs():
    parser = ArgumentParser(description="Process input rootfiles into numpy arrays for MonoHbb XGBoost analysis.")
    parser.add_argument('--From', action='store', default='inputs', help='Directory that contains ntuples.')
    parser.add_argument('--Saveto',action='store',default='Preselected_samples',help='Name of directory to save output files')
    parser.add_argument('--test', action='store_true',default=False)
    parser.add_argument('--type', choices=['ZP2HDM', 'A2HDM','Z','W','ttbar','VH','diboson','stop','data'], default=['Z','W','ttbar','VH','diboson','stop', 'ZP2HDM', 'A2HDM'], action='store', nargs='*')
    return  parser.parse_args()

def preselection(Data,region):
    # preselection. Drop all rows in the dataframe that don't satisfy conditional tests. Keys: ~ = not, & = and, | = or. Consult https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#boolean-indexing for details
    if region=='Resolved':
        data=Data[Data.N_BJets_04>2]
        data=data[
        ((data.Jet_BLight1BPt >= 45000.) | (data.Jet_BLight2BPt >= 45000.)) &
        (data.IsMETTrigPassed!=0) &
        (data.MetTST_met>150000.) &
        (data.MetTST_met<500000.) &
        (data.N_BaselineElectrons == 0) &
        (data.N_SignalElectrons == 0 ) &
        (data.N_BaselineMuons == 0) &
        (data.N_SignalMuons == 0) &
        (data.N_Jets04 >=2) &
        (data.m_jj > 50000.) &
        (data.m_jj < 280000.) &
        (data.N_SignalTaus ==0) &
        (data.N_TausExtended_Resolved ==0) &
        (data.DeltaRJJ <1.8) &
        (((data.N_Jets04==2) & (data.sigjet012ptsum > 120000.)) | ((data.N_Jets04>2) & (data.sigjet012ptsum>150000.))) &
        (abs(data.DeltaPhiMin3) >= math.pi/9) &
        (abs(data.DeltaPhiJJ) <= math.pi*7/9 ) &
        (abs(data.DeltaPhiMetJJ) >= math.pi*2/3  ) &
        (data.MetTST_Significance_noPUJets_noSoftTerm >= 12 )
        ]
    else:
        data=Data[
        (Data.N_BaselineElectrons==0) &
        (Data.N_SignalElectrons==0) ]
        data=data[(data.N_BaselineMuons==0) &
        (data.N_SignalMuons==0) &
        (data.TrackJet_1isBjet) & 
        (data.TrackJet_2isBjet) &
        (data.MetTST_met >  500000.) &
        (data.m_J>50000.) &
        (data.m_J<270000.) &
        (data.IsMETTrigPassed != 0) &
        (data.N_associated_Jets02 >= 2) &
        (data.N_BTags_not_associated_02 > 0) &
        (data.N_not_associated_Taus == 0) &
        (data.N_TausExtended_Merged == 0) &
        (abs(data.DeltaPhiMin3) >= math.pi/9) &
        (data.TrackJet_1passOR == 1) &
        (data.TrackJet_2passOR == 1)
        ]
    return data

def get_SumW(meta_data_tree):
    # takes in a dataframe (data_frame) and a meta_data_tree, the sample's DSID and container of sample
    # get sum of weight from TotalSumW tree
    TotalSumW=meta_data_tree.array('TotalSumW')
    SumW=np.sum(TotalSumW)
    return SumW

def get_weight(SumW,data_frame,DSID,container,region):

    # get cross section, k-factor and filter efficiency
    xs=1
    kf=1
    fe=1
    if 'zp2hdmbb' in container or '2HDMa' in container: pass
    else:
        xsectiontxt=open('data/PMGxsecDB_mc16_signal_bkg.txt','r')
        lines = xsectiontxt.read().splitlines()
        found=False
        for line in lines:
            if line.startswith(DSID):
                xs=float(line.split()[2])
                kf=float(line.split()[4])
                fe=float(line.split()[3])
                found=True
        if not found:
            print('Cant find DSID in PMGxsecDB_mc16_signal_bkg.txt. Assign xs, kf, fe to 1.')

    # compute weight for dataset from SumW, kf, fe, xs
    if region=='Resolved':
        weight=data_frame.GenWeight * data_frame.GenWeightMCSampleMerging * data_frame.EleWeight * data_frame.MuoWeight * data_frame.TauWeight * data_frame.JetWeightJVT * data_frame.JetWeightBTag * data_frame.MET_TriggerSF * xs * kf * fe / SumW
    else:
        weight=data_frame.GenWeight * data_frame.GenWeightMCSampleMerging * data_frame.EleWeight * data_frame.MuoWeight * data_frame.TauWeight * data_frame.TrackJetWeight * data_frame.MET_TriggerSF * xs * kf * fe / SumW

    #print 'INFO: Cross-section:     ', xs
    #print 'INFO: k-factor:          ', kf
    #print 'INFO: Filter Efficiency: ', fe
    #print 'Sum of weights:          ', SumW

    return weight

def CheckEmpty(root, branch):
    # both uproot and root_pandas don't like reading from empty TTrees. Check if the 'MonoH_Nominal' branch is empty before attempting to open them
    f = ROOT.TFile(root)
    t = f.Get(branch)
    if t.GetEntries() == 0: empty = True
    else: empty = False
    return empty

def process_roots(inputdir, isdata, test, category):
    '''Processing all root files in one category'''
    # get a dictionary of input container to category
    category_dict=Data_sample_to_category_dict(inputdir)
    processing_list = {}
    # sort the unput containers into groups: mc16a, mc16d, mc16e and produce a dictionary of groups
    for container in category_dict.keys():
        if category_dict[container]==category:
            group=Root_name(container).split('_')[0]
            if group not in processing_list.keys(): processing_list[group]=[]
            processing_list[group].append(container)

    for group in processing_list.keys():
        # create an empty dataframes for resolved and merged
        data_resolved=pd.DataFrame()
        data_merged=pd.DataFrame()
        Start=time()
        for container in sorted(processing_list[group]):
            start=time()
            print 'Processing %s.' % container
            if not os.path.isdir(inputdir+"/"+container):
                raise SystemExit("ERROR: The input file %s doesn\'t exist!! Exiting!" % (inputdir+"/"+container))
            temp_data_resolved=pd.DataFrame()
            temp_data_merged=pd.DataFrame()
            # get data sample ID
            DSID=Data_sample_ID(container)

            # Open input files, convert to dataframes, compute weights and concatenate
            SumW=0
            ntuples=sorted([ntuple for ntuple in os.listdir(inputdir+"/"+container) if ntuple.endswith('.root')])
            for ntuple in ntuples:
                print('Opening %s' % inputdir + "/" + container + '/' + ntuple)
                emptyfile=CheckEmpty(inputdir + "/" + container + '/' + ntuple, 'MonoH_Nominal')
                if not isdata:
                    # get MetaDataTree
                    meta_data_tree=uproot.open(inputdir+"/"+container+'/'+ntuple)['MetaDataTree']
                    # compute the sum of weight.
                    SumW+=get_SumW(meta_data_tree=meta_data_tree)
                # open input file with root_pandas
                if emptyfile: continue
                roots = read_root(inputdir+"/"+container+'/'+ntuple,key='MonoH_Nominal',chunksize=500000)
                # apply preselections by submitting the primary dataframe (data) to preselection module in resolved and merged regions
                for root in roots:
                    if not isdata and int(DSID) <= 364197 and int(DSID) >= 364100: root = root[abs(root.GenWeight) < 100.]
                    root_resolved=preselection(root,'Resolved')
                    root_merged=preselection(root,'Merged')

                    # concatenate primary dataframes (temp_data) with current dataframe (root) if the latter is non-empty
                    if not root_resolved.empty: temp_data_resolved=pd.concat([temp_data_resolved,root_resolved],ignore_index=True)
                    if not root_merged.empty: temp_data_merged=pd.concat([temp_data_merged,root_merged],ignore_index=True)

            if not isdata:
                # compute weight for each entry if the temporary data frame is non-empty
                if not temp_data_resolved.empty:
                    temp_weight_resolved = get_weight(SumW=SumW, data_frame=temp_data_resolved, DSID=DSID, container=container, region='Resolved')
                    temp_data_resolved = temp_data_resolved.assign(weight=temp_weight_resolved)
                if not temp_data_merged.empty:
                    temp_weight_merged = get_weight(SumW=SumW, data_frame=temp_data_merged, DSID=DSID, container=container, region='Merged')
                    temp_data_merged = temp_data_merged.assign(weight=temp_weight_merged)

            # append temp_data to data if the former is non-empty
            if not temp_data_resolved.empty: data_resolved=pd.concat([data_resolved,temp_data_resolved],ignore_index=True)
            if not temp_data_merged.empty: data_merged=pd.concat([data_merged,temp_data_merged],ignore_index=True)
            # number of events passing preslections
            print('Current event yield after preselection: %d in merged and %d in resolved region' % (data_merged.shape[0],data_resolved.shape[0]))
            print 'Finishing %s in %d seconds' % (container,time()-start)
            print '.'*100

        event_yield_merged = data_merged.shape[0]
        event_yield_resolved = data_resolved.shape[0]

        # add a column called 'MyeventNumber' which will be used to identify row in later steps
        data_resolved = data_resolved.assign(MyeventNumber=np.arange(event_yield_resolved))
        data_merged = data_merged.assign(MyeventNumber=np.arange(event_yield_merged))
        #print 'sumw: ', data_resolved.weight.sum()

        # split to single-entry variables and vector-like variables. to_root does not support saving the latter. The list of these vector-like variables is located in parameters
        print ''
        print 'Separating data into single-entry and vector-like variables.'
        Vector_variables=set(vector_variables['samples']).intersection(set(data_resolved.keys())) if not 'data' in category else set(vector_variables['data']).intersection(set(data_resolved.keys()))
        data_resolved_vector=data_resolved[Vector_variables|{'MyeventNumber'}]
        data_merged_vector=data_merged[Vector_variables|{'MyeventNumber'}]

        # drop the vector-like variables from main frames.
        data_resolved=data_resolved.drop(columns=Vector_variables)
        data_merged=data_merged.drop(columns=Vector_variables)

        print ''
        print 'Finishing %s in %d seconds' % (group,time()-Start)
        processing_list[group]={}
        processing_list[group]['Resolved']=[data_resolved,data_resolved_vector]
        processing_list[group]['Merged']=[data_merged,data_merged_vector]
        print '*' * 80

    return processing_list

def main():

    args=getArgs()
    inputdir,types,test,savedir=args.From,args.type,args.test,args.Saveto
    count=0
    start=time()
    category_dict=Data_sample_to_category_dict(inputdir)
    Categories=set([category_dict[container] for container in category_dict.keys()])
    
    for type in types:
        if type=='ZP2HDM':
            categories=[cat for cat in Categories if 'zp2hdmbbmzp' in cat]
        elif type=='A2HDM':
            categories=[cat for cat in Categories if '2HDMa' in cat]
        else:
            categories=[type]
        for category in categories:
            count+=1
            print "========================================================="
            print "Processing %s" % category
            print "========================================================="
            savepath=savedir+'/'+category
            hdfname=savedir+'/'+'Vector_like.h5'
            processed_datas = process_roots(inputdir=inputdir,isdata=True if category=='data' else False,test=test,category=category)
            print 'Saving processed data...'
            if not os.path.isdir(savepath):
                os.makedirs(savepath)
            # Since both uproot and root_pandas don't like reading empty root files, we save empty dataframes with an 'empty' tag to avoid reading.
            for group in processed_datas:
                rootname = savepath + '/' + category + '_' + group + '.root'
                rootname_empty = rootname.replace('.root', '_empty.root')
                if os.path.isfile(rootname): os.remove(rootname)
                if os.path.isfile(rootname_empty): os.remove(rootname_empty)
                for region in processed_datas[group]:
                    processed_data = processed_datas[group][region][0]
                    processed_data_vector = processed_datas[group][region][1]
                    saved = False
                    # if process_data is empty: Fill with np.nan and save with an 'empty' tag. Don't save to HDF file. mode = 'w' to create a file and 'a' to append to current file.
                    if processed_data.empty:
                        processed_data = pd.DataFrame(data=np.nan, columns=processed_data.keys(), index=[0])
                        processed_data.to_root(path=rootname_empty, key=region, mode='w' if not os.path.isfile(rootname_empty) else 'a')
                        saved = True
                    else:
                        processed_data.to_root(rootname,key=region,mode='a' if os.path.isfile(rootname) else 'w')
                    hdfkey = region + '/' + category + '/' + group
                    # HDF file is quite funny. We can't have multiple pipelines operating on it. If we run the script in parallel to save time, we want to avoid error when HDF5 is open by another script. so if it can't save the dataframe, the programme will wait for 15s and attempt again. 
                    while not saved :
                        try:
                            print 'Attempting to save %s to HDF.' % hdfkey
                            processed_data_vector.to_hdf(hdfname,key=hdfkey,mode='a')
                            print '*** Finish saving %s ***' % hdfkey
                            saved=True
                        except:
                            print 'HDF5 file is being open, saving %s in 15s.' % hdfkey
                            sleep(15)

            stop=time()
            print 'Time elapsed: %d' % (stop-start)
            print '%d categories processed.' % count

    print ""
    print "========================================================="
    print "Finishing the process. %d categories processed over %d seconds." % (count,time()-start)
    print "========================================================="

    return

if __name__ == '__main__':
    main()
