import os
from argparse import ArgumentParser

parser=ArgumentParser()
parser.add_argument('-m','--recovermissing', action='store_true', default=False, help='Recover the missing files.')
parser.add_argument('-b','--recoverbad', action='store_true', default=False, help='Recover the bad files.')
args=parser.parse_args()

def recover(recoverlist):
    text_file = open(recoverlist, "r")
    lines = text_file.read().splitlines()
    enumerator=0
    for line in lines:
        arg=str(line).split(' ')
        print "python test_new_process.py -n %s/%s -c %s" % (arg[0],arg[1],arg[2])
        os.system("python test_new_process.py -n %s/%s -c %s %s" % (arg[0],arg[1],arg[2],'-d' if 'data' in arg[4] else ''))
        enumerator+=1
        print('Recovered %d files' % enumerator)


if args.recovermissing:
    print "Recovering missing files..."
    recover('arrays/missing_samplelist.txt')
    print "All missing samples have been recovered. Will delete the missing samplelist."
    os.remove('arrays/missing_samplelist.txt')
if args.recoverbad:
    print "Recovering bad files..."
    recover('arrays/badsamplelist.txt')
    print "All missing samples have been recovered. Will delete the bad samplelist."
    os.remove('arrays/badsamplelist.txt')
if not (args.recovermissing or args.recoverbad):
    print "Recovering missing files..."
    recover('arrays/missing_samplelist.txt')
    print "All missing samples have been recovered. Will delete the missing samplelist."
    os.remove('arrays/missing_samplelist.txt')
    print "Recovering bad files..."
    recover('arrays/badsamplelist.txt')
    print "All missing samples have been recovered. Will delete the bad samplelist."
    os.remove('arrays/badsamplelist.txt')


