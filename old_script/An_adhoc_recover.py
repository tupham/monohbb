import os
import pandas as pd
import Initial_wrangle
from monohbbml.parameters import *

def main():
    #os.system('mv Preselected_samples Preselected_samples_store')
    #os.makedirs('Preselected_samples')
    category_dict=Data_sample_to_output_name_dict('inputs')
    output_dict={}
    hdf=pd.HDFStore('Preselected_samples_store/Vector_like.h5')
    new_hdf=pd.HDFStore('Preselected_samples/Vector_like.h5')
    keys=hdf.keys()
    print len(keys)
    for key in category_dict.keys(): output_dict[category_dict[key]]=key
    for category in [dir for dir in os.listdir('Preselected_samples_store') if os.path.isdir('Preselected_samples_store'+'/'+dir)]:
        if not os.path.isdir('Preselected_samples/'+category): os.makedirs('Preselected_samples/'+category)
        cat_dir='Preselected_samples_store/'+category
        for root in [f for f in os.listdir(cat_dir) if f.endswith('.root')]:
            key='Resolved'+'/'+category+'/'+root.replace('.root','')
            if key in keys:
                print key
                os.system('mv %s %s' % (cat_dir+'/'+root, 'Preselected_samples/'+category+'/'+root ) )
                new_hdf.put(key,hdf.get(key))
            else:
                print key
                container=output_dict[root]
                Initial_wrangle.process_arrays(container=container,inputdir='inputs',test=False,isdata=True if category == 'data' else False, category=category)
    hdf.close()
    new_hdf.close()

if __name__ == '__main__':
    main()
