#!/usr/bin/env python
import os
from argparse import ArgumentParser
#import json
#import traceback
#import math
import uproot
#from root_numpy import root2array
import numpy as np
#import pickle
#from sklearn import model_selection
from sklearn.metrics import roc_curve, auc, confusion_matrix
#from sklearn.model_selection import train_test_split
#from keras.optimizers import RMSprop
#from keras.callbacks import EarlyStopping, ModelCheckpoint
#from sklearn.preprocessing import StandardScaler
import xgboost as xgb
from tabulate import tabulate
#from bayes_opt import BayesianOptimization
import matplotlib.pyplot as plt

def getArgs():
    """Get arguments from command line."""
    parser = ArgumentParser()
    parser.add_argument('-r', '--region', action='store', choices=['m', 'm_500_900', 'm_900', 'r'], default='r', help='Region to process')
    parser.add_argument('--cat', '--categorical', action='store_true', help='Create categorical model')
    parser.add_argument('-s', '--signal', action='store', type=signal_multiplier, default='5', help='Number of signal events to use in training as a multiple of the number of background events. Value of 0 means use all signal events.')
    parser.add_argument('-n', '--name', action='store', default='test', help='Name of output plot.')
    parser.add_argument('-p', '--params', action='store', type=dict, default=None, help='json string.') #type=json.loads
    parser.add_argument('--numRound', action='store', type=int, default=10000, help='Number of rounds.')
    parser.add_argument('--roc', action='store_true', help='Plot ROC')
    parser.add_argument('--save', action='store_true', help='Save model weights to HDF5 file')
    parser.add_argument('--ZPtHDM', type=int, nargs='+', default = [], help='Masspoint of training ZP2HDM model samples.')
    parser.add_argument('--tHDMa_ggF', type=int, nargs='+', default = [], help='Masspoint of training 2HDM+a model samples (with ggF production).')
    parser.add_argument('--tHDMa_bb', type=int, nargs='+', default = [], help='Masspoint of training 2HDM+a model samples (with bb production).')
    parser.add_argument('--sf',type=float,default=1.,help='Scale factor of signal to make the signal and background effectively same in size.')
    parser.add_argument('--feature',action='store_true',default=False,help='Plot feature importance')
    parser.add_argument('--featureonly', action='store_true', default=False, help='Only plot feature importance')
    return parser.parse_args()

def signal_multiplier(s):
    s = float(s)
    if s < 0.0:
        raise argparse.ArgumentTypeError('%r must be >= 0!' % s)
    return s


def process_features(region,masspoints,type='weight',show=False):

    print '============ Plotting features ============\n'
    sig = ''
    for i in range(0, len(masspoints), 2):
        mA = masspoints[i]
        mZP = masspoints[i + 1]
        sig += 'zp2hdmbbmzp%smA%s_' % (mA, mZP)

    model = xgb.Booster()
    model.load_model( 'models/%s%s.h5' % (sig, region))

    xgb.plot_importance(booster=model, importance_type=type)

    # plt.rcdefaults()
    fig, ax = plt.subplots()

    importances = model.get_score(importance_type=type).values()
    names = model.get_score(importance_type=type).keys()
    print model.get_score(importance_type=type).items()

    names = [s.strip('f') for s in names]
    names = [int(i) for i in names]

    print len(importances), " features used to make BDT cuts"

    if region == 'resolved':
        real_names = [
            # "jet1_pt", "jet1_eta", "jet1_phi", "jet1_E",
            # "jet2_pt", "jet2_eta", "jet2_phi", "jet2_E",
            "dijet_pt", "dijet_eta",

            # "jet3_pt", "jet3_eta", "jet3_phi", "jet3_E",
            # "jet4_pt", "jet4_eta", "jet4_phi", "jet4_E",
            "met", "delta_phi", "n_j" #"DeltaRjj"
            , "HT", "pseudo_score_j1",

                "pseudo_score_j2"
        ]
    else:
        real_names = [
            "met", "met_phi",
            "fatjet_pt", "fatjet_eta", "fatjet_phi", "fatjet_E",
        ]

    print names
    for i in range(len(names)):
        names[i] = real_names[names[i]]
    print names
    print len(names), " names"

    features = sorted(zip(importances, names))
    importances = list(reversed([x for x, y in features]))
    names = list(reversed([y for x, y in features]))

    y_pos = np.arange(len(names))

    ax.barh(y_pos, importances, align='center',
            color='blue', ecolor='black')
    ax.set_yticks(y_pos)
    ax.set_yticklabels(names)
    ax.invert_yaxis()  # labels read top-to-bottom
    ax.set_xlabel('Importance (%s)' % type)
    ax.set_title('Feature Importance %s' % region)

    currentdir=os.getcwd()
    parent = os.path.split(currentdir)[1]
    if not os.path.isdir('feature_plots'):
        os.makedirs('feature_plots/')
    plt.savefig('feature_plots/'  + '%s_%s_%s_feature.pdf' % (parent, sig, region))

    if show:
        plt.show()

    return


def getAUC(y_test, weight, score):
    fpr, tpr, _ = roc_curve(y_test, score, weight)
    roc_auc = auc(fpr, tpr, reorder=True)
    return roc_auc

def plotROC(y_train, weight_train, score_train, y_val, weight_val, score_val, filename, show=False):

    fpr_train, tpr_train, _ = roc_curve(y_train, score_train, sample_weight=weight_train)
    roc_auc_train = auc(fpr_train, tpr_train, reorder=True)
    fpr_val, tpr_val, _ = roc_curve(y_val, score_val, sample_weight=weight_val)
    roc_auc_val = auc(fpr_val, tpr_val, reorder=True)
    #fpr_test, tpr_test, _ = roc_curve(y_test, score_test, sample_weight=weight_test)
    #roc_auc_test = auc(fpr_test, tpr_test, reorder=True)

    print('Training ROC AUC = %f' % roc_auc_train)
    print('Validation ROC AUC = %f' % roc_auc_val)
    #print('Test ROC AUC = %f' % roc_auc_test)

    fpr_train = 1.0 - fpr_train
    fpr_val = 1.0 - fpr_val
    #fpr_test = 1.0 - fpr_test

    plt.grid(color='gray', linestyle='--', linewidth=1)
    plt.plot(tpr_train, fpr_train, label='Train set, area = %0.6f' % roc_auc_train, color='red', linestyle='dashed')
    plt.plot(tpr_val, fpr_val, label='Val set, area = %0.6f' % roc_auc_val, color='blue', linestyle='dashdot')
    #plt.plot(tpr_test, fpr_test, label='Test set, area = %0.6f' % roc_auc_test, color='red', linestyle='dashed')
    plt.plot([0, 1], [1, 0], linestyle='--', color='black', label='Luck')
    plt.xlabel('Signal acceptance')
    plt.ylabel('Background rejection')
    plt.title('Receiver operating characteristic')
    plt.xlim(0, 1)
    plt.ylim(0, 1)
    plt.xticks(np.arange(0, 1, 0.1))
    plt.yticks(np.arange(0, 1, 0.1))
    plt.legend(loc='lower left', framealpha=1.0)

    #plt.savefig('plots/' + filename + '.png')
    if not os.path.isdir('bdt_plots'):
        os.mkdir('bdt_plots')
    plt.savefig('bdt_plots/' + filename + '.eps')

    if show: plt.show()

    plt.clf()
    return


def load_npy(catagories,region):

    badfile=False
    # signal loading
    train_npy=[]
    val_npy=[]
    train_wt=[]
    val_wt=[]
    for category in catagories:
        print 'Loading %s data...' %(category)
        filedir='arrays/'+category
        for npy in sorted(os.listdir(filedir)):
            if not npy.endswith('_%s_jets.npy' %(region)):
                continue
            if not ('_val_' in npy or '_train_' in npy):
                continue
            try:
                npytemp=[]
                if region[0] == 'r':
                    jets=np.load(filedir+'/'+npy)
                    if [jets.shape[1],jets.shape[2]]!=[1,2]:
                        print 'ERROR: The dimension of jets npy file, [%d,%d], is not correct!!' % (jets.shape[1],jets.shape[2])
                        error
                    npytemp.append(jets.reshape(len(jets),-1))
                met=np.load(filedir+'/'+npy.replace('jets','met'))
                if [met.shape[1],met.shape[2]]!=[1,6]:
                    print 'ERROR: The dimension of met npy file, [%d,%d], is not correct!!' % (met.shape[1],met.shape[2])
                    error
                npytemp.append(met.reshape(len(met),-1))
                if region[0] == 'm':
                    fatjet=np.load(filedir+'/'+npy.replace('jets','Fatjet'))
                    if [fatjet.shape[1],fatjet.shape[2]]!=[1,2]:
                        print 'ERROR: The dimension of fatjet npy file, [%d,%d], is not correct!!' % (fatjet.shape[1],fatjet.shape[2])
                        error
                    npytemp.append(fatjet.reshape(len(fatjet),-1))
                npytemp=np.concatenate(npytemp,axis=1)
                #npytemp=utils.restrictSample(npytemp, len(npytemp), 1)

                wt=np.load(filedir+'/'+npy.replace('jets','weight'))

                if '_train_' in npy:
                    train_npy.append(npytemp)
                    train_wt.append(wt)
                else:
                    val_npy.append(npytemp)
                    val_wt.append(wt)
            except:
                badfile=True
                print('ERROR: Unable to load \"'+ filedir+'/'+npy+'\"')
                stop=0
                for container in os.listdir('inputs'):
                    if stop: break
                    if category in container:
                        for ntuple in os.listdir('inputs/'+container):
                            if npy.split('_')[1] in ntuple and npy.split('_')[2] in ntuple:
                                print 'Original file: \"%s/%s\"'%(container,ntuple)
                                try:
                                    if txt: pass
                                except:
                                    txt = open('arrays/badsamplelist.txt','w')
                                if '_train_' in npy: section='_train'
                                elif '_val_' in npy: section='_val'
                                else: section=''
                                txt.write('%s %s %s %s %s badsample\n'%(container, ntuple, section, region, category))
                                stop=1
                                break
                return 0, 0, 0, 0, True
    train=np.concatenate(train_npy)
    val=np.concatenate(val_npy)
    train_wt=np.concatenate(train_wt)
    val_wt=np.concatenate(val_wt)

    return train, val, train_wt, val_wt, badfile

def train_model(params, args, region, sigs, bkgs):
    

    print 'Start preparing training samples...'
    # loading data
    train_sig, val_sig, train_sig_wt, val_sig_wt, badfilesig = load_npy(sigs,region)
    train_bkg, val_bkg, train_bkg_wt, val_bkg_wt, badfilebkg = load_npy(bkgs,region)

    if badfilesig or badfilebkg:
        return False

    # split data into train, val, and test samples
    print('Splitting data.')

    headers = ['Sample', 'Total', 'Training', 'Validation']
    sample_size_table = [
        ['Signal'    , len(train_sig)+len(val_sig), train_sig.shape, val_sig.shape],
        ['Background', len(train_bkg)+len(val_bkg), train_bkg.shape, val_bkg.shape],
    ]
    print tabulate(sample_size_table, headers=headers, tablefmt='simple')
 
    # organize data for training
    print('Organizing data for training.')

    train = np.concatenate((train_sig, train_bkg))
    val   = np.concatenate((val_sig  , val_bkg  ))

    y_train_cat = np.concatenate((np.zeros(len(train_sig), dtype=np.uint8), np.ones(len(train_bkg), dtype=np.uint8)))
    y_val_cat   = np.concatenate((np.zeros(len(val_sig)  , dtype=np.uint8), np.ones(len(val_bkg)  , dtype=np.uint8)))

    SF=args.sf
    if SF == -1: SF = 1.*len(train_sig_wt)/len(train_bkg_wt)

    y_train_weight = np.concatenate((train_sig_wt*(len(train_sig_wt)+len(train_bkg_wt))*SF/(np.average(train_sig_wt)*(1+SF)*len(train_sig_wt)), train_bkg_wt*(len(train_sig_wt)+len(train_bkg_wt))/(np.average(train_bkg_wt)*(1+SF)*len(train_bkg_wt))))
    y_val_weight   = np.concatenate((val_sig_wt*(len(train_sig_wt)+len(train_bkg_wt))*SF/(np.average(train_sig_wt)*(1+SF)*len(train_sig_wt)), val_bkg_wt*(len(train_sig_wt)+len(train_bkg_wt))/(np.average(train_bkg_wt)*(1+SF)*len(train_bkg_wt))))

    dTrain = xgb.DMatrix(train, label=y_train_cat, weight=(y_train_weight))
    dVal = xgb.DMatrix(val, label=y_val_cat, weight=(y_val_weight))
    # train model
    print('Train model.')
    num_round=args.numRound

    param = {'colsample_bytree': 0.601636189527514, 'silent': 1, 'eval_metric': ['auc','logloss'], 'max_delta_step': 3.633816495872836, 'nthread': 4, 'min_child_weight': 4, 'subsample': 0.6588843327224047, 'eta': 0.019251809428140865, 'objective': 'binary:logistic', 'alpha': 0.2894769437092044, 'max_depth': 7, 'gamma': 0.12186533874627076, 'booster': 'gbtree'}

    if params:
        for key in params:
            param[key] = params[key]

    print(param)

    evallist  = [(dTrain, 'train'), (dVal, 'eval')]
    evals_result = {}
    eval_result_history = []
    try:
        bst = xgb.train(param, dTrain, num_round, evals=evallist, early_stopping_rounds=20, evals_result=evals_result)
    except KeyboardInterrupt:
        print('Finishing on SIGINT.')

    # test model
    print('Test model.')

    # will do: split data to train, val and test datasets
    score = bst.predict(dVal)
    if args.roc:
        score_train = bst.predict(dTrain)
    #scoresig=bst.predict(xgb.DMatrix(val_sig))
    #np.save('score.npy',scoresig)
    aucValue = getAUC(y_val_cat, y_val_weight, score)
    print("param: %s, Val AUC: %s" % (param, aucValue))

    if args.roc:
        plotROC(y_train_cat, y_train_weight, score_train, y_val_cat, y_val_weight, score, 'roc_%s'%(region), show=False)

    print(" ")
    print("======reversing training and validation samples======")
    evallist  = [(dVal, 'train'), (dTrain, 'eval')]
    evals_result = {}
    eval_result_history = []
    try:
        bst_r = xgb.train(param, dVal, num_round, evals=evallist, early_stopping_rounds=20, evals_result=evals_result)
    except KeyboardInterrupt:
        print('Finishing on SIGINT.')

    # test model
    print('Test model.')

    # will do: split data to have train, val and test datasets
    score = bst_r.predict(dTrain)
    if args.roc:
        score_train = bst.predict(dVal)
    #scoresig=bst.predict(xgb.DMatrix(val_sig))
    #np.save('score.npy',scoresig)
    aucValue = getAUC(y_train_cat, y_train_weight, score)
    print("param: %s, Val AUC: %s" % (param, aucValue))

    if args.roc:
        plotROC(y_val_cat, y_val_weight, score_train, y_train_cat, y_train_weight, score, 'roc_%s_r'%(region), show=False)

    # save model
    if not os.path.isdir('models'):
        os.makedirs('models')
    if args.save:
        print('=============================================================')
        print('Saving model')
        signame=''
        for sig in sigs:
            signame+=sig+'_'
        bst.save_model('models/%s%s.h5' % (signame,region))
        bst_r.save_model('models/%s%s_r.h5' % (signame,region))

    return True




def main():
    args=getArgs()
    params=args.params

    if not args.featureonly:

        print '=============================================================================='
        sigs=[]
        for i in range(0,len(args.ZPtHDM),2):
            sigs.append('zp2hdmbbmzp%smA%s'%(args.ZPtHDM[i],args.ZPtHDM[i+1]))
        for i in range(0,len(args.tHDMa_ggF),2):
            sigs.append('2HDMa_ggF_tb1_sp035_mA%s_ma%s'%(args.tHDMa_ggF[i],args.tHDMa_ggF[i+1]))
        for i in range(0,len(args.tHDMa_bb),2):
            sigs.append('2HDMa_bb_tb10_sp035_mA%s_ma%s'%(args.tHDMa_bb[i],args.tHDMa_bb[i+1]))
        print 'INFO:  Training as signal on:  ', sigs

        bkgs = ['Z','W','ttbar']#,'VH','diboson']#,'stop','diboson','VH']
        print 'INFO:  Traing as backgroun on:  ', bkgs
        print '------------------------------------------------------------------------------'

        if args.region[0] == 'm':
            region = args.region.replace('m', 'merged')
        else:
            region = args.region.replace('r', 'resolved')

        while not train_model(params, args, region, sigs, bkgs):
            os.system('python RecoverMissingFiles.py -b')

        print '------------------------------------------------------------------------------'
        print 'Finished training.'

    if args.feature or args.featureonly:
        process_features('resolved' if args.region=='r' else 'merged', args.ZPtHDM,show=True)

    return

if __name__ == '__main__':
    main()
