#!/usr/bin/env python
#
#
#
#  Created by Jay Chan
#
#  8.22.2018
#
#  Modified from ttHyy XGBosst anaylsis codes
#
#
#
#
import os
import ROOT
from argparse import ArgumentParser
from math import sqrt, log
import math
from pdb import set_trace

ROOT.gROOT.SetBatch(True)

def getArgs():
    """Get arguments from command line."""
    parser = ArgumentParser()
    parser.add_argument('-r', '--region', action='store', choices=['m', 'r'], default='r', help='Region to process')
    parser.add_argument('--ZPtHDM', type=int, nargs='+', default = [], help='Masspoint of training ZP2HDM model samples.')
    parser.add_argument('--tHDMa_ggF', type=int, nargs='+', default = [], help='Masspoint of training 2HDM+a model samples (with ggF production).')
    parser.add_argument('--tHDMa_bb', type=int, nargs='+', default = [], help='Masspoint of training 2HDM+a model samples (with bb production).')
    parser.add_argument('--signalmodel', action='store', nargs='+', default=['ZP2HDM', 'ZP2HDM', 'ZP2HDM'], help="targeted signal models: 'ZP2HDM', '2HDMa_ggF', '2HDMa_bb'")
    parser.add_argument('-s','--signalmasspoint',type=int,nargs='+',default=[1400,600,1000,600,600,300],help='Masspoint of specified signal model mass point of ZP2HDM.')
    parser.add_argument('-n','--nscan',type=int,default=100,help='number of scan.')
    parser.add_argument('--name',action='store',default='baseline_yields',help='Name of the yields.')
    parser.add_argument('-u','--ulimit',type=float,default=0.2,help='constraint on u.')
    parser.add_argument('--skip', action='store_true', help='skip the hadd part.')
    #parser.add_argument('-l','--lumi',type=float,default=79.8, help='The integrated luminosit to be normalized to.') a->36.1 d->43.7
    return  parser.parse_args()

def calc_sig(sig, bkg,s_err,b_err):

  ntot = sig + bkg

  if(sig <= 0): return 0, 0
  if(bkg <= 0): return 0, 0

  if 2*((ntot*log(ntot/bkg)) - sig) <= 0: return 0, 0
  #Counting experiment
  #significance = sqrt(2*((ntot*log(ntot/bkg)) - sig))
  significance = sig/sqrt(bkg)

  #error on significance
  numer = sqrt((log(ntot/bkg)*s_err)**2 + ((log(1+(sig/bkg)) - (sig/bkg))*b_err)**2)
  #if (isTest) delta_b = n_data/sqrt(n_data/(4./45.))
  uncert = (numer/significance)

  #safety feature to prevent low number of events
  #if(n_data < 0.8) {significance = 0; uncert=0;}
  return significance, uncert

def hist_integral(hist,i,j):
    err = ROOT.Double()
    if i>j:
        n=0
        err=0
    else: n = hist.IntegralAndError(i,j,err)
    return n, err

def sum_hist_integral(integrals,xs=1):
    err, n = 0, 0
    for integral in integrals:
        n += integral[0]
        err += integral[1]**2
    return n*xs, sqrt(err)*xs

def sum_z(zs,us):
    sumu=0
    sumz=0
    for i in range(len(zs)):
        sumz+=zs[i]**2
        sumu+=(zs[i]*us[i])**2
    sumz=sqrt(sumz)
    sumu=sqrt(sumu)/sumz if sumz != 0 else 0
    return sumz,sumu
        

def GetSigmas(inDict, inFile):
    
    BRHbb=0.571

    fin = open(inFile,"r")
    lines = fin.readlines()
    
    #print lines
    
    for line in lines:
        if line[0]=="#":
            continue
        
        mzp   = line.split()[1]
        ma    = line.split()[2]
        sigma = float(line.split()[5])
        
        #print "read : ",mzp,ma,sigma

        inDict[mzp+","+ma]=float(sigma)*BRHbb
        #inDict[mzp+","+ma]=1.
    
    return inDict

def gettingsig(train_sig,region,sigs,bkgs, imax,nscan, targetsigs, yieldname, CrossSections):#, lumi):

    print 'Evaluating significances for all of the signal models...'

    baselinetxt=open('data/yields/%s.txt'%(yieldname),'r')
    baseline_yield={}
    lines = baselinetxt.read().splitlines()
    if region=='merged':
        index=4
        for line in lines:
            if line.startswith('#'): continue
            if line=='':continue
            if line.startswith('-'): continue
            baseline_yield[line.split()[0]]=float(line.split()[index])
    else:
        for line in lines:
            if line.startswith('#'): continue
            if line=='':continue
            if line.startswith('-'): continue
            baseline_yield[line.split()[0]] = [float(line.split()[1]), float(line.split()[2]), float(line.split()[3])]

    if region == 'merged': var='m_J'
    else: var='m_jj'
  
    if not os.path.isdir('significances'):
        print 'INFO: Creating output folder: \"significances\"'
        os.makedirs("significances")
    if not os.path.isdir('significances/model_%s%s' % (train_sig,region)):
        print 'INFO: Creating new model folder:  model_%s%s' % (train_sig,region)
        os.makedirs('significances/model_%s%s' % (train_sig,region))

    filename_ext = ''
    for j in range(len(targetsigs)):
        filename_ext += '_%s'%targetsigs[j]
    txt=open('significances/model_%s%s/targetsig%s.txt' % (train_sig, region, filename_ext), 'w')
    txt.write('#nscan: %d\n'%nscan)
    for j in range(len(imax)):
        txt.write('#imax #%d: %d\n'%(j, imax[j]))
        txt.write('#Bondary #%d: %f\n'%(j, (imax[j]-1.)/nscan))
    txt.write('#Signal_Model    Significance_after_BDT  Uncertainty_after_BDT   Baseline_Significance   Baseline_Uncertainty    Percentage_improvement\n')
    txt.write('_______________________________________________________________________________________________________________________________________\n\n')

    f_bkg_mc16a = ROOT.TFile('outputs/model_%s%s/bkg_mc16a.root' % (train_sig,region))
    f_bkg_mc16d = ROOT.TFile('outputs/model_%s%s/bkg_mc16d.root' % (train_sig,region))
    f_bkg_mc16e = ROOT.TFile('outputs/model_%s%s/bkg_mc16e.root' % (train_sig,region))
    t_bkg_mc16a = f_bkg_mc16a.Get('test')
    t_bkg_mc16d = f_bkg_mc16d.Get('test')
    t_bkg_mc16e = f_bkg_mc16e.Get('test')
    h_bkg_mc16a = ROOT.TH1F('h_bkg_mc16a','h_bkg_mc16a',nscan,0.,1.)
    h_bkg_mc16a.Sumw2()
    h_bkg_mc16d = ROOT.TH1F('h_bkg_mc16d','h_bkg_mc16d',nscan,0.,1.)
    h_bkg_mc16d.Sumw2()
    h_bkg_mc16e = ROOT.TH1F('h_bkg_mc16e','h_bkg_mc16e',nscan,0.,1.)
    h_bkg_mc16e.Sumw2()
    t_bkg_mc16a.Draw("bdt_score>>h_bkg_mc16a","weight*36.1*1000*(%s>=100000&&%s<=140000)"%(var,var))
    t_bkg_mc16d.Draw("bdt_score>>h_bkg_mc16d","weight*44.2*1000*(%s>=100000&&%s<=140000)"%(var,var))
    t_bkg_mc16e.Draw("bdt_score>>h_bkg_mc16e","weight*58.4*1000*(%s>=100000&&%s<=140000)"%(var,var))
    w_bkg=5

    nbkgs, nbkg_err = [], []
    for j in range(len(imax)+1):
        nbkgj,nbkgj_err=sum_hist_integral([hist_integral(h_bkg_mc16a,1 if j == 0 else imax[j-1],(imax[j]-1) if j != len(imax) else nscan),hist_integral(h_bkg_mc16d, 1 if j == 0 else imax[j-1],(imax[j]-1) if j != len(imax) else nscan),hist_integral(h_bkg_mc16e, 1 if j == 0 else imax[j-1],(imax[j]-1) if j != len(imax) else nscan)])
        nbkgs.append(nbkgj)
        nbkg_err.append(nbkgj_err)

    #print '1st bin statistical error: %f%%' %(nbkg3_err/nbkg3*100 if nbkg3 != 0 else 0)
    #print '2nd bin statistical error: %f%%' %(nbkg2_err/nbkg2*100 if nbkg2 != 0 else 0)
    #print '3rd bin statistical error: %f%%' %(nbkg1_err/nbkg1*100 if nbkg1 != 0 else 0)
    #print 'Last bin statistical error: %f%%' %(nbkgl_err/nbkgl*100 if nbkgl != 0 else 0)

    
    nbkg=baseline_yield['bkg_tot']


    for sig in sigs:

        nsig_mc16a = []
        if os.path.isfile('outputs/model_%s%s/%s_mc16a.root' % (train_sig,region,sig)):
            f_sig_mc16a = ROOT.TFile('outputs/model_%s%s/%s_mc16a.root' % (train_sig,region,sig))
            t_sig_mc16a = f_sig_mc16a.Get('test')
            h_sig_mc16a=ROOT.TH1F(sig+'_mc16a',sig+'_mc16a',nscan,0.,1.)
            h_sig_mc16a.Sumw2()
            t_sig_mc16a.Draw("bdt_score>>%s_mc16a"%(sig),"weight*1000*36.1*(%s>=100000&&%s<=140000)"%(var,var))
            for j in range(len(imax)+1):
                nsigj_mc16a = hist_integral(h_sig_mc16a, 1 if j == 0 else imax[j-1],(imax[j]-1) if j != len(imax) else nscan)
                nsig_mc16a.append(nsigj_mc16a)
        else:
            for j in range(len(imax)+1):
                nsig_mc16a.append((0, 0))

        nsig_mc16d = []
        if os.path.isfile('outputs/model_%s%s/%s_mc16d.root' % (train_sig,region,sig)):
            f_sig_mc16d = ROOT.TFile('outputs/model_%s%s/%s_mc16d.root' % (train_sig,region,sig))
            t_sig_mc16d = f_sig_mc16d.Get('test')
            h_sig_mc16d=ROOT.TH1F(sig+'_mc16d',sig+'_mc16d',nscan,0.,1.)
            t_sig_mc16d.Draw("bdt_score>>%s_mc16d"%(sig),"weight*1000*44.2*(%s>=100000&&%s<=140000)"%(var,var)) 
            for j in range(len(imax)+1):
                nsigj_mc16d = hist_integral(h_sig_mc16d, 1 if j == 0 else imax[j-1],(imax[j]-1) if j != len(imax) else nscan)
                nsig_mc16d.append(nsigj_mc16d)
        else:
            for j in range(len(imax)+1):
                nsig_mc16d.append((0, 0))

        nsig_mc16e = []
        if os.path.isfile('outputs/model_%s%s/%s_mc16e.root' % (train_sig,region,sig)):
            f_sig_mc16e = ROOT.TFile('outputs/model_%s%s/%s_mc16e.root' % (train_sig,region,sig))
            t_sig_mc16e = f_sig_mc16e.Get('test')
            h_sig_mc16e=ROOT.TH1F(sig+'_mc16e',sig+'_mc16e',nscan,0.,1.)
            if 'zp2hdm' in sig: t_sig_mc16e.Draw("bdt_score>>%s_mc16e"%(sig),"weight*1000*58.4*(%s>=100000&&%s<=140000)"%(var,var))
            else: t_sig_mc16e.Draw("bdt_score>>%s_mc16e"%(sig),"weight*1000*139*(%s>=100000&&%s<=140000)"%(var,var))
            for j in range(len(imax)+1):
                nsigj_mc16e = hist_integral(h_sig_mc16e, 1 if j == 0 else imax[j-1],(imax[j]-1) if j != len(imax) else nscan)
                nsig_mc16e.append(nsigj_mc16e)

        else:
            for j in range(len(imax)+1):
                nsig_mc16e.append((0, 0))

        xs=CrossSections[sig.replace('zp2hdmbbmzp','').replace('mA',',')] if 'zp2hdm' in sig else 1

        nsigs, nsig_err = [], []
        for j in range(len(nsig_mc16a)):
            nsigj,nsigj_err=sum_hist_integral([nsig_mc16a[j],nsig_mc16d[j],nsig_mc16e[j]],xs)
            nsigs.append(nsigj)
            nsig_err.append(nsigj_err)

        ss, us = [], []
        for j in range(len(nsigs)):
            sj, uj = calc_sig(nsigs[j],nbkgs[j],nsig_err[j],nbkg_err[j])
            ss.append(sj)
            us.append(uj)
        s,u=sum_z(ss, us)
        if region == 'resolved':
            nsig=[ i*(CrossSections[sig.replace('zp2hdmbbmzp','').replace('mA',',')] if 'zp2hdm' in sig else 1) for i in baseline_yield[sig]]
            z2 = 0
            for i in range(3):
                z2 += calc_sig(nsig[i],nbkg[i],sqrt(nsig[i]),sqrt(nbkg[i]))[0]**2
            s0 = sqrt(z2)
            #s0,u0=calc_sig(nsig,nbkg,sqrt(nsig),sqrt(nbkg))
        else:
            nsig = baseline_yield[sig]*(CrossSections[sig.replace('zp2hdmbbmzp','').replace('mA',',')] if 'zp2hdm' in sig else 1)
            s0 = calc_sig(nsig,nbkg,sqrt(nsig),sqrt(nbkg))[0]
       
        print 'Significance of %s:  %f +- %f         Baseline significance:  %f        Improvement: %f %%'%(sig,s,abs(u),s0,(s-s0)/s0*100 if s0 !=0 else 0)
        txt.write('%s  %f  %f  %f  %f\n'%(sig,s,abs(u),s0,(s-s0)/s0*100 if s0 !=0 else 0))


def categorizing(train_sig,region,targetsigs,bkgs,nscan, CrossSections,ulimit, targetmodel, skip):


    if not skip:
        #if not os.path.isfile('outputs/model_%s_%s/bkg.root' % (train_sig,region)):
        bkglist_mc16a=''
        bkglist_mc16d=''
        bkglist_mc16e=''
        for bkg in bkgs:
            if os.path.isfile('outputs/model_%s%s/%s_mc16a.root'% (train_sig,region,bkg)):bkglist_mc16a+=' outputs/model_%s%s/%s_mc16a.root'% (train_sig,'Resolved',bkg)
            if os.path.isfile('outputs/model_%s%s/%s_mc16d.root'% (train_sig,region,bkg)): bkglist_mc16d+=' outputs/model_%s%s/%s_mc16d.root'% (train_sig,'Resolved',bkg)
            if os.path.isfile('outputs/model_%s%s/%s_mc16e.root'% (train_sig,region,bkg)): bkglist_mc16e+=' outputs/model_%s%s/%s_mc16e.root'% (train_sig,'Resolved',bkg)
        os.system("hadd -f outputs/model_%s%s/bkg_mc16a.root"%(train_sig,region)+bkglist_mc16a)
        os.system("hadd -f outputs/model_%s%s/bkg_mc16d.root"%(train_sig,region)+bkglist_mc16d)
        os.system("hadd -f outputs/model_%s%s/bkg_mc16e.root"%(train_sig,region)+bkglist_mc16e)

    f_bkg_mc16a = ROOT.TFile('outputs/model_%s%s/bkg_mc16a.root' % (train_sig,region))
    f_bkg_mc16d = ROOT.TFile('outputs/model_%s%s/bkg_mc16d.root' % (train_sig,region))
    f_bkg_mc16e = ROOT.TFile('outputs/model_%s%s/bkg_mc16e.root' % (train_sig,region))
    t_bkg_mc16a = f_bkg_mc16a.Get('test')
    t_bkg_mc16d = f_bkg_mc16d.Get('test')
    t_bkg_mc16e = f_bkg_mc16e.Get('test')

    h_bkg_mc16a=ROOT.TH1F('h_bkg_mc16a','h_bkg_mc16a',nscan,0,1)
    h_bkg_mc16d=ROOT.TH1F('h_bkg_mc16d','h_bkg_mc16d',nscan,0,1)
    h_bkg_mc16e=ROOT.TH1F('h_bkg_mc16e','h_bkg_mc16e',nscan,0,1)

    h_bkg_mc16a.Sumw2()
    h_bkg_mc16d.Sumw2()
    h_bkg_mc16e.Sumw2()

    if region == 'merged': var='m_J'
    else: var='m_jj'

    t_bkg_mc16a.Draw("bdt_score>>h_bkg_mc16a","weight*(%s>=100000&&%s<=140000)*1000*36.1"%(var,var))
    t_bkg_mc16d.Draw("bdt_score>>h_bkg_mc16d","weight*(%s>=100000&&%s<=140000)*1000*44.2"%(var,var))
    t_bkg_mc16e.Draw("bdt_score>>h_bkg_mc16e","weight*(%s>=100000&&%s<=140000)*1000*58.4"%(var,var))


    xs=1#CrossSections[targetsig1.replace('zp2hdmbbmzp','').replace('mA',',')]

    imax = [0 for j in range(len(targetmodel))]

    for j in range(len(targetmodel)):

        if targetmodel[j] == 'ZP2HDM': f_sig_mc16a = ROOT.TFile('outputs/model_%s%s/%s_mc16a.root' % (train_sig,region,targetsigs[j]))
        if targetmodel[j] == 'ZP2HDM': f_sig_mc16d = ROOT.TFile('outputs/model_%s%s/%s_mc16d.root' % (train_sig,region,targetsigs[j]))
        f_sig_mc16e = ROOT.TFile('outputs/model_%s%s/%s_mc16e.root' % (train_sig,region,targetsigs[j]))
        if targetmodel[j] == 'ZP2HDM': t_sig_mc16a = f_sig_mc16a.Get('test')
        if targetmodel[j] == 'ZP2HDM': t_sig_mc16d = f_sig_mc16d.Get('test')
        t_sig_mc16e = f_sig_mc16e.Get('test')

        h_sig_mc16a=ROOT.TH1F('h_sig_mc16a','h_sig_mc16a',nscan,0,1)
        h_sig_mc16d=ROOT.TH1F('h_sig_mc16d','h_sig_mc16d',nscan,0,1)
        h_sig_mc16e=ROOT.TH1F('h_sig_mc16e','h_sig_mc16e',nscan,0,1)

        h_sig_mc16a.Sumw2()
        h_sig_mc16d.Sumw2()
        h_sig_mc16e.Sumw2()

        if targetmodel[j] == 'ZP2HDM': t_sig_mc16a.Draw("bdt_score>>h_sig_mc16a","weight*(%s>=100000&&%s<=140000)*1000*36.1"%(var,var))
        if targetmodel[j] == 'ZP2HDM': t_sig_mc16d.Draw("bdt_score>>h_sig_mc16d","weight*(%s>=100000&&%s<=140000)*1000*44.2"%(var,var))
        t_sig_mc16e.Draw("bdt_score>>h_sig_mc16e","weight*(%s>=100000&&%s<=140000)*1000*%f"%(var,var, 58.4 if targetmodel[j] == 'ZP2HDM' else 139))

        #imax=0
        smax=0
        umax=0
        nsig1max=0
        nbkg1max=0
        for i in range(1,nscan+1 if j == 0 else imax[j-1]):
            nsigl,nsigl_err=sum_hist_integral([hist_integral(h_sig_mc16a,1,i-1),hist_integral(h_sig_mc16d,1,i-1),hist_integral(h_sig_mc16e,1,i-1)],xs)
            nbkgl,nbkgl_err=sum_hist_integral([hist_integral(h_bkg_mc16a,1,i-1),hist_integral(h_bkg_mc16d,1,i-1),hist_integral(h_bkg_mc16e,1,i-1)])
            sl,ul=calc_sig(nsigl,nbkgl,nsigl_err,nbkgl_err)
            nsig1,nsig1_err=sum_hist_integral([hist_integral(h_sig_mc16a,i,nscan if j == 0 else imax[j-1]-1),hist_integral(h_sig_mc16d,i,nscan if j == 0 else imax[j-1]-1),hist_integral(h_sig_mc16e,i,nscan if j == 0 else imax[j-1]-1)],xs)
            nbkg1,nbkg1_err=sum_hist_integral([hist_integral(h_bkg_mc16a,i,nscan if j == 0 else imax[j-1]-1),hist_integral(h_bkg_mc16d,i,nscan if j == 0 else imax[j-1]-1),hist_integral(h_bkg_mc16e,i,nscan if j == 0 else imax[j-1]-1)])
            if nbkg1 == 0 or nbkg1_err/nbkg1 > ulimit: continue
            s1,u1=calc_sig(nsig1,nbkg1,nsig1_err,nbkg1_err)
            s,u=sum_z([sl,s1],[ul,u1])
            if s>smax:
                smax=s
                imax[j]=i
                umax=u
                nsig1max=nsig1
                nbkg1max=nbkg1
        #print nsig1max, nbkg1max, nsig2max, nbkg2max,nsig3max, nbkg3max, s1max, s2max, s3max

        print '========================================================================='
        print 'The maximal significance:  %f +- %f' %(smax, umax)
        print '#%d boundary:  %f' %(j, (imax[j]-1.)/nscan)
        print '========================================================================='


    return sorted(imax)

def main():

    args=getArgs()

    #lumi=args.lumi

    ZP2HDM = [[600, 300], [600, 400], [800, 300], [800, 400], [800, 500], [1000, 400], [1000, 500], [1000, 600], [1200, 500],
              [1200, 600], [1200, 700], [1400, 500], [1400, 600], [1400, 700], [1400, 800], [1600, 300], [1600, 500], [1600, 600],
              [1600, 700], [1600, 800], [1800, 500], [1800, 600], [1800, 700], [1800, 800], [2000, 400], [2000, 500], [2000, 600],
              [2000, 700], [2000, 800], [2200, 300], [2200, 400], [2200, 500], [2200, 600], [2200, 700], [2400, 300], [2400, 400],
              [2400, 500], [2400, 600], [2400, 700], [400, 300], [400, 400], [2600, 300], [2600, 400], [2600, 500], [2600, 600],
              [2600, 700], [2800, 300], [2800, 400], [2800, 500], [2800, 600], [3000, 300], [3000, 400], [3000, 500], [3000, 600]]
    A2HDM =  [[300, 150], [400, 250], [600, 350], [800, 500],
              [1000, 150], [1100, 250], [1200, 350], [1300, 500],
              [1400, 150], [1600, 250], [1600, 350], [1800, 150]]


    sigs = []
    for mass in ZP2HDM:
        sigs.append('zp2hdmbbmzp%smA%s'%(mass[0],mass[1]))

    for mass in A2HDM:

        MA = str(mass[0])
        Ma  = str(mass[1])

        for production in ['2HDMa_bb_tb10', '2HDMa_ggF_tb1']:

            sigs.append(production + '_sp035_mA'+ MA + '_ma' + Ma)

    sigmass=args.signalmasspoint

    targetsigs = []
    for i in range(len(args.signalmodel)):
        if args.signalmodel[i] == 'ZP2HDM':
            targetsigs.append('zp2hdmbbmzp%smA%s'%(sigmass[i*2],sigmass[i*2+1]))
        elif args.signalmodel[i] == '2HDMa_ggF':
            targetsigs.append('2HDMa_ggF_tb1_sp035_mA%s_ma%s'%(sigmass[i*2],sigmass[i*2+1]))
        elif args.signalmodel[i] == '2HDMa_bb':
            targetsigs.append('2HDMa_bb_tb10_sp035_mA%s_ma%s'%(sigmass[i*2],sigmass[i*2+1]))

    train_sig=''
    for i in range(0,len(args.ZPtHDM),2):
        train_sig += 'zp2hdmbbmzp%smA%s_'%(args.ZPtHDM[i],args.ZPtHDM[i+1])
    for i in range(0,len(args.tHDMa_ggF),2):
        train_sig += '2HDMa_ggF_tb1_sp035_mA%s_ma%s_'%(args.tHDMa_ggF[i],args.tHDMa_ggF[i+1])
    for i in range(0,len(args.tHDMa_bb),2):
        train_sig += '2HDMa_bb_tb10_sp035_mA%s_ma%s_'%(args.tHDMa_bb[i],args.tHDMa_bb[i+1])

    bkgs = ['Z','W','ttbar','stop','VH','diboson']

    if args.region == 'm':
        region = 'merged'
    else:
        region = 'resolved'

    nscan=args.nscan

    CrossSections={}
    CrossSections = GetSigmas(CrossSections, "data/sigma_FixedHiggsesTo300_08052017.txt")

    imax=categorizing(train_sig,region,targetsigs,bkgs, nscan, CrossSections,args.ulimit, args.signalmodel, args.skip)

    yieldname=args.name

    gettingsig(train_sig,region,sigs,bkgs, imax,nscan, targetsigs, yieldname, CrossSections)

    return



if __name__ == '__main__':
    main()
