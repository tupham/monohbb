#!/usr/bin/env python
#
#
#
#  Created by Jay Chan
#
#  8.12.2018
#
#  Modified from ttHyy XGBosst anaylsis codes
#
#
#
#
import os
import math
from argparse import ArgumentParser
import ROOT
import numpy as np
from array import array
import time
import sys
import uproot

def getArgs():
    """Get arguments from command line."""
    parser = ArgumentParser(description="Process input rootfiles into numpy arrays for MonoHbb XGBoost analysis.")
    parser.add_argument('-n', '--name', action='store', required=True, help='File to process')
    parser.add_argument('-i', '--inputdir', action='store', default='inputs', help='Directory that contains ntuples.')
    parser.add_argument('-r', '--region', action='store', choices=['m', 'm_500_900', 'm_900', 'r', 'r_150_200', 'r_200_350','r_350_500'], required=True, default='m', help='Region to process')
    parser.add_argument('-t', '--train', action='store_true', default=False, help='To process training samples')
    parser.add_argument('-v', '--val', action='store_true', default=False, help='To process validation samples')
    parser.add_argument('-c', '--category', action='store', required=True, help='The category of samples')
    parser.add_argument('-d', '--isdata', action='store_true', default=False, help='Real data')
    parser.add_argument('--test',action='store_true',default=False)
    return  parser.parse_args()

def set_pscore(x):
    if x < 0.11:
        score=0
    elif x < 0.64:
        score=1
    elif x < 0.83:
        score=2
    elif x<0.94:
        score = 3
    else:
        score = 4
    return score

def process_arrays(args):


    DSID=args.name.split('.')[3]
    container=args.name.split('/')[0]

    if not args.isdata:
        # Open sisters files to calculate the total sum of weights
        SumW=0
        for ntuple in os.listdir(args.inputdir+"/"+args.name.split('/')[0]):
        
            f = ROOT.TFile.Open(args.inputdir+"/"+args.name.split('/')[0]+"/"+ntuple)
            meta = f.Get('MetaDataTree')

            # meta variables
            TotalSumW = array('d', [0])
            meta.SetBranchAddress('TotalSumW',TotalSumW)

            for i in range(meta.GetEntries()):
                meta.GetEntry(i)
                print(TotalSumW[0])
                SumW+=TotalSumW[0]

        # load the text file to get cross section
        if 'zp2hdmbb' in args.name or '2HDMa' in args.name: 
            xs=1
            kf=1
            fe=1
        else:
            inlist=False
            xsectiontxt=open('data/PMGxsecDB_mc16_signal_bkg.txt','r')
            lines = xsectiontxt.read().splitlines()
            for line in lines:
                if line.startswith(DSID):
                    xs=float(line.split()[2])
                    kf=float(line.split()[4])
                    fe=float(line.split()[3])
                    inlist=True
                    break
            if not inlist: 
                print 'ERROR: DSID: %s is not in the list!!'%(DSID)
                quit()
        print 'INFO: Cross-section:     ', xs
        print 'INFO: k-factor:          ', kf
        print 'INFO: Filter Efficiency: ', fe
        print 'Sum of weights:          ', SumW
 
    # Open input files
    f = ROOT.TFile.Open(args.inputdir+"/"+args.name)
    t = f.Get('MonoH_Nominal')

    # Define variables
    IsMETTrigPassed = array('B', [0])
    N_SignalElectrons = array('I', [0])
    N_SignalMuons = array('I',[0])
    N_BaselineElectrons = array('I',[0])
    N_BaselineMuons = array('I',[0])
    N_SignalTaus = array('I', [0])
    N_not_associated_Taus = array('I', [0])
    N_TausExtended_Merged = array('I', [0])
    N_TausExtended_Resolved = array('I', [0])
    N_Jets04 = array('I', [0])
    N_Jets10 = array('I', [0])
    N_associated_Jets02 = array('I', [0])
    N_BJets_04 = array('I', [0])
    N_BTags_associated_02 = array('I', [0])
    N_BTags_not_associated_02 = array('I', [0])
    TrackJet_1isBjet = array('B', [0])
    TrackJet_2isBjet = array('B', [0])
    sigjet012ptsum = array('f', [0])
    m_jj = array('f', [0])
    m_J = array('f', [0])
    Jet_BLight1BPt = array('f', [0])
    Jet_BLight2BPt = array('f', [0])
    DeltaRJJ = array('f', [0])
    DeltaPhiMin3 = array('f', [0])
#    DeltaPhiMetTrackMet = array('f', [0])
    DeltaPhiJJ = array('f', [0])
    DeltaPhiMetJJ = array('f', [0])
    HtRatioResolved = array('f', [0])
    HtRatioMerged = array('f', [0])
    MetTST_Significance_noPUJets_noSoftTerm = array('f', [0])
    eventNumber = array('L', [0])
    MetTST_met = array('f', [0])
    MetTST_phi = array('f', [0])
    MetTrack_met = array('f', [0])
    Jet_pt = ROOT.vector('float')()
    Jet_eta = ROOT.vector('float')()
    Jet_phi = ROOT.vector('float')()
    Jet_m = ROOT.vector('float')()
    Jet_bjet = ROOT.vector('char')()
    ForwardJet_pt = ROOT.vector('float')()
    ForwardJet_eta = ROOT.vector('float')()
    ForwardJet_phi = ROOT.vector('float')()
    ForwardJet_m = ROOT.vector('float')()
    FatJet_pt = ROOT.vector('float')()
    FatJet_eta = ROOT.vector('float')()
    FatJet_phi = ROOT.vector('float')()
    FatJet_m = ROOT.vector('float')()
    TrackJet_pt = ROOT.vector('float')()
    TrackJet_eta = ROOT.vector('float')()
    TrackJet_phi = ROOT.vector('float')()
    TrackJet_m = ROOT.vector('float')()
    TrackJet_bjet = ROOT.vector('char')()
    TrackJet_isAssociated = ROOT.vector('char')()

    # new variables
    Jet_MV2c10=ROOT.vector('double')()


    TrackJet_1passOR = array('B', [0])
    TrackJet_2passOR = array('B', [0])
    if not args.isdata:
        GenWeight = array('d', [0])
        GenWeightMCSampleMerging = array('d', [0])
        EleWeight = array('d', [0])
        MuoWeight = array('d', [0])
        TauWeight = array('d', [0])
        TrackJetWeight = array('d', [0])
        JetWeightJVT = array('d', [0])
        JetWeightBTag = array('d', [0])
        MET_TriggerSF = array('d', [0])
        

    #Set up the address of variables in branch
    t.SetBranchAddress('IsMETTrigPassed', IsMETTrigPassed)
    t.SetBranchAddress('N_SignalElectrons', N_SignalElectrons)
    t.SetBranchAddress('N_SignalMuons', N_SignalMuons)
    t.SetBranchAddress('N_BaselineElectrons', N_BaselineElectrons)
    t.SetBranchAddress('N_BaselineMuons', N_BaselineMuons)
    t.SetBranchAddress('N_SignalTaus', N_SignalTaus)
    t.SetBranchAddress('N_not_associated_Taus', N_not_associated_Taus)
    t.SetBranchAddress('N_TausExtended_Merged', N_TausExtended_Merged)
    t.SetBranchAddress('N_TausExtended_Resolved', N_TausExtended_Resolved)
    t.SetBranchAddress('N_Jets04', N_Jets04)
    t.SetBranchAddress('N_Jets10', N_Jets10)
    t.SetBranchAddress('N_BJets_04', N_BJets_04)
    t.SetBranchAddress('N_associated_Jets02', N_associated_Jets02)
    t.SetBranchAddress('N_BTags_associated_02', N_BTags_associated_02)
    t.SetBranchAddress('N_BTags_not_associated_02', N_BTags_not_associated_02)
    t.SetBranchAddress('TrackJet_1isBjet', TrackJet_1isBjet)
    t.SetBranchAddress('TrackJet_2isBjet', TrackJet_2isBjet)
    t.SetBranchAddress('sigjet012ptsum',sigjet012ptsum )
    t.SetBranchAddress('m_jj', m_jj)
    t.SetBranchAddress('m_J', m_J)
    t.SetBranchAddress('Jet_BLight1BPt', Jet_BLight1BPt)
    t.SetBranchAddress('Jet_BLight2BPt',Jet_BLight2BPt )
    t.SetBranchAddress('DeltaRJJ', DeltaRJJ)
    t.SetBranchAddress('DeltaPhiMin3',DeltaPhiMin3)
#    t.SetBranchAddress('DeltaPhiMetTrackMet', DeltaPhiMetTrackMet)
    t.SetBranchAddress('DeltaPhiJJ', DeltaPhiJJ)
    t.SetBranchAddress('DeltaPhiMetJJ', DeltaPhiMetJJ)
    t.SetBranchAddress('HtRatioResolved', HtRatioResolved)
    t.SetBranchAddress('HtRatioMerged', HtRatioMerged)
    t.SetBranchAddress('MetTST_Significance_noPUJets_noSoftTerm', MetTST_Significance_noPUJets_noSoftTerm)
    t.SetBranchAddress('eventNumber', eventNumber)
    t.SetBranchAddress('MetTST_met', MetTST_met)
    t.SetBranchAddress('MetTST_phi', MetTST_phi)
    t.SetBranchAddress('MetTrack_met', MetTrack_met)
    t.SetBranchAddress('Jet_pt', Jet_pt)
    t.SetBranchAddress('Jet_eta', Jet_eta)
    t.SetBranchAddress('Jet_phi', Jet_phi)
    t.SetBranchAddress('Jet_m', Jet_m)
    t.SetBranchAddress('Jet_bjet', Jet_bjet)
    t.SetBranchAddress('ForwardJet_pt', ForwardJet_pt)
    t.SetBranchAddress('ForwardJet_eta', ForwardJet_eta)
    t.SetBranchAddress('ForwardJet_phi', ForwardJet_phi)
    t.SetBranchAddress('ForwardJet_m', ForwardJet_m)
    t.SetBranchAddress('FatJet_pt',FatJet_pt )
    t.SetBranchAddress('FatJet_eta', FatJet_eta)
    t.SetBranchAddress('FatJet_phi', FatJet_phi)
    t.SetBranchAddress('FatJet_m', FatJet_m)
    t.SetBranchAddress('TrackJet_pt',TrackJet_pt )
    t.SetBranchAddress('TrackJet_eta', TrackJet_eta)
    t.SetBranchAddress('TrackJet_phi', TrackJet_phi)
    t.SetBranchAddress('TrackJet_m', TrackJet_m)
    t.SetBranchAddress('TrackJet_bjet', TrackJet_bjet)
    t.SetBranchAddress('TrackJet_isAssociated', TrackJet_isAssociated)

    # add mT_METclosestBJet and mT_METfarestBJet,SetBranchAddress and Jet_MV2c10
    t.SetBranchAddress('Jet_MV2c10',Jet_MV2c10)

    t.SetBranchAddress('TrackJet_1passOR', TrackJet_1passOR)
    t.SetBranchAddress('TrackJet_2passOR', TrackJet_2passOR)
    if not args.isdata:
        t.SetBranchAddress('GenWeight', GenWeight)
        t.SetBranchAddress('GenWeightMCSampleMerging', GenWeightMCSampleMerging)
        t.SetBranchAddress('EleWeight', EleWeight)
        t.SetBranchAddress('MuoWeight', MuoWeight)
        t.SetBranchAddress('TauWeight', TauWeight)
        t.SetBranchAddress('TrackJetWeight', TrackJetWeight)
        t.SetBranchAddress('JetWeightJVT', JetWeightJVT)
        t.SetBranchAddress('JetWeightBTag', JetWeightBTag)
        t.SetBranchAddress('MET_TriggerSF', MET_TriggerSF)


    # Create new tree
    outtree = ROOT.TTree('AppInput', 'AppInput')
    outtree.SetDirectory(0)
 
    # Create branches for tree
    weight=array('d',[0])
    dijet_pt=array('f',[0])
    dijet_eta=array('f',[0])
    dijet_phi=array('f',[0])
    dijet_E=array('f',[0])
    delta_phi=array('f',[0])
    pseudo_score_j1=array('i', [0])
    pseudo_score_j2=array('i', [0])
    if args.region=='m':
        fatjet_pt=array('f',[0])
        fatjet_eta=array('f',[0])
        fatjet_phi=array('f',[0])
        fatjet_E=array('f',[0])
        DeltaRjj=array('f',[0])


    outtree.Branch('eventNumber', eventNumber,'eventNumber/l')
    outtree.Branch('m_jj', m_jj,'m_jj/F')
    outtree.Branch('m_J', m_J,'m_J/F')
    outtree.Branch('MetTST_met', MetTST_met,'MetTST_met/F')
    outtree.Branch('MetTST_phi', MetTST_phi,'MetTST_phi/F')
    outtree.Branch('dijet_pt',dijet_pt,'dijet_pt/F')
    outtree.Branch('dijet_eta',dijet_eta,'dijet_eta/F')
    outtree.Branch('dijet_phi',dijet_phi,'dijet_phi/F')
    outtree.Branch('dijet_E',dijet_E,'dijet_E/F')
    outtree.Branch('delta_phi',delta_phi,'delta_phi/F')
    outtree.Branch('N_Jets04', N_Jets04, 'N_Jets04/I')
    #outtree.Branch('DeltaRJJ', DeltaRJJ, 'DeltaRJJ/F')

    # set outtree branches for new variables

    #outtree.Branch('mT_METclosestBJet', mT_METclosestBJet, 'mT_METclosestBJet/F')
    #outtree.Branch('mT_METfarestBJet', mT_METfarestBJet, 'mT_METfarestBJet/F')
    #outtree.Branch('N_ForwardJets04',N_ForwardJets04,'N_ForwardJets04/I')
    outtree.Branch('pseudo_score_j1',pseudo_score_j1,'pseudo_score_j1/B')
    outtree.Branch('pseudo_score_j2', pseudo_score_j2, 'pseudo_score_j2/B')
    #outtree.Branch('MetTST_Significance_noPUJets_noSoftTerm', MetTST_Significance_noPUJets_noSoftTerm,'MetTST_Significance_noPUJets_noSoftTerm/F')

    if args.region[0]=='m':
        outtree.Branch('fatjet_pt',fatjet_pt,'fatjet_pt/F')
        outtree.Branch('fatjet_eta',fatjet_eta,'fatjet_eta/F')
        outtree.Branch('fatjet_phi',fatjet_phi,'fatjet_phi/F')
        outtree.Branch('fatjet_E',fatjet_E,'fatjet_E/F')
        outtree.Branch('HtRatioMerged',HtRatioMerged,'HtRatioMerged/F')
        outtree.Branch('DeltaRjj',DeltaRjj,'DeltaRjj/F')
    if not args.isdata:
        outtree.Branch('weight', weight, 'weight/D')


    met = []
    jets = []
    fatjet = []
    if args.train or args.val and not args.isdata:
        wt = []

    print "Running %d events" % (t.GetEntries())

    '''
    cutflow = ROOT.TH1F('cutflow', 'cutflow', 12, 0, 12)
    cutflow.GetXaxis().SetBinLabel(1, 'Initial')
    cutflow.GetXaxis().SetBinLabel(2, 'MET trigger')
    cutflow.GetXaxis().SetBinLabel(3, 'Electron veto')
    cutflow.GetXaxis().SetBinLabel(4, 'Muon veto')
    cutflow.GetXaxis().SetBinLabel(5, 'MET>500')
    cutflow.GetXaxis().SetBinLabel(6, '2 associated jets02')
    cutflow.GetXaxis().SetBinLabel(7, 'Not associated tau veto')
    cutflow.GetXaxis().SetBinLabel(8, 'Extended tau veto')
    cutflow.GetXaxis().SetBinLabel(9, 'Non-associated B veto')
    cutflow.GetXaxis().SetBinLabel(10, '2 associated B')
    cutflow.GetXaxis().SetBinLabel(11, 'HTratio<0.57')
    cutflow.GetXaxis().SetBinLabel(12, '|DeltaPhiMin3|>9')
    ''' 

    event_yield=0
    #cutflow_check=0

    start_time = time.time()

    for i in range(t.GetEntries()):

        if (i % 1000 == 0):
            elapsed_time = time.time() - start_time
            tt = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
            print '%s |%s%s| %d/100'%(tt, '>'*(i*100/t.GetEntries()), ' '*(100-i*100/t.GetEntries()), i*100/t.GetEntries())
            sys.stdout.write("\033[F") # Cursor up one line

        t.GetEntry(i)
        
        if not args.isdata and int(DSID) <= 364197 and int(DSID) >= 364100:
            if not abs(GenWeight[0]) < 100: continue

        # resolved region
        if args.region[0] == 'r':

            # preselection
            if not (IsMETTrigPassed[0]): continue
            if not (N_BaselineElectrons[0]==0 and N_SignalElectrons[0]==0): continue
            if not (N_BaselineMuons[0]==0 and N_SignalMuons[0]==0): continue
            if args.region == 'r':
                if not (MetTST_met[0]>150000 and MetTST_met[0]<=500000): continue
            elif args.region=='r_150_200':
                if not (MetTST_met[0]>150000 and MetTST_met[0]<=200000): continue
            elif args.region=='r_200_350':
                if not (MetTST_met[0]>200000 and MetTST_met[0]<=350000): continue
            else:
                if not (MetTST_met[0]>350000 and MetTST_met[0]<=500000): continue
            if not (N_Jets04[0]>=2): continue
           # if not (m_jj[0] > 40000): continue
            if not (m_jj[0] > 50000 and m_jj[0] < 280000): continue
           # if not ((MetTrack_met[0]>=30000 and N_BJets_04[0]<2) or N_BJets_04[0]>=2): continue
            if not (Jet_BLight1BPt[0]>=45000 or Jet_BLight2BPt[0]>=45000): continue
            if not (N_SignalTaus[0]==0): continue
            if not (N_TausExtended_Resolved[0]==0): continue
            if not (DeltaRJJ[0]<1.8): continue
            if not (N_BJets_04[0] ==2): continue
            #if not (HtRatioResolved[0]<=0.37): continue
            if not ((N_Jets04[0] ==2 and sigjet012ptsum[0] >120000) or (N_Jets04[0] >2 and sigjet012ptsum[0] > 150000)): continue
            if not (abs(DeltaPhiMin3[0]) >= math.pi/9): continue
            #cutflow_check+=1
#            if not (abs(DeltaPhiMetTrackMet[0])<=math.pi/2): continue
            if not (abs(DeltaPhiJJ[0])<=math.pi*7/9):continue
            if not (abs(DeltaPhiMetJJ[0])>=math.pi*2/3): continue
            if not (MetTST_Significance_noPUJets_noSoftTerm[0]>=12): continue

            #separate events into training and validation samples
            if args.train and eventNumber[0]%100 >= 50: continue
            if args.val and eventNumber[0]%100 < 50: continue

            event_yield+=1
            
            # weight
            if not args.isdata:
                if not args.train and not args.val:
                    weight[0]=(GenWeight[0]*GenWeightMCSampleMerging[0]*EleWeight[0]*MuoWeight[0]*TauWeight[0]*JetWeightJVT[0]*JetWeightBTag[0]*MET_TriggerSF[0]*xs*kf*fe/SumW)
                else:
                    wt.append((GenWeight[0]*GenWeightMCSampleMerging[0]*EleWeight[0]*MuoWeight[0]*TauWeight[0]*JetWeightJVT[0]*JetWeightBTag[0]*MET_TriggerSF[0]*xs*kf*fe/SumW))

            # jets
            # Ordered by: Central b jets, Central non-b jets, Forward jets. All ordered by pt
            ej = []
            for j in range(0, len(Jet_pt)):
                jetp=ROOT.Math.LorentzVector("ROOT::Math::PtEtaPhiM4D<float>")(Jet_pt[j],Jet_eta[j], Jet_phi[j], Jet_m[j])
                #jetp=ROOT.TLorentzVector()
                #jetp.SetPtEtaPhiM(Jet_pt[j],Jet_eta[j], Jet_phi[j], Jet_m[j])
                Jet_E=jetp.E()
                ej.append([Jet_pt[j], Jet_eta[j], Jet_phi[j], Jet_E, Jet_bjet[j], Jet_MV2c10[j]])
            ej = sorted(ej, key=lambda x: x[0], reverse=True)
            ej = sorted(ej, key=lambda x: x[4], reverse=True)
            pscore=[]
            for j in range(len(ej)):
                jet_mv2c10=ej[j].pop()
                pscore.append(set_pscore(jet_mv2c10))
                ej[j].pop()
            if not len(pscore)>=2:
                pscore=[-999,-999]
            ef = []
            for j in range(0, len(ForwardJet_pt)):
                jetp = ROOT.Math.LorentzVector("ROOT::Math::PtEtaPhiM4D<float>")(ForwardJet_pt[j],ForwardJet_eta[j], ForwardJet_phi[j], ForwardJet_m[j])
                #jetp=ROOT.TLorentzVector()
                #jetp.SetPtEtaPhiM(ForwardJet_pt[j],ForwardJet_eta[j], ForwardJet_phi[j], ForwardJet_m[j])
                Jet_E=jetp.E()
                ef.append([ForwardJet_pt[j], ForwardJet_eta[j], ForwardJet_phi[j], Jet_E])
            ef = sorted(ef, key=lambda x: x[0], reverse=True)
            ej=ej+ef
            if len(ej)>=2:
                j1p=ROOT.Math.LorentzVector("ROOT::Math::PtEtaPhiE4D<float>")(ej[0][0],ej[0][1],ej[0][2],ej[0][3])
                j2p=ROOT.Math.LorentzVector("ROOT::Math::PtEtaPhiE4D<float>")(ej[1][0],ej[1][1],ej[1][2],ej[1][3])
                #j2p=ROOT.TLorentzVector()
                #j1p.SetPtEtaPhiE(ej[0][0],ej[0][1],ej[0][2],ej[0][3])
                #j2p.SetPtEtaPhiE(ej[1][0],ej[1][1],ej[1][2],ej[1][3])
                DiJetp=j1p+j2p
                dijetp=[[DiJetp.Pt(),DiJetp.Eta(),DiJetp.Phi(),DiJetp.E()]]
                delta_phi[0]=ROOT.TVector2.Phi_mpi_pi(MetTST_phi[0] - dijetp[0][2])
            elif len(ej)==1:
                ej.append([-999]*4)
                dijetp=[[-999]*4]
                delta_phi[0]=-999
            else:
                ej=[[-999]*4]*2
                dijetp=[[-999]*4]
                delta_phi[0]=-999

            # met and extra
            em = []
            if (len(MetTST_met) > 0):
                em.append([MetTST_met[0], delta_phi[0], N_Jets04[0], HtRatioResolved[0],pscore[0],pscore[1]])
            met.append(em)

            if not args.train and not args.val:
                dijet_pt[0]=dijetp[0][0]
                dijet_eta[0]=dijetp[0][1]
                dijet_phi[0]=dijetp[0][2]
                dijet_E[0]=dijetp[0][3]
                [pseudo_score_j1[0], pseudo_score_j2[0]] = pscore[0:2]
                '''if event_yield%10==0 and args.test:
                    #entries=outtree.GetEntry(0)
                    #print 'entry', entries
                    #tree=outtree.uproot.tree.TTreeMethods()
                    outtree_array=[eventNumber[0], m_jj[0], m_J[0], MetTST_met[0], MetTST_phi [0], dijet_pt[0],dijet_eta[0],dijet_phi[0],dijet_E[0],N_Jets04[0],pseudo_score_j1[0],pseudo_score_j2[0]]
                    print "Current outtree array: ", outtree_array'''
            dijetp[0].pop()
            dijetp[0].pop()
            jets.append(dijetp)

            #if event_yield%10==0 and args.test: print "Current met array: ", met[event_yield-1], "\nCurrent jet array: ", jets[event_yield-1], "\n"

            pass


        # merged region
        else:
            # preselection
            if not (m_J[0] > 50000 and m_J[0] < 270000): continue
            #cutflow.Fill(0)
            if not (IsMETTrigPassed[0]): continue
            #cutflow.Fill(1)
            if not (N_BaselineElectrons[0]==0 and N_SignalElectrons[0]==0): continue
            #cutflow.Fill(2)
            if not (N_BaselineMuons[0]==0 and N_SignalMuons[0]==0): continue
            #cutflow.Fill(3)
            if args.region == 'm':
                if not (MetTST_met[0]>500000): continue
            elif args.region == 'm_500_900':
                if not (MetTST_met[0]>500000 and MetTST_met[0]<=900000): continue
            elif args.region == 'm_900':
                if not (MetTST_met[0]>900000): continue
            #cutflow.Fill(4)
            if not (N_associated_Jets02[0]>=2): continue
            #cutflow.Fill(5)
            #if not (N_Jets10[0]>=1): continue
           # if not ((MetTrack_met[0]>=30000 and N_BTags_associated_02[0]<2) or N_BTags_associated_02[0]>=2): continue
            if not (N_not_associated_Taus[0]==0): continue
            #cutflow.Fill(6)
            if not (N_TausExtended_Merged[0]==0): continue
            #cutflow.Fill(7)
            if not (N_BTags_not_associated_02[0] == 0): continue
            #cutflow.Fill(8)
            #if not (N_BTags_associated_02[0] ==2): continue
            if not (TrackJet_1isBjet[0] and TrackJet_2isBjet[0]): continue
            #cutflow.Fill(9)
            #if args.region in ['m', 'm_900'] and not (HtRatioMerged[0]<=0.57): continue
            #cutflow.Fill(10)
            if not (abs(DeltaPhiMin3[0]) >= math.pi/9): continue
            #cutflow.Fill(11)
#            if not (abs(DeltaPhiMetTrackMet[0])<=math.pi/2): continue
            if not (TrackJet_1passOR[0] == 1 and TrackJet_2passOR[0] == 1): continue

            #separate events into training and validation samples
            if (args.train and eventNumber[0]%100 >= 50): continue
            if (args.val and eventNumber[0]%100 < 50): continue
            #if (args.val and eventNumber[0]%100 >= 80): continue

            event_yield+=1
            
            # weight
            if not args.isdata:
                if not args.train and not args.val:
                    weight[0] = GenWeight[0]*GenWeightMCSampleMerging[0]*EleWeight[0]*MuoWeight[0]*TauWeight[0]*TrackJetWeight[0]*MET_TriggerSF[0]*xs*kf*fe/SumW
                else:
                    wt.append(GenWeight[0]*GenWeightMCSampleMerging[0]*EleWeight[0]*MuoWeight[0]*TauWeight[0]*TrackJetWeight[0]*MET_TriggerSF[0]*xs*kf*fe/SumW)

            # Track jets
            # Ordered by: Track associated b jets, Track associated non-b jets, Track non-asscociated jets. All ordered by pt
            ej = []
            en = []
            for j in range(0, len(TrackJet_pt)):
                jetp=ROOT.TLorentzVector()
                jetp.SetPtEtaPhiM(TrackJet_pt[j],TrackJet_eta[j], TrackJet_phi[j], TrackJet_m[j])
                Jet_E=jetp.E()
                if TrackJet_isAssociated[j]==1:
                    ej.append([TrackJet_pt[j], TrackJet_eta[j], TrackJet_phi[j], Jet_E, TrackJet_bjet[j]])
                else:
                    en.append([TrackJet_pt[j], TrackJet_eta[j], TrackJet_phi[j], Jet_E])
            ej = sorted(ej, key=lambda x: x[0], reverse=True)
            ej = sorted(ej, key=lambda x: x[4], reverse=True)
            en = sorted(en, key=lambda x: x[0], reverse=True)
            for j in range(0, len(ej)): ej[j].pop()
            ej=ej+en
            if (len(ej) > 2):
                for j in range(len(ej) - 2): ej.pop()
            elif (len(ej) < 2):
                print 'Oppse!!!'
                for j in range(2 - len(ej)): ej.append([-999] * 2)


            '''
            # jets
            # Ordered by: Central b jets, Central non-b jets, Forward jets. All ordered by pt
            ej = []
            for j in range(0, len(Jet_pt)):
                jetp=ROOT.Math.LorentzVector("ROOT::Math::PtEtaPhiM4D<float>")(Jet_pt[j],Jet_eta[j], Jet_phi[j], Jet_m[j])
                #jetp.SetPtEtaPhiM(Jet_pt[j],Jet_eta[j], Jet_phi[j], Jet_m[j])
                Jet_E=jetp.E()
                ej.append([Jet_pt[j], Jet_eta[j], Jet_phi[j], Jet_E, Jet_bjet[j],Jet_MV2c10[j]])
            ej = sorted(ej, key=lambda x: x[0], reverse=True)
            ej = sorted(ej, key=lambda x: x[4], reverse=True)
            for j in range(0, len(ej)): ej[j].pop()
            ef = []
            for j in range(0, len(ForwardJet_pt)):
                jetp=ROOT.Math.LorentzVector("ROOT::Math::PtEtaPhiM4D<float>")(ForwardJet_pt[j],ForwardJet_eta[j], ForwardJet_phi[j], ForwardJet_m[j])
                #jetp.SetPtEtaPhiM(ForwardJet_pt[j],ForwardJet_eta[j], ForwardJet_phi[j], ForwardJet_m[j])
                Jet_E=jetp.E()
                ef.append([ForwardJet_pt[j], ForwardJet_eta[j], ForwardJet_phi[j], Jet_E])
            ef = sorted(ef, key=lambda x: x[0], reverse=True)
            ej=ej+ef

            if len(ej)>=2:
                j1p=ROOT.Math.LorentzVector("ROOT::Math::PtEtaPhiE4D<float>")(ej[0][0],ej[0][1],ej[0][2],ej[0][3])
                j2p=ROOT.Math.LorentzVector("ROOT::Math::PtEtaPhiE4D<float>")(ej[1][0],ej[1][1],ej[1][2],ej[1][3])
                #j1p.SetPtEtaPhiE(ej[0][0],ej[0][1],ej[0][2],ej[0][3])
                #j2p.SetPtEtaPhiE(ej[1][0],ej[1][1],ej[1][2],ej[1][3])
                DiJetp=j1p+j2p
                dijetp=[[DiJetp.Pt(),DiJetp.Eta(),DiJetp.Phi(),DiJetp.E()]]
                DeltaPhi = ROOT.TVector2.Phi_mpi_pi(MetTST_phi[0] - dijetp[0][2])
            elif len(ej)==1:
                ej.append([-999]*4)
                dijetp=[[-999]*4]
                DeltaPhi=-999
            else:
                ej=[[-999]*4]*2
                dijetp=[[-999]*4]
                #DeltaPhi=-999
            '''

            #jet1p=ROOT.Math.LorentzVector("ROOT::Math::PtEtaPhiE4D<float>")(jet1_pt[0],jet1_eta[0],jet1_phi[0],jet1_E[0])
            jet1p=ROOT.TLorentzVector()
            jet1p.SetPtEtaPhiE(ej[0][0],ej[0][0],ej[0][0],ej[0][0])
            #jet2p=ROOT.Math.LorentzVector("ROOT::Math::PtEtaPhiE4D<float>")(jet2_pt[0],jet2_eta[0],jet2_phi[0],jet2_E[0])
            jet2p=ROOT.TLorentzVector()
            jet2p.SetPtEtaPhiE(ej[1][0],ej[1][0],ej[1][0],ej[1][0])
            DeltaRjj[0]=jet1p.DeltaR(jet2p)

            #jet3_pt[0]=ej[2][0]
            #jet3_eta[0]=ej[2][1]
            #jet3_phi[0]=ej[2][2]
            #jet3_E[0]=ej[2][3]
            #jet4_pt[0]=ej[3][0]
            #jet4_eta[0]=ej[3][1]
            #jet4_phi[0]=ej[3][2]
            #jet4_E[0]=ej[3][3]
            #dijet_pt[0]=dijetp[0][0]
            #dijet_eta[0]=dijetp[0][1]
            #dijet_phi[0]=dijetp[0][2]
            #dijet_E[0]=dijetp[0][3]

            #dijetp[0].pop()
            #dijetp[0].pop()
            #jets.append(dijetp)

            # fat jet
            # Ordered by: Track associated b jets, Track associated non-b jets, Track non-asscociated jets. All ordered by pt
            eJ = []
            for j in range(0, len(FatJet_pt)):
                jetp=ROOT.Math.LorentzVector("ROOT::Math::PtEtaPhiM4D<float>")(FatJet_pt[j],FatJet_eta[j], FatJet_phi[j], FatJet_m[j])
                #jetp.SetPtEtaPhiM(FatJet_pt[j],FatJet_eta[j], FatJet_phi[j], FatJet_m[j])
                Jet_E=jetp.E()
                eJ.append([FatJet_pt[j], FatJet_eta[j], FatJet_phi[j], Jet_E])
            eJ = sorted(eJ, key=lambda x: x[0], reverse=True)
            if (len(eJ) > 1):
                for j in range(len(eJ) - 1): eJ.pop()
            elif (len(eJ) < 1):
                for j in range(1 - len(eJ)): eJ.append([-999] * 4)

            if not args.train and not args.val:
                fatjet_pt[0]=eJ[0][0]
                fatjet_eta[0]=eJ[0][1]
                fatjet_phi[0]=eJ[0][2]
                fatjet_E[0]=eJ[0][3]
            delta_phi[0] = ROOT.TVector2.Phi_mpi_pi(MetTST_phi[0] - eJ[0][2])

            eJ[0].pop()
            eJ[0].pop()
            fatjet.append(eJ)

            # mets
            em = []
            if (len(MetTST_met) > 0): em.append(
                [MetTST_met[0], delta_phi[0], N_Jets04[0], DeltaRjj[0], HtRatioMerged[0]])
            met.append(em)


        if not args.train and not args.val:
            outtree.Fill()
    
    elapsed_time = time.time() - start_time
    tt = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
    print '%s |%s| 100/100'%(tt, '>'*100)

    print "INFO: The event yield after preselection is: ", event_yield

    # If the arrays are not empty, then convert them into numpy arrays and save them

    #output_container = args.name.split('/')[0]
    category=args.category     
    if not os.path.isdir('arrays'):
        print 'INFO: Creating output folder: \"arrays\"'
        os.makedirs("arrays")
    if not os.path.isdir('arrays/'+category):
        print 'INFO: Creating new arrays container: '+ category
        os.makedirs("arrays/"+category)
        
  
    array_basename=args.name.split('/')[1].replace('.root'+args.name.split('.root')[-1],'').replace('user.amatic.', '').replace('user.anghosh.','').replace('.XAMPP.','')
    if '0900d' in container and 'mc16' in container:
        array_basename='mc16d_'+array_basename
    elif '0900a' in container and 'mc16' in container:
        array_basename='mc16a_'+array_basename
    elif ('0900e' in container or '0902e' in container) and 'mc16' in container:
        array_basename='mc16e_'+array_basename
    elif 'data15' in container:
        array_basename='data15_'+array_basename
    elif 'data16' in container:
        array_basename='data16_'+array_basename
    elif 'data17' in container:
        array_basename='data17_'+array_basename
    elif 'data18' in container:
        array_basename='data18_'+array_basename
    else:
        print 'WARNING: File does not match with a tipical file name!!'

    if args.region[0] == 'm':
        region = args.region.replace('m', 'merged')
    else:
        region = args.region.replace('r', 'resolved')

    if event_yield==0:
        print "WARNING: No event left after the preselections. Will not save the results."
        with open('arrays/%s/%s%s%s_%s.txt' % (category, array_basename, '_train' if args.train else '', '_val' if args.val else '', region),"w") as txt:
            txt.write('%s%s%s_%s.npy\n' % (array_basename, '_train' if args.train else '', '_val' if args.val else '', region))
    else:
        if args.train or args.val and not args.isdata:
            wt = np.array(wt)
            print 'Saving numpy arrays of weight to "arrays/%s"...' % (category)
            np.save('arrays/%s/%s%s%s_%s_weight.npy' % (category, array_basename, '_train' if args.train else '', '_val' if args.val else '', region), wt)

        met = np.array(met)
        print 'Saving numpy arrays of MET to \"arrays/%s\"...' % (category)
        np.save('arrays/%s/%s%s%s_%s_met.npy' % (category, array_basename, '_train' if args.train else '', '_val' if args.val else '', region), met)

        if args.region[0] == 'r':
            jets = np.array(jets)
            print 'Saving numpy arrays of jets to \"arrays/%s\"...' % (category)
            np.save('arrays/%s/%s%s%s_%s_jets.npy' % (category, array_basename, '_train' if args.train else '', '_val' if args.val else '', region), jets)

        elif args.region[0] == 'm':
            fatjet = np.array(fatjet)
            print 'Saving numpy arrays of fatjet to \"arrays/%s\"...' % (category)
            np.save('arrays/%s/%s%s%s_%s_Fatjet.npy' % (category, array_basename, '_train' if args.train else '', '_val' if args.val else '', region), fatjet)

    #if 1==1:    
        if not args.train and not args.val:
            if not os.path.isdir('AppInputs'):
                print 'INFO: Creating new folder: \"AppInputs\"'
                os.makedirs("AppInputs")
            if not os.path.isdir('AppInputs/'+category):
                print 'INFO: Creating new AppInputs container: '+ category
                os.makedirs("AppInputs/"+category)

            print 'Saving AppInputs to \"AppInputs/%s\"...' % (category)
            outfile=ROOT.TFile('AppInputs/%s/%s_%s.root' %(category, array_basename, region), 'RECREATE')
            outtree.Write()
            #cutflow.Write()
            outfile.Close()

    return


def main():
    """Run numpy arrays production for MonoHbb XGBoost analysis."""

    args=getArgs()
    if args.train and args.val: 
        print 'ERROR: Please select only one running section!! (either --val or --train)'
        print 'Exiting...'
        quit()
    #if not (args.region == 'm' or args.region == 'r'):
    #    print 'ERROR: The region is not defined correctly. Please select from \'m\' and \'r\'.'
    #    print 'Exiting...'
    #    quit()
    if not os.path.isfile(args.inputdir+"/"+args.name):
        print "ERROR: The input file %s doesn\'t exist!!" % (args.inputdir+"/"+args.name)
        print 'Exiting...'
        quit()
    if args.train:
        section='training'
    elif args.val:
        section='validation'
    else:
        section='general'
    print "========================================================="
    print "Processing %s" % args.name
    print "========================================================="
    print ""
    print "INFO: Making %s samples" % section
    if args.region[0] =='m': print "INFO: Preselection for the merged region. %s" %args.region
    else: print "INFO: Preselection for the resolved region."
    start=time.time()
    process_arrays(args)
    stop=time.time()
    if args.test:
        print(stop-start)
    print ""
    print "========================================================="
    print "Finishing the process"
    print "========================================================="


    return

if __name__ == '__main__':
    main()
