#!/usr/bin/env python
#
#
#
#  Created by Jay Chan
#
#  8.13.2018
#
#
#
#
#

import os
from argparse import ArgumentParser
from monohbbml.condor import condor_booklist

def getArgs():
    """Get arguments from command line."""

    parser = ArgumentParser(description="Process input rootfiles into numpy arrays for MonoHbb XGBoost analysis.")
    parser.add_argument('-i', '--inputdir', action='store', default='inputs', help='Directory that contains ntuples.')
    return  parser.parse_args()


def arrange_samplelist(channel,category,inputdir):

    samples=[]
    for filename in os.listdir(inputdir):
        if "XAMPP" not in filename:
            continue
        elif channel not in filename: 
            continue
        else:
            filedir=inputdir+"/"+filename
            for ntuple in os.listdir(filedir):
                if not ".root" in ntuple:
                    continue
                else:
                    samples.append("%s/%s %s"% (filename,ntuple,category))
    return samples

def main():

    args=getArgs()
    inputdir=args.inputdir
    sample_list=[['ttbar',['ttbar']],['W',['Wmunu','Wenu','Wtaunu']],['Z',['Znunu','Ztautau','Zee','Zmumu']],['stop',['stop']],['diboson',['WW','ZZ','WZ']],['VH',['qq', 'ggZ']], ['data', ['data15','data16','data17','data18']]]
    ZP2HDM = [[600, 300], [600, 400], [800, 300], [800, 400], [800, 500], [1000, 400], [1000, 500], [1000, 600], [1200, 500],
              [1200, 600], [1200, 700], [1400, 500], [1400, 600], [1400, 700], [1400, 800], [1600, 300], [1600, 500], [1600, 600],
              [1600, 700], [1600, 800], [1800, 500], [1800, 600], [1800, 700], [1800, 800], [2000, 400], [2000, 500], [2000, 600],
              [2000, 700], [2000, 800], [2200, 300], [2200, 400], [2200, 500], [2200, 600], [2200, 700], [2400, 300], [2400, 400],
              [2400, 500], [2400, 600], [2400, 700], [400, 300], [400, 400], [2600, 300], [2600, 400], [2600, 500], [2600, 600],
              [2600, 700], [2800, 300], [2800, 400], [2800, 500], [2800, 600], [3000, 200], [3000, 300], [3000, 400], [3000, 500], [3000, 600]]
    A2HDM =  [[300, 150], [400, 250], [600, 350], [800, 500],
              [1000, 150], [1100, 250], [1200, 350], [1300, 500],
              [1400, 150], [1600, 250], [1600, 350], [1800, 150]]

    condor_list = {}
    for j in range(len(sample_list)):

        category=sample_list[j][0]

        for channel in sample_list[j][1]:

            condor_list[channel] = condor_booklist('test_new_Submit.sh', 'ProcessArrays', channel)
            condor_list[channel].initialdir_in_arguments()
            condor_list[channel].set_JobFlavour('longlunch')
            samples = arrange_samplelist(channel,category,inputdir)
            condor_list[channel].add_Argument(samples)
            condor_list[channel].submit()

    
    condor_list['zp2hdm'] = condor_booklist('test_new_Submit.sh', 'ProcessArrays', 'zp2hdm')
    condor_list['zp2hdm'].initialdir_in_arguments()
    condor_list['zp2hdm'].set_JobFlavour('longlunch')

    for massPoint in ZP2HDM:

        MZP = str(massPoint[0])
        MA  = str(massPoint[1])
        channel = 'zp2hdmbbmzp'+ MZP + 'mA' + MA

        samples=arrange_samplelist(channel,channel,inputdir)
        condor_list['zp2hdm'].add_Argument(samples)

    #condor_list['zp2hdm'].summary('Basic')
    condor_list['zp2hdm'].submit()


    condor_list['2hdma'] = condor_booklist('test_new_Submit.sh', 'ProcessArrays', '2hdma')
    condor_list['2hdma'].initialdir_in_arguments()
    condor_list['2hdma'].set_JobFlavour('longlunch')
    
    for massPoint in A2HDM:

        MA = str(massPoint[0])
        Ma  = str(massPoint[1])

        for production in ['2HDMa_bb_tb10', '2HDMa_ggF_tb1']:

            channel = production + '_sp035_mA'+ MA + '_ma' + Ma

            samples=arrange_samplelist(channel,channel,inputdir)
            condor_list['2hdma'].add_Argument(samples)
    
    #condor_list['2hdma'].summary('Basic')
    condor_list['2hdma'].submit()
    
if __name__=='__main__':
    main()

