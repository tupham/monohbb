#!/usr/bin/env python
#
#
#
#  Created by Jay Chan
#
#  8.13.2018
#
#
#
#
#

import os
from datetime import datetime
from argparse import ArgumentParser
from monohbbml.condor import condor_booklist

def getArgs():
    """Get arguments from command line."""
    parser = ArgumentParser(description="Process input rootfiles into numpy arrays for MonoHbb XGBoost analysis.")
    parser.add_argument('-i', '--inputdir', action='store', default='inputs', help='Directory that contains ntuples.')
    return  parser.parse_args()

def main():
    args=getArgs()
    date = datetime.now().strftime("%Y-%m-%d-%H-%M")
    inputdir=args.inputdir
    sample_list=['ttbar','ttbar','Wmunu','Wenu','Wtaunu','Znunu','Ztautau','Zee','Zmumu','stop','WW','ZZ','WZ', 'qq', 'gg_', 'ggZ', 'data15','data16','data17', 'data18']
    ZP2HDM = [[600, 300], [600, 400], [800, 300], [800, 400], [800, 500], [1000, 400], [1000, 500], [1000, 600], [1200, 500],
              [1200, 600], [1200, 700], [1400, 500], [1400, 600], [1400, 700], [1400, 800], [1600, 300], [1600, 500], [1600, 600],
              [1600, 700], [1600, 800], [1800, 500], [1800, 600], [1800, 700], [1800, 800], [2000, 400], [2000, 500], [2000, 600],
              [2000, 700], [2000, 800], [2200, 300], [2200, 400], [2200, 500], [2200, 600], [2200, 700], [2400, 300], [2400, 400],
              [2400, 500], [2400, 600], [2400, 700], [400, 300], [400, 400], [2600, 300], [2600, 400], [2600, 500], [2600, 600],
              [2600, 700], [2800, 300], [2800, 400], [2800, 500], [2800, 600], [3000, 200], [3000, 300], [3000, 400], [3000, 500], [3000, 600]]
    A2HDM =  [[300, 150], [400, 250], [600, 350], [800, 500],
              [1000, 150], [1100, 250], [1200, 350], [1300, 500],
              [1400, 150], [1600, 250], [1600, 350], [1800, 150]]


    condor_list= {}
    for channel in sample_list:

        condor_list[channel] = condor_booklist('SubmitRecoverMissingFiles.sh', 'RecoverMissingFiles', channel)
        condor_list[channel].initialdir_in_arguments()
        condor_list[channel].set_JobFlavour('longlunch')
    
    for massPoint in ZP2HDM:
        MZP = str(massPoint[0])
        MA  = str(massPoint[1])
        channel = 'zp2hdmbbmzp'+ MZP + 'mA' + MA
        condor_list[channel] = condor_booklist('SubmitRecoverMissingFiles.sh', 'RecoverMissingFiles', channel)
        condor_list[channel].initialdir_in_arguments()
        condor_list[channel].set_JobFlavour('longlunch')

    for massPoint in A2HDM:

        MA = str(massPoint[0])
        Ma  = str(massPoint[1])

        for production in ['2HDMa_bb_tb10', '2HDMa_ggF_tb1']:

            channel = production + '_sp035_mA'+ MA + '_ma' + Ma
            condor_list[channel] = condor_booklist('SubmitRecoverMissingFiles.sh', 'RecoverMissingFiles', channel)
            condor_list[channel].initialdir_in_arguments()
            condor_list[channel].set_JobFlavour('longlunch')

    text_file = open("arrays/missing_samplelist.txt", "r")
    lines = text_file.read().splitlines()
    for line in lines:
        args=str(line).split(' ')
        #print args
        if 'train' in args[2]:
            section='t'
        elif 'val' in args[2]:
            section='v'
        else:
            section='none'
        if 'merged' in args[3]:
            region=args[3].replace('merged', 'm')
        elif 'resolved' in args[3]:
            region=args[3].replace('resolved', 'r')

        condor_list[args[5]].add_Argument("%s/%s %s %s %s"% (args[0],args[1],section,region,args[4]))
        
    for channel in condor_list:
        condor_list[channel].submit()
    
if __name__=='__main__':
    main()
