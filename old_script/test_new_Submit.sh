#!/bin/bash
#
#
#
#    Created by Jay Chan
#
#        8.13.2018
#
#
#
#
initdir=$1
filename=$2
category=$3


cd $initdir
source ./setup_lxplus.sh
echo python process_arrays.py -n $filename $runsection -r $region -c $category
if [[ "$category" == *data* ]]; then
python test_new_process.py -n $filename -c $category -d
else
python test_new_process.py -n $filename -c $category
fi
#!/usr/bin/env bash