#!/usr/bin/env python
#  Created by Tuan Pham
#
#  July 27
import os
from argparse import ArgumentParser
import pandas as pd
from monohbbml.parameters import *

def getArgs():
    """Get arguments from command line."""
    parser = ArgumentParser()
    parser.add_argument('--checkonly', action='store_true', default=False, help='Not save the text file')
    parser.add_argument('--inputdir',action='store',default='inputs')
    parser.add_argument('--checkdir',action='store',default='Preselected_samples')
    parser.add_argument('-cat','--category', action='store',nargs='+',default=['all'],choices=['all', 'VH','W','Z','diboson','ttbar','stop','data','ZP2HDM','A2HDM'],help='Categories to check.')
    return  parser.parse_args()

def Check_by_name(args):
    inputdir=args.inputdir
    checkdir=args.checkdir
    categories=args.category
    # find root files in check dir and check for duplication of names
    files=[]
    for root, path, file in os.walk(checkdir):
        files+=[f for f in file if '.root' in f]
    # create a dictionary containing output file names, with input containers as keys.
    namebook=Data_sample_to_output_name_dict(inputdir)
    catbook=Data_sample_to_category_dict(inputdir)
    processing_list={}
    for container in catbook.keys():
        if category_dict[container]==category:
            group=Root_name(container).split('_')[0]
            if group not in processing_list.keys(): processing_list[group]=[]
            processing_list[group].append(container)
    for file in files:
        if not file in unique_files:
            unique_files.append(file)
        else:
            duplicated_files.append(file)
    if len(duplicated_files)!=0:
        print('Duplicated files exist.')
        for duplicated_file in duplicated_files:
            print('File: ', duplicated_file)
            print('Duplicated input container: ', [container for container in os.listdir(inputdir) if Root_name(container)==duplicated_file])
        raise SystemExit('Please check your naming system before moving on, lest some bitter flavour of utter confusion adds to your already difficult life!')

    # check if all elements in files are in namebook
    junkfile=[file for file in files if file not in [namebook[key] for key in namebook.keys()]]
    if len(junkfile)!=0:
        print('Some output files are not appropriately named!')
        print(junkfile)

    # create missing sample dictionary, with input containers as keys
    missing_samples={}

    real_file=[]
    if 'all' in categories:
        for container in namebook.keys():
            category=catbook[container]
            if category not in missing_samples.keys(): missing_samples[category]={}
            if namebook[container] not in files:
                missing_samples[category][container]=namebook[container]
            else: real_file+=[namebook[container]]
        input=namebook
    else:
        for category in categories:
            for container in namebook.keys():
                if category!=catbook[container]: continue
                if category not in missing_samples.keys(): missing_samples[category]={}
                if namebook[container] not in files:
                    missing_samples[category][container]=namebook[container]
                else: real_file+=[namebook[container]]
        input=[container for container in catbook.keys() if catbook[container] in categories]
    # print out missing samples
    for category in missing_samples.keys():
        if len(missing_samples[category])==0: missing_samples.pop(category)
    if len(missing_samples)!= 0:
        print('Missing files:')
    for category in missing_samples.keys():
        print '-'*50
        print category
        for container in missing_samples[category].keys():
            print(missing_samples[category][container])
            print('Original container: %s' % container)
            print '.'*50

    print('#'*80)
    count=0
    for category in missing_samples.keys():
        count+=len(missing_samples[category].keys())

    print('%d output files exist in total.' % len(real_file))
    print('%d missing files in total.' % count)
    print('%d input containers in checked categories.' % len(input))

    return missing_samples

def Check_HDF(args):
    inputdir=args.inputdir
    checkdir=args.checkdir
    categories=args.category
    keys={}
    container_dict=Data_sample_to_output_name_dict(inputdir)
    root_dict={}
    for container in container_dict.keys():
        root_dict[container_dict[container]]=container
    for category in [dir for dir in os.listdir(checkdir) if os.path.isdir(checkdir+'/'+dir)]:
        for root in [root for root in os.listdir(checkdir+'/'+category) if root.endswith('.root')]:
            key='/Resolved/'+category+'/'+root.replace('.root','')
            keys[key]=root_dict[root]
    missing_keys={}
    with pd.HDFStore('Preselected_samples/Vector_like.h5') as hdf:
        hdf_keys=hdf.keys()
        for missing_key in set(keys.keys())-set(hdf_keys):
            missing_keys[missing_key]=keys[missing_key]
    if len(missing_keys)!=0:
        print 'Vector_like.h5 contains some missing keys, as follow: '
        for missing_key in missing_keys.keys():
            print missing_key
    return missing_keys


def main():
    args=getArgs()
    missing_samples=Check_by_name(args)
    # save to txt
    if not args.checkonly:
        with open('Preselected_samples/missing_samplelist.txt', 'w') as txt:
            for category in missing_samples.keys():
                for container in missing_samples[category].keys():
                    txt.write(container+' '+missing_samples[category][container]+' '+category+'\n')
    missing_keys=Check_HDF(args)
    with open('Preselected_samples/hdf_missing_samplelist.txt', 'w') as txt:
        for missing_key in missing_keys.keys():
            txt.write(missing_keys[missing_key]+' '+missing_key+'\n')

if __name__=='__main__':
    main()
