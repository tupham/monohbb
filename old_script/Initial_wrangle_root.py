#  Created by Tuan Pham
#  July 26th 2019
import os
import math
from argparse import ArgumentParser
import numpy as np
from time import time, sleep
import uproot
import pandas as pd
from root_pandas import *
from monohbbml.parameters import *
from array import array

def getArgs():
    parser = ArgumentParser(description="Process input rootfiles into numpy arrays for MonoHbb XGBoost analysis.")
    parser.add_argument('-i', '--inputdir', action='store', default='inputs', help='Directory that contains ntuples.')
    parser.add_argument('-s','--saveto',action='store',default='Preselected_samples',help='Name of directory to save output files')
    parser.add_argument('--test', action='store_true',default=False)
    parser.add_argument('--type', choices=['ZP2HDM', 'A2HDM','Z','W','ttbar','VH','diboson','stop','data'], default=['Z','W','ttbar','VH','diboson','stop', 'ZP2HDM', 'A2HDM'], action='store', nargs='*')
    return  parser.parse_args()

def preselection(data,region):
    # preselection. Drop all rows in the dataframe that don't satisfy conditional tests. Keys: ~ = not, & = and, | = or. Consult https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#boolean-indexing for details
    IsMETTrigPassed = array('B', [0])
    N_SignalElectrons = array('I', [0])
    N_SignalMuons = array('I',[0])
    N_BaselineElectrons = array('I',[0])
    N_BaselineMuons = array('I',[0])
    N_SignalTaus = array('I', [0])
    N_not_associated_Taus = array('I', [0])
    N_TausExtended_Merged = array('I', [0])
    N_TausExtended_Resolved = array('I', [0])
    N_Jets04 = array('I', [0])
    N_associated_Jets02 = array('I', [0])
    N_BJets_04 = array('I', [0])
    N_BTags_associated_02 = array('I', [0])
    N_BTags_not_associated_02 = array('I', [0])
    TrackJet_1isBjet = array('B', [0])
    TrackJet_2isBjet = array('B', [0])
    sigjet012ptsum = array('f', [0])
    m_jj = array('f', [0])
    m_J = array('f', [0])
    Jet_BLight1BPt = array('f', [0])
    Jet_BLight2BPt = array('f', [0])
    DeltaRJJ = array('f', [0])
    DeltaPhiMin3 = array('f', [0])
    DeltaPhiJJ = array('f', [0])
    DeltaPhiMetJJ = array('f', [0])
    MetTST_Significance_noPUJets_noSoftTerm = array('f', [0])
    eventNumber = array('L', [0])
    MetTST_met = array('f', [0])
    MetTST_phi = array('f', [0])
    MetTrack_met = array('f', [0])
    TrackJet_1passOR = array('B', [0])
    TrackJet_2passOR = array('B', [0])

    data.SetBranchAddress('IsMETTrigPassed',IsMETTrigPassed)
    data.SetBranchAddress('N_SignalElectrons',N_SignalElectrons)
    data.SetBranchAddress('N_SignalMuons',N_SignalMuons)
    data.SetBranchAddress('N_BaselineElectrons',N_BaselineElectrons)
    data.SetBranchAddress('N_BaselineMuons',N_BaselineMuons)
    data.SetBranchAddress('N_SignalTaus',N_SignalTaus)
    data.SetBranchAddress('N_not_associated_Taus',N_not_associated_Taus)
    data.SetBranchAddress('N_TausExtended_Merged',N_TausExtended_Merged)
    data.SetBranchAddress('N_TausExtended_Resolved',N_TausExtended_Resolved)
    data.SetBranchAddress('N_Jets04',N_Jets04)
    data.SetBranchAddress('N_associated_Jets02',N_associated_Jets02)
    data.SetBranchAddress('N_BJets_04',N_BJets_04)
    data.SetBranchAddress('N_BTags_associated_02',N_BTags_associated_02)
    data.SetBranchAddress('N_BTags_not_associated_02',N_BTags_not_associated_02)
    data.SetBranchAddress('sigjet012ptsum',sigjet012ptsum)
    data.SetBranchAddress('m_jj',m_jj)
    data.SetBranchAddress('m_J',m_J)
    data.SetBranchAddress('Jet_BLight1BPt',Jet_BLight1BPt)
    data.SetBranchAddress('Jet_BLight2BPt',Jet_BLight2BPt)
    data.SetBranchAddress('DeltaRJJ',DeltaRJJ)
    data.SetBranchAddress('DeltaPhiMin3',DeltaPhiMin3)
    data.SetBranchAddress('DeltaPhiJJ',DeltaPhiJJ)
    data.SetBranchAddress('DeltaPhiMetJJ',DeltaPhiMetJJ)
    data.SetBranchAddress('MetTST_Significance_noPUJets_noSoftTerm',MetTST_Significance_noPUJets_noSoftTerm)
    data.SetBranchAddress('eventNumber',eventNumber)
    data.SetBranchAddress('MetTST_met',MetTST_met)
    data.SetBranchAddress('MetTST_phi',MetTST_phi)
    data.SetBranchAddress('MetTrack_met',MetTrack_met)
    data.SetBranchAddress('TrackJet_1passOR',TrackJet_1passOR)
    data.SetBranchAddress('TrackJet_2passOR',TrackJet_2passOR)
    data.SetBranchAddress('TrackJet_1isBjet',TrackJet_1isBjet)
    data.SetBranchAddress('TrackJet_2isBjet',TrackJet_2isBjet)
    if region=='resolved':
        en=[]
        for i in range(data.GetEntries()):
            data.GetEntry(i)
            if not (N_BJets_04[0] > 2): continue
            if not (IsMETTrigPassed[0]): continue
            if not (N_BaselineElectrons[0]==0 and N_SignalElectrons[0]==0): continue
            if not (N_BaselineMuons[0]==0 and N_SignalMuons[0]==0): continue
            if not (MetTST_met[0]>150000. and MetTST_met[0]<=500000.): continue
            #if not (N_Jets04[0]>=2): continue
            if not (m_jj[0] > 50000 and m_jj[0] < 280000): continue
            if not (Jet_BLight1BPt[0]>=45000 or Jet_BLight2BPt[0]>=45000): continue
            if not (N_SignalTaus[0]==0): continue
            if not (N_TausExtended_Resolved[0]==0): continue
            if not (DeltaRJJ[0]<1.8): continue
            if not ((N_Jets04[0] ==2 and sigjet012ptsum[0] >120000) or (N_Jets04[0] >2 and sigjet012ptsum[0] > 150000)): continue
            if not (abs(DeltaPhiMin3[0]) >= math.pi/9): continue
            if not (abs(DeltaPhiJJ[0])<=math.pi*7/9):continue
            if not (abs(DeltaPhiMetJJ[0])>=math.pi*2/3): continue
            if not (MetTST_Significance_noPUJets_noSoftTerm[0]>=12): continue
            en+=[eventNumber[0]]
    else:
        en=[]
        for i in range(data.GetEntries()):
            data.GetEntry(i)
            if not (m_J[0] > 50000 and m_J[0] < 270000): continue
            if not (IsMETTrigPassed[0]): continue
            if not (N_BaselineElectrons[0]==0 and N_SignalElectrons[0]==0): continue
            if not (N_BaselineMuons[0]==0 and N_SignalMuons[0]==0): continue
            if not (MetTST_met[0]>500000): continue
            if not (N_associated_Jets02[0]>=2): continue
            if not (N_not_associated_Taus[0]==0): continue
            if not (N_TausExtended_Merged[0]==0): continue
            if not (N_BTags_not_associated_02[0] == 0): continue
            if not (TrackJet_1isBjet[0] and TrackJet_2isBjet[0]): continue
            if not (abs(DeltaPhiMin3[0]) >= math.pi/9): continue
            if not (TrackJet_1passOR[0] == 1 and TrackJet_2passOR[0] == 1): continue
            en += [eventNumber[0]]
    return en

def get_SumW(meta_data_tree):
    # takes in a dataframe (data_frame) and a meta_data_tree, the sample's DSID and container of sample
    # get sum of weight from TotalSumW tree
    TotalSumW=meta_data_tree.array('TotalSumW')
    SumW=np.sum(TotalSumW)
    return SumW

def get_weight(SumW,data_frame,DSID,container,region):

    # get cross section, k-factor and filter efficiency
    xs=1
    kf=1
    fe=1
    if 'zp2hdmbb' in container or '2HDMa' in container: pass
    else:
        xsectiontxt=open('data/PMGxsecDB_mc16_signal_bkg.txt','r')
        lines = xsectiontxt.read().splitlines()
        found=False
        for line in lines:
            if line.startswith(DSID):
                xs=float(line.split()[2])
                kf=float(line.split()[4])
                fe=float(line.split()[3])
                found=True
        if not found:
            print('Cant find DSID in PMGxsecDB_mc16_signal_bkg.txt. Assign xs, kf, fe to 1.')

    # compute weight for dataset from SumW, kf, fe, xs
    if region=='resolved':
        weight=data_frame.GenWeight * data_frame.GenWeightMCSampleMerging * data_frame.EleWeight * data_frame.MuoWeight * data_frame.TauWeight * data_frame.JetWeightJVT * data_frame.JetWeightBTag * data_frame.MET_TriggerSF * xs * kf * fe / SumW
    else:
        weight=data_frame.GenWeight * data_frame.GenWeightMCSampleMerging * data_frame.EleWeight * data_frame.MuoWeight * data_frame.TauWeight * data_frame.TrackJetWeight * data_frame.MET_TriggerSF * xs * kf * fe / SumW

    #print 'INFO: Cross-section:     ', xs
    #print 'INFO: k-factor:          ', kf
    #print 'INFO: Filter Efficiency: ', fe
    #print 'Sum of weights:          ', SumW

    return weight

def process_roots(inputdir,isdata,test,category):
    category_dict=Data_sample_to_category_dict(inputdir)
    processing_list={}
    for container in category_dict.keys():
        if category_dict[container]==category:
            group=Root_name(container).split('_')[0]
            if group not in processing_list.keys(): processing_list[group]=[]
            processing_list[group].append(container)

    for group in processing_list.keys():
        # create an empty dataframes for resolved and merged
        data_resolved=pd.DataFrame()
        data_merged=pd.DataFrame()
        for container in sorted(processing_list[group]):
            print 'Processing %s.' % container
            if not os.path.isdir(inputdir+"/"+container):
                raise SystemExit("ERROR: The input file %s doesn\'t exist!! Exiting!" % (inputdir+"/"+container))
            temp_data_resolved=pd.DataFrame()
            temp_data_merged=pd.DataFrame()
            # get data sample ID
            DSID=Data_sample_ID(container)

            # Open input files, convert to dataframes, compute weights and concatenate
            SumW=0
            ntuples=sorted([ntuple for ntuple in os.listdir(inputdir+"/"+container) if ntuple.endswith('.root')])
            for ntuple in ntuples:
                # open input file with root_pandas
                print('Opening %s' % inputdir+"/"+container+'/'+ntuple)
                s=time()
                #roots = read_root(inputdir+"/"+container+'/'+ntuple,key='MonoH_Nominal',chunksize=50000)
                t = ROOT.TFile(inputdir+"/"+container+'/'+ntuple)
                root=t.Get('MonoH_Nominal')

                # get MetaDataTree
                meta_data_tree=uproot.open(inputdir+"/"+container+'/'+ntuple)['MetaDataTree']
                if not isdata:
                    # get weight dataframe from the principle dataframe (root) and MetaData
                    SumW+=get_SumW(meta_data_tree=meta_data_tree)

                # apply preselections by submitting the primary dataframe (data) to preselection module in resolved and merged regions
                en_resolved=preselection(root,'resolved')
                en_merged=preselection(root,'merged')
                for chunk in read_root(inputdir+"/"+container+'/'+ntuple,key='MonoH_Nominal',chunksize=500000):
                    root_resolved=chunk.loc[chunk['eventNumber'].isin(en_resolved)]
                    root_merged=chunk.loc[chunk['eventNumber'].isin(en_merged)]
                    if not isdata and int(DSID) <= 364197 and int(DSID) >= 364100:
                        root_resolved=root_resolved[abs(root_resolved.GenWeight)<100.]
                        root_merged=root_merged[abs(root_merged.GenWeight)<100.]
                    # concatenate primary dataframes (temp_data) with current dataframe (root)
                    temp_data_resolved=pd.concat([temp_data_resolved,root_resolved],ignore_index=True)
                    temp_data_merged=pd.concat([temp_data_merged,root_merged],ignore_index=True)
                print time()-s
                    #print 'Yield from current ntuple in resolved: ', temp_data_resolved.shape[0]
                #print 'Yield from current ntuple in merged: ', temp_data_merged.shape[0]

            if not isdata:
                temp_weight_resolved, temp_weight_merged=get_weight(SumW=SumW,data_frame=temp_data_resolved,DSID=DSID,container=container,region='resolved'),get_weight(SumW=SumW,data_frame=temp_data_merged,DSID=DSID,container=container,region='merged')
                #print 'sumw: ', temp_weight_resolved.sum()
                temp_data_resolved=temp_data_resolved.assign(weight=temp_weight_resolved)
                temp_data_merged=temp_data_merged.assign(weight=temp_weight_merged)

            # append temp_data to data
            data_resolved=pd.concat([data_resolved,temp_data_resolved],ignore_index=True)

            data_merged=pd.concat([data_merged,temp_data_merged],ignore_index=True)
            # number of events passing preslections
            print('Current event yield after preselection: %d in merged and %d in resolved region' % (data_merged.shape[0],data_resolved.shape[0]))
            print 'Finishing %s' % container
            print '.'*100

        event_yield_merged=data_merged.shape[0]
        event_yield_resolved=data_resolved.shape[0]

        # add a column called 'MyeventNumber' which will be used to identify row in later steps
        data_resolved = data_resolved.assign(MyeventNumber=np.arange(event_yield_resolved))
        data_merged = data_merged.assign(MyeventNumber=np.arange(event_yield_merged))
        #print 'sumw: ', data_resolved.weight.sum()

        # split to single-entry variables and vector-like variables. to_root does not support saving the latter. The list of these vector-like variables is located in parameters
        print ''
        print 'Separating data into single-entry and vector-like variables.'
        Vector_variables=set(vector_variables['samples']).intersection(set(data_resolved.keys())) if not 'data' in category else set(vector_variables['data']).intersection(set(data_resolved.keys()))
        data_resolved_vector=data_resolved[Vector_variables|{'MyeventNumber'}]
        data_merged_vector=data_merged[Vector_variables|{'MyeventNumber'}]

        # drop the vector-like variables from main frames.
        data_resolved=data_resolved.drop(columns=Vector_variables)
        data_merged=data_merged.drop(columns=Vector_variables)

        print ''
        print 'Finishing %s' % group
        processing_list[group]={}
        processing_list[group]['Resolved']=[data_resolved,data_resolved_vector]
        processing_list[group]['Merged']=[data_merged,data_merged_vector]
        print '*' * 80

    return processing_list

def main():

    args=getArgs()
    inputdir,types,test,savedir=args.inputdir,args.type,args.test,args.saveto
    count=0
    start=time()
    category_dict=Data_sample_to_category_dict(inputdir)
    Categories=set([category_dict[container] for container in category_dict.keys()])
    #with pd.HDFStore('Preselected_samples/Vector_like.h5') as hdf:

    for type in types:
        if type=='ZP2HDM':
            categories=[cat for cat in Categories if 'zp2hdmbbmzp' in cat]
        elif type=='A2HDM':
            categories=[cat for cat in Categories if '2HDMa' in cat]
        else:
            categories=[type]
        for category in categories:
            count+=1
            print "========================================================="
            print "Processing %s" % category
            print "========================================================="
            savepath=savedir+'/'+category
            hdfname=savedir+'/'+'Vector_like.h5'
            processed_data = process_roots(inputdir=inputdir,isdata=True if category=='data' else False,test=test,category=category)
            print 'Saving processed data...'
            if not os.path.isdir(savepath):
                os.makedirs(savepath)
            for group in processed_data.keys():
                rootname=savepath+'/'+category+'_'+group+'.root'
                if os.path.isfile(rootname): os.remove(rootname)
                for region in processed_data[group].keys():
                    processed_data_group_region=processed_data[group][region][0]
                    processed_data_group_region_vector=processed_data[group][region][1]
                    if processed_data_group_region.empty:
                        processed_data_group_region=pd.DataFrame(data=np.nan,columns=processed_data_group_region.keys(),index=[0])
                        processed_data_group_region_vector=pd.DataFrame(data=np.nan,columns=processed_data_group_region_vector.keys(),index=[0])
                    processed_data_group_region.to_root(rootname,key=region,mode='a')
                    saved=False
                    hdfkey=region+'/'+category+'/'+group
                    while not saved:
                        try:
                            print 'Attempting to save %s to HDF.' % hdfkey
                            processed_data_group_region_vector.to_hdf(hdfname,key=hdfkey,mode='a')
                            print '*** Finish saving %s ***' % hdfkey
                            saved=True
                        except:
                            print 'HDF5 file is being open, saving %s in 15s.' % hdfkey
                            sleep(15)

            stop=time()
            print 'Time elapsed: %d' % (stop-start)
            print '%d categories processed.' % count

    print ""
    print "========================================================="
    print "Finishing the process. %d categories processed over %d seconds." % (count,time()-start)
    print "========================================================="

    return

if __name__ == '__main__':
    main()
