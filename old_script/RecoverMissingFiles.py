import os
from argparse import ArgumentParser
import pandas as pd
from Initial_wrangle import *
from monohbbml.parameters import *

parser=ArgumentParser()
#parser.add_argument('-m','--recovermissing', action='store_true', default=True, help='Recover the missing files.')
parser.add_argument('-b','--recoverbad', action='store_true', default=False, help='Recover the bad files.')
parser.add_argument('--inputdir',action='store',default='inputs')
args=parser.parse_args()

def recover(recoverlist):
    inputdir=args.inputdir
    text_file = open(recoverlist, "r")
    lines = text_file.read().splitlines()
    count=0
    for category in [dir for dir in os.listdir('Preselected_samples') if os.path.isdir('Preselected_samples'+'/'+dir)]:
        for root in [file for file in os.listdir('Preselected_samples'+'/'+category) if file.endswith('.root')]:
            count+=1
    start=time.time()
    #with pd.HDFStore('Preselected_samples/Vector_like.h5') as hdf:
    for line in lines:
        arg=str(line).split(' ')
        print(arg)
        container=arg[0]
        category=arg[2]
        test=False
        data_resolved_vector, name = process_arrays(container=container,inputdir=inputdir,isdata=True if category=='data' else False,test=test,category=category)
        if data_resolved_vector.empty: data_resolved_vector=pd.DataFrame(data=np.nan,columns=data_resolved_vector.keys(),index=[0])
        data_resolved_vector.to_hdf('Preselected_samples/Vector_like.h5',key='Resolved'+'/'+category+'/'+name,mode='a')
        stop=time.time()
        count+=1
        print 'Time elapsed: %d' % (stop-start)
        print '%d containers processed.' % count
        if count%20==0:
            hdf=pd.HDFStore('Preselected_samples/Vector_like.h5',mode='r')
            print 'HDF file contains %d frames' % len(hdf.keys())
            hdf.close()

def recover_hdf(recoverlist):
    inputdir=args.inputdir
    category_dict=Data_sample_to_category_dict(inputdir)
    text_file = open(recoverlist, "r")
    lines = text_file.read().splitlines()
    count=0
    start=time.time()
    for line in lines:
        arg=str(line).split(' ')
        container=arg[0]
        category=category_dict[container]
        test=False
        data_resolved_vector, name = process_arrays(container=container,inputdir=inputdir,isdata=True if category=='data' else False,test=test,category=category)
        if data_resolved_vector.empty: data_resolved_vector=pd.DataFrame(data=np.nan,columns=data_resolved_vector.keys(),index=[0])
        data_resolved_vector.to_hdf('Preselected_samples/Vector_like.h5',key='Resolved'+'/'+category+'/'+name,mode='a')
        stop=time.time()
        count+=1
        print 'Time elapsed: %d' % (stop-start)
        print '%d containers processed.' % count
        if count%20==0:
            hdf=pd.HDFStore('Preselected_samples/Vector_like.h5',mode='r')
            print 'HDF file contains %d frames' % len(hdf.keys())
            hdf.close()


print "Recovering missing files..."
recover('Preselected_samples/missing_samplelist.txt')
print "All missing samples have been recovered. Will delete the missing samplelist."
#os.remove('Preselected_samples/missing_samplelist.txt')

print '*'*50
print "Recovering missing HDF data..."
recover_hdf('Preselected_samples/hdf_missing_samplelist.txt')
print "All missing samples have been recovered. Will delete the missing samplelist."
#os.remove('Preselected_samples/missing_samplelist.txt')


os.remove('Preselected_samples/missing_samplelist.txt')
