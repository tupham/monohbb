#!/bin/bash
#
#
#
#    Created by Jay Chan
#
#        8.13.2018
#
#
#
#
initdir=$1
filename=$2
category=$3


cd $initdir
source ./setup_lxplus.sh
echo python process_arrays.py -n $filename $runsection -r $region -c $category
if [[ "$category" == *data* ]]; then
#python process_arrays.py -n $filename -r m_500_900 -c $category -d
#python process_arrays.py -n $filename -r m_900 -c $category -d
python process_arrays.py -n $filename -r r -c $category -d
else
#python process_arrays.py -n $filename -t -r m_500_900 -c $category
#python process_arrays.py -n $filename -v -r m_500_900 -c $category
#python process_arrays.py -n $filename -r m_500_900 -c $category
#python process_arrays.py -n $filename -t -r m_900 -c $category
#python process_arrays.py -n $filename -v -r m_900 -c $category
#python process_arrays.py -n $filename -r m_900 -c $category
python process_arrays.py -n $filename -t -r r -c $category
python process_arrays.py -n $filename -v -r r -c $category
python process_arrays.py -n $filename -r r -c $category
fi
