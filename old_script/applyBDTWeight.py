#!/usr/bin/env python
#
#
#
#  Created by Jay Chan
#
#  8.12.2018
#
#  Modified from ttHyy XGBosst anaylsis codes
#
#
#
#
import os
from argparse import ArgumentParser
import math
import ROOT
#from root_numpy import root2array
from array import array
import numpy as np
import pickle
from sklearn.preprocessing import StandardScaler
import xgboost as xgb


def getArgs():
    """Get arguments from command line."""
    parser = ArgumentParser()
    parser.add_argument('-r', '--region', action='store', choices=['m', 'm_500_900', 'm_900', 'r'], default='r', help='Region to process')
    parser.add_argument('--ZPtHDM', type=int, nargs='+', default = [], help='Masspoint of training ZP2HDM model samples.')
    parser.add_argument('--tHDMa_ggF', type=int, nargs='+', default = [], help='Masspoint of training 2HDM+a model samples (with ggF production).')
    parser.add_argument('--tHDMa_bb', type=int, nargs='+', default = [], help='Masspoint of training 2HDM+a model samples (with bb production).')
    return parser.parse_args()


def apply_weight(train_sig,catagories,region,isdata=False):

    model = xgb.Booster()
    model.load_model('models/%s%s.h5'%(train_sig, region))

    model_r = xgb.Booster()
    model_r.load_model('models/%s%s_r.h5'%(train_sig, region))

    # loading
    badfile=False
    cp1=False
    cp2=False
    for category in catagories:
        print 'Reading from %s samples...' %(category)
        AppInputsdir="AppInputs/"+category
        npydir='arrays/'+category
        """ Scan the files in each category in AppInputs, then go back to arrays and import the corresponding general file, which contain both training and validation samples"""
        for appinput in os.listdir(AppInputsdir):
            if not appinput.endswith('_%s.root' % (region)):
                continue
            if '_val_' in appinput: continue
            try:
                if region[0] == 'r':
                    jets=np.load(npydir+'/'+appinput.replace('.root','_jets.npy'))
                    cp1=True
                    if [jets.shape[1],jets.shape[2]]!=[1,2]:
                        print 'ERROR: Dimension of jets npy, [%d,%d], is not correct!!' %  (jets.shape[1],jets.shape[2])
                        error
                met=np.load(npydir+'/'+appinput.replace('.root','_met.npy'))
                cp2=True
                if [met.shape[1],met.shape[2]]!=[1,7]:
                    print 'ERROR: Dimension of met npy, [%d,%d], is not correct!!' %  (met.shape[1],met.shape[2])
                    error
                if region[0] == 'm':
                    fatjet=np.load(npydir+'/'+appinput.replace('.root','_Fatjet.npy'))
                    if [fatjet.shape[1],fatjet.shape[2]]!=[1,4]:
                        print 'ERROR: Dimension of jets npy, [%d,%d], is not correct!!' %  (fatjet.shape[1],fatjet.shape[2])
                        error
                '''Get the root file in AppInputs and its data on m_jj, m_J, and MET_E'''
                infile = ROOT.TFile.Open(AppInputsdir+'/'+appinput)
                intree = infile.Get('AppInput')
                eventNumber = array('L', [0])
                m_jj = array('f', [0])
                m_J = array('f', [0])
                MetTST_met = array('f', [0])

                if not isdata:
                    weight=array('d',[0])
                intree.SetBranchAddress('eventNumber', eventNumber)
                intree.SetBranchAddress('m_jj', m_jj)
                intree.SetBranchAddress('m_J', m_J)
                intree.SetBranchAddress('MetTST_met', MetTST_met)
                if not isdata:
                    intree.SetBranchAddress('weight', weight)

                # output tree
                bdt_score = array('f', [0])
                outtree = ROOT.TTree('test', 'test')
                outtree.SetDirectory(0)
                outtree.Branch('eventNumber', eventNumber,'eventNumber/l')
                outtree.Branch('m_jj', m_jj,'m_jj/F')
                outtree.Branch('m_J', m_J,'m_J/F')
                outtree.Branch('MetTST_met', MetTST_met,'MetTST_met/F')
                if not isdata:
                    outtree.Branch('weight', weight, 'weight/D')
                outtree.Branch('bdt_score', bdt_score, 'bdt_score/F')

                # get BDT scores for given arrays
                #print 'Getting BDT scores from the trained model...'
                events=[]
                if region[0] == 'r': events.append(jets.reshape((jets.shape[0], -1)))
                events.append(met.reshape((met.shape[0], -1)))
                if region[0] == 'm': events.append(fatjet.reshape((fatjet.shape[0], -1)))
                events = np.concatenate(events, axis=1)
                dEvents = xgb.DMatrix(events)
 
                score = model.predict(dEvents)
                score_r = model_r.predict(dEvents)

                if len(score) != intree.GetEntries():
                    print "ERROR: The number of events doesn't match!!!"
                    quit()


                #print 'writing bdt scores into new rootfiles...'
                for i in range(intree.GetEntries()):
                    intree.GetEntry(i)
                    if not isdata:
                        if eventNumber[0]%100 < 50:
                            bdt_score[0] = 1.0 - score_r[i]
                        else:
                            bdt_score[0] = 1.0 - score[i]
                    else:
                        bdt_score[0] = 1.0 - (score[i]+score_r[i])/2

                    outtree.Fill()

                if not os.path.isdir('outputs'):
                    print 'INFO: Creating new folder: \"outputs\"'
                    os.makedirs("outputs")
                if not os.path.isdir('outputs/model_%s%s' %(train_sig,region)):
                    print 'INFO: Creating model container:  model_%s%s' %(train_sig,region)
                    os.makedirs('outputs/model_%s%s' %(train_sig,region))
                if not os.path.isdir('outputs/model_%s%s/%s' %(train_sig,region,category)):
                    print 'INFO: Creating in the model container a new category:  %s' %(category)
                    os.makedirs('outputs/model_%s%s/%s' %(train_sig,region,category))


                outfile = ROOT.TFile('outputs/model_%s%s/%s/%s' % (train_sig,region,category,appinput), 'RECREATE')
                outtree.Write()
                outfile.Close()

                infile.Close()
            except:
                badfile=True
                print('ERROR: Unable to read \"'+ AppInputsdir+'/'+appinput+'\"')
                if cp1 and not cp2: print 'Able to load %s but not %s' % (npydir+'/'+appinput.replace('.root','_jets.npy'),npydir+'/'+appinput.replace('.root','_met.npy'))
                if cp2 and not cp1: print 'Able to load %s but not %s' % (npydir+'/'+appinput.replace('.root','_met.npy'),npydir+'/'+appinput.replace('.root','_met.npy'))
                if cp1 and cp2: print 'Able to load both'
                if not cp1 and not cp2: 'Unable to load both'
                stop=0
                for container in os.listdir('inputs'):
                    if stop: break
                    if category in container:
                        for ntuple in os.listdir('inputs/'+container):
                            if appinput.split('_')[1] in ntuple and appinput.split('_')[2] in ntuple:
                                print 'Original file: \"%s/%s\"'%(container,ntuple)
                                try:
                                    if txt: pass
                                except:
                                    txt = open('arrays/badsamplelist.txt','w')
                                txt.write('%s %s %s %s %s badsample\n'%(container, ntuple, 'none', region, category))
                                stop=1
                                break

        if badfile:
            return False


        print 'Combining all the root-files in the category:  ', category
        if os.path.isdir('outputs/model_%s%s/%s'%(train_sig,region,category)):
            if 'data' in category:
                os.system("hadd -f outputs/model_%s%s/%s.root outputs/model_%s%s/%s/*.root" %(train_sig,region,category,train_sig,region,category))
            else:
                mc16a=False
                mc16d=False
                mc16e=False
                for out in os.listdir('outputs/model_%s%s/%s'%(train_sig,region,category)):
                    if 'mc16a' in out: mc16a=True
                    if 'mc16d' in out: mc16d=True
                    if 'mc16e' in out: mc16e=True
                    if mc16a and mc16d and mc16e: break
                if mc16a:
                    os.system("hadd -f outputs/model_%s%s/%s_mc16a.root outputs/model_%s%s/%s/mc16a*.root" %(train_sig,region,category,train_sig,region,category))
                if mc16d:
                    os.system("hadd -f outputs/model_%s%s/%s_mc16d.root outputs/model_%s%s/%s/mc16d*.root" %(train_sig,region,category,train_sig,region,category))
                if mc16e:
                    os.system("hadd -f outputs/model_%s%s/%s_mc16e.root outputs/model_%s%s/%s/mc16e*.root" %(train_sig,region,category,train_sig,region,category))

    return True



def main():
    args=getArgs()
    
    ZP2HDM = [[600, 300], [600, 400], [800, 300], [800, 400], [800, 500], [1000, 400], [1000, 500], [1000, 600], [1200, 500],
              [1200, 600], [1200, 700], [1400, 500], [1400, 600], [1400, 700], [1400, 800], [1600, 300], [1600, 500], [1600, 600],
              [1600, 700], [1600, 800], [1800, 500], [1800, 600], [1800, 700], [1800, 800], [2000, 400], [2000, 500], [2000, 600],
              [2000, 700], [2000, 800], [2200, 300], [2200, 400], [2200, 500], [2200, 600], [2200, 700], [2400, 300], [2400, 400],
              [2400, 500], [2400, 600], [2400, 700], [400, 300], [400, 400], [2600, 300], [2600, 400], [2600, 500], [2600, 600],
              [2600, 700], [2800, 300], [2800, 400], [2800, 500], [2800, 600], [3000, 300], [3000, 400], [3000, 500], [3000, 600]]
    A2HDM =  [[300, 150], [400, 250], [600, 350], [800, 500],
              [1000, 150], [1100, 250], [1200, 350], [1300, 500],
              [1400, 150], [1600, 250], [1600, 350], [1800, 150]]

    sigs = []
    for mass in ZP2HDM:
        sigs.append('zp2hdmbbmzp%smA%s'%(mass[0],mass[1]))
    for mass in A2HDM:

        MA = str(mass[0])
        Ma  = str(mass[1])

        for production in ['2HDMa_bb_tb10', '2HDMa_ggF_tb1']:

            sigs.append(production + '_sp035_mA'+ MA + '_ma' + Ma)

    train_sig=''
    for i in range(0,len(args.ZPtHDM),2):
        train_sig += 'zp2hdmbbmzp%smA%s_'%(args.ZPtHDM[i],args.ZPtHDM[i+1])
    for i in range(0,len(args.tHDMa_ggF),2):
        train_sig += '2HDMa_ggF_tb1_sp035_mA%s_ma%s_'%(args.tHDMa_ggF[i],args.tHDMa_ggF[i+1])
    for i in range(0,len(args.tHDMa_bb),2):
        train_sig += '2HDMa_bb_tb10_sp035_mA%s_ma%s_'%(args.tHDMa_bb[i],args.tHDMa_bb[i+1])

    bkgs = ['Z','W','ttbar','stop','diboson','VH']
    #bkgs=['stop','diboson','VH']

    if args.region[0] == 'm':
        region = args.region.replace('m', 'merged')
    else:
        region = args.region.replace('r', 'resolved')

    print '=============================================================================='
    print 'INFO:  Using the model: %s%s.h'%(train_sig,region) 
    print '------------------------------------------------------------------------------'

    while not apply_weight(train_sig,sigs,region):
        os.system('python RecoverMissingFiles.py -b')
    while not apply_weight(train_sig,bkgs,region):
        os.system('python RecoverMissingFiles.py -b')
    while not apply_weight(train_sig,['data'],region,True):
        os.system('python RecoverMissingFiles.py -b')

    print '------------------------------------------------------------------------------'
    print 'Finishing the process'

    return

if __name__ == '__main__':
    main()
