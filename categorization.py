#!/usr/bin/env python
#
#
#
#  Created by Jay Chan
#
#  8.22.2018
#
#  Modified from ttHyy XGBosst anaylsis codes
#
#
#
#
import os
import ROOT
from ROOT import TH2F, TCanvas, gStyle, gROOT
from argparse import ArgumentParser
from math import sqrt, log
import math
from pdb import set_trace
from monohbbml.parameters import *
from getYields import get_yield
from AtlasStyle import *

ROOT.gROOT.SetBatch(True)

def getArgs():
    """Get arguments from command line."""
    parser = ArgumentParser()
    parser.add_argument('--region', action='store', choices=['Merged', 'Merged_500_900', 'Merged_900', 'Resolved'], default='Resolved', help='Region to process')
    parser.add_argument('--signalmodel', action='store', nargs='+', default=['ZP2HDM', '1400', '600', 'ZP2HDM', '1000', '600', 'ZP2HDM', '600', '300'], help="targeted signal models: 'ZP2HDM', '2HDMa_ggF', '2HDMa_bb'")
    parser.add_argument('-n','--nscan',type=int,default=100,help='number of scan.')
    parser.add_argument('--baseline',action='store',default='baseline_yields',help='Name of the yields.')
    parser.add_argument('-u', '--ulimit', type=float, default=0.2, help='constraint on u.')
    parser.add_argument('--ZPtHDM', type=int, nargs='+', default = [], help='Masspoint of training ZP2HDM model samples.')
    parser.add_argument('--tHDMa_ggF', type=int, nargs='+', default = [], help='Masspoint of training 2HDM+a model samples (with ggF production).')
    parser.add_argument('--tHDMa_bb', type=int, nargs='+', default=[], help='Masspoint of training 2HDM+a model samples (with bb production).')
    #parser.add_argument('-l','--lumi',type=float,default=79.8, help='The integrated luminosit to be normalized to.') a->36.1 d->43.7
    parser.add_argument('--From', action='store', help='Directory of the root files')
    return  parser.parse_args()

def calc_sig(sig, bkg,s_err,b_err):

  ntot = sig + bkg

  if(sig <= 0): return 0, 0
  if(bkg <= 0): return 0, 0

  if 2*((ntot*log(ntot/bkg)) - sig) <= 0: return 0, 0
  #Counting experiment
  #significance = sqrt(2*((ntot*log(ntot/bkg)) - sig))
  significance = sig/sqrt(bkg)

  #error on significance
  numer = sqrt((log(ntot/bkg)*s_err)**2 + ((log(1+(sig/bkg)) - (sig/bkg))*b_err)**2)
  uncert = (numer/significance)

  return significance, uncert

def hist_integral(hist,xlow,xhigh):
    err = ROOT.Double()
    if xlow>xhigh:
        n=0
        err=0
    else: n = hist.IntegralAndError(xlow,xhigh,err)
    return n, err

def sum_hist_integral(integrals,xs=1):
    # each element in integrals looks like [integral, error]
    #print integrals
    sum_integral=np.sum(np.array([integral[0] for integral in integrals]))*xs
    sum_error_squared=np.sum( np.array( [integral[1] for integral in integrals] ) **2 )
    sum_error=sqrt(sum_error_squared)*xs
    return sum_integral, sum_error

def sum_z(significances,uncertainties):
    significance=sqrt(np.sum(np.array(significances)**2))
    uncertainty=sqrt(np.sum((np.array(significances)*np.array(uncertainties))**2))/significance if significance!=0 else 0
    return significance, uncertainty

def GetSigmas(inDict, inFile):
    BRHbb=0.571
    lines = open(inFile,"r").readlines()
    for line in [l for l in lines if l[0]!='#']:
        if not '2HDMa' in line:
            mzp   = line.split()[1]
            ma    = line.split()[2]
            sigma = float(line.split()[5])
            inDict['zp2hdmbbmzp'+mzp+"mA"+ma]=float(sigma)*BRHbb
        else:
            if '2HDMa_ggF' in line:
                name  = line.split()[1].replace('MGPy8EG_A14N30LO_', '').replace('monoHbb_', '')
            elif '2HDMa_bb' in line:
                name  = line.split()[1].replace('MGPy8EG_A14N30LO_', '').replace('monoHbb_', '')
            else: continue
            sigma = float(line.split()[2])
            fe    = float(line.split()[3])
            inDict[name] = sigma * fe
    
    return inDict

def gettingsig(train_sig,region,sigs,bkgs, imax,nscan, targetsigs, yieldname, CrossSections):#, lumi):
    # A module to calculate the significances
# Obtain significance of cut-based baseline
    args=getArgs()
    print 'Evaluating baseline significances for all signal models...'
    sig_yield = pd.DataFrame(dtype=float, data={'category': sorted(sigs)})
    bkg_yield = pd.DataFrame(dtype=float,data={'category': sorted(bkgs)})
    for reg in sorted(['Merged', 'Resolved_350_500', 'Resolved_200_350', 'Resolved_150_200']):
        sig_yield[reg] = 0.
        bkg_yield[reg] = 0.
    yields = get_yield(sig_yield, bkg_yield, args.From, verbose=False)
    sig_yield, bkg_yield = yields['mbb_100_140']['sig'], yields['mbb_100_140']['bkg'].set_index('category')
    sig_yield = sig_yield[sig_yield['category'].isin(CrossSections.keys())]
    baseline_significance = pd.DataFrame()
    baseline_significance['category'] = sig_yield['category']
    for reg in list(sig_yield.columns):
        if reg == 'category': continue
        baseline_significance[reg] = sig_yield.apply(lambda x: calc_sig(x[reg] * CrossSections[x.category], bkg_yield.at['bkg_tot', reg], sqrt(x[reg] * CrossSections[x.category]), sqrt(bkg_yield.at['bkg_tot', reg]))[0], axis=1, result_type='expand')
    baseline_significance['Resolved'] = baseline_significance.apply(lambda x: sqrt(np.sum([x[key]** 2 for key in baseline_significance.keys() if 'Resolved' in key])), axis=1)
    print '\nSignificance of baseline:' 
    print baseline_significance.to_string()

# obtain bdt significance
    if not os.path.isdir('significances/model_%s%s' % (train_sig,region)):
        print 'INFO: Creating new model folder:  model_%s%s' % (train_sig,region)
        os.makedirs('significances/model_%s%s' % (train_sig,region))
    # Open a new dataframe to store the significances of signal models
    significance_df = baseline_significance.loc[:,['category','Resolved']] if region == 'Resolved' else baseline_significance.loc[:,['category','Merged']]
    significance_df.columns=['Signal_model', 'Significance_baseline']
    significance_df['Significance_BDT'], significance_df['Uncertainty_BDT'] = 0, 0
    significance_df.set_index('Signal_model', inplace=True)
    # Open the output files
    f_bkg_mc16a = ROOT.TFile('outputs/model_%s%s/bkg_mc16a.root' % (train_sig,region))
    f_bkg_mc16d = ROOT.TFile('outputs/model_%s%s/bkg_mc16d.root' % (train_sig,region))
    f_bkg_mc16e = ROOT.TFile('outputs/model_%s%s/bkg_mc16e.root' % (train_sig,region))
    # get the background BDT score in 'region'
    t_bkg_mc16a = f_bkg_mc16a.Get(region)
    t_bkg_mc16d = f_bkg_mc16d.Get(region)
    t_bkg_mc16e = f_bkg_mc16e.Get(region)
    h_bkg_mc16a = ROOT.TH1F('h_bkg_mc16a','h_bkg_mc16a',nscan,0.,1.)
    h_bkg_mc16a.Sumw2()
    h_bkg_mc16d = ROOT.TH1F('h_bkg_mc16d','h_bkg_mc16d',nscan,0.,1.)
    h_bkg_mc16d.Sumw2()
    h_bkg_mc16e = ROOT.TH1F('h_bkg_mc16e','h_bkg_mc16e',nscan,0.,1.)
    h_bkg_mc16e.Sumw2()
    #Ht='HtRatioMerged<=0.57' if region[0]=='m' else 'HtRatioResolved<=0.37'
    if region[0] == 'M': var='m_J'
    else: var='m_jj'
    t_bkg_mc16a.Draw("BDT_score>>h_bkg_mc16a","weight*36.1*1000*(%s>=100000&&%s<=140000)"%(var,var))#,Ht))
    t_bkg_mc16d.Draw("BDT_score>>h_bkg_mc16d","weight*44.2*1000*(%s>=100000&&%s<=140000)"%(var,var))#,Ht))
    t_bkg_mc16e.Draw("BDT_score>>h_bkg_mc16e","weight*58.4*1000*(%s>=100000&&%s<=140000)"%(var,var))#,Ht))


    #nbkgs, nbkg_err=[], []
    bkg_numbers, bkg_number_errors=[], []
    for j in range(len(imax) + 1):
        hist_integrals = []
        for h_bkg in [h_bkg_mc16a,h_bkg_mc16d,h_bkg_mc16e]: 
            hist_int=hist_integral(hist = h_bkg, xlow = 1 if j == 0 else imax[j-1], xhigh = (imax[j]-1) if j != len(imax) else nscan)
            hist_integrals.append(hist_int)
        nbkgj,nbkgj_err = sum_hist_integral(integrals = hist_integrals)
        bkg_numbers.append(nbkgj)
        bkg_number_errors.append(nbkgj_err)

    #nbkg=bkg.loc['bkg_tot',:]
    # Calculate BDT significance 
    for sig in CrossSections.keys():
        nsigs={'mc16a': [], 'mc16d': [], 'mc16e': []}
        for mc in nsigs:
            if os.path.isfile('outputs/model_%s%s/%s_%s.root' % (train_sig,region,sig,mc)):
                f_sig = ROOT.TFile('outputs/model_%s%s/%s_%s.root' % (train_sig,region,sig,mc))
                t_sig = f_sig.Get(region)
                h_sig=ROOT.TH1F(sig+'_'+mc,sig+'_'+mc,nscan,0.,1.)
                h_sig.Sumw2()
                if mc.endswith('a'): lumi = '36.1'
                elif mc.endswith('d'): lumi = ' 44.2'
                elif mc.endswith('e') and '2HDMa' not in sig: lumi = '58.4'
                elif mc.endswith('e') and '2HDMa' in sig: lumi = '139'
                t_sig.Draw("BDT_score>>%s_%s"%(sig,mc),"weight*1000*%s*(%s>=100000&&%s<=140000)"%(lumi,var,var))#,Ht))
                for j in range(len(imax)+1):
                    nsig = hist_integral(h_sig, 1 if j == 0 else imax[j-1],(imax[j]-1) if j != len(imax) else nscan)
                    nsigs[mc].append(nsig)
            else:
                for j in range(len(imax)+1):
                    nsigs[mc].append((0, 0))

        xs=CrossSections[sig]

        #nsigs, nsig_err = [], []
        # get the list of weighted numbers of signals and uncertainty for all MC signal samples of the same signal
        signal_numbers = [sum_hist_integral([nsigs['mc16a'][j], nsigs['mc16d'][j], nsigs['mc16e'][j]], xs)[0] for j in range(len(imax) + 1)]
        signal_number_errors = [sum_hist_integral([nsigs['mc16a'][j], nsigs['mc16d'][j], nsigs['mc16e'][j]], xs)[1] for j in range(len(imax) + 1)]
        # get the list of counting significances for all MC signal samples of the same signal
        significances = [calc_sig(signal_numbers[j], bkg_numbers[j], signal_number_errors[j], bkg_number_errors[j])[0] for j in range(len(signal_numbers))]
        uncertainties = [calc_sig(signal_numbers[j], bkg_numbers[j], signal_number_errors[j], bkg_number_errors[j])[1] for j in range(len(signal_numbers))]
        # finally calculate the combined significance and uncertainty accross the MC samples
        significance_df.at[sig,'Significance_BDT'] = sqrt( np.sum(np.array(significances) ** 2) )
        significance_df.at[sig,'Uncertainty_BDT']  = sqrt( np.sum((np.array(significances)*np.array(uncertainties))**2))/significance_df.at[sig,'Significance_BDT'] if significance_df.at[sig,'Significance_BDT'] != 0 else 0
        
    significance_df['Percentage_improvement'] = significance_df.apply(lambda x: x['Significance_BDT'] / x['Significance_baseline'] if x['Significance_baseline'] != 0 else 0, axis=1, result_type='expand')
    significance_df.dropna(inplace=True)
    significance_df.reset_index()
    print significance_df.to_string()
    if not os.path.isdir('significances/model_%s%s' % (train_sig,region)):
        print 'INFO: Creating new model folder:  model_%s%s' % (train_sig,region)
        os.makedirs('significances/model_%s%s' % (train_sig, region))
    filename_ext = ''
    for j in range(len(targetsigs)):
        filename_ext += '_%s'%targetsigs[j]
    significance_df.reset_index().to_csv('significances/model_%s%s/%s.csv' % (train_sig, region, filename_ext), sep=' ', index=False, mode='w')
    return significance_df


def plot_improvement(significance_df):
    ROOT.gROOT.SetBatch(True)

    impplot_zpthdm = TH2F('impplot', 'impplot', 14, 300., 3100., 7, 150., 850.)
    sigplot_zpthdm=TH2F('impplot', 'impplot', 14, 300.,3100., 7, 150., 850.)
    impplot_bb=TH2F('impplot', 'impplot', 16, 250.,1850.,3,100.,400.)
    sigplot_bb=TH2F('impplot', 'impplot', 16, 250.,1850.,3,100.,400.)
    impplot_ggF=TH2F('impplot', 'impplot', 16, 250.,1850.,3,100.,400.)
    sigplot_ggF=TH2F('impplot','impplot',16,250.,1850.,3,100.,400.)

    max={'zpthdm': [0, 0], 'bb': [0, 0], 'ggF': [0, 0]}
    for signal in list(significance_df.index):
        if 'zp2hdm' in signal: 
            mA = int(signal.split('mA')[1])
            mzp = int(signal.split('mA')[0].split('mzp')[1])
            improvement = significance_df.at[signal, 'Percentage_improvement']
            significance = significance_df.at[signal, 'Significance_BDT']
            if improvement > max['zpthdm'][0]: max['zpthdm'][0] = improvement
            if significance > max['zpthdm'][1]: max['zpthdm'][1] = significance
            if improvement != 0:
                impplot_zpthdm.Fill(mzp, mA, improvement)
                sigplot_zpthdm.Fill(mzp, mA, significance)
        elif '2HDMa_ggF' in signal:
            ma = int(signal.replace('_','').split('ma')[1])
            mA = int(signal.replace('_','').split('ma')[0].split('mA')[1])
            improvement = significance_df.at[signal, 'Percentage_improvement']
            significance = significance_df.at[signal, 'Significance_BDT']
            if improvement > max['ggF'][0]: max['ggF'][0] = improvement
            if significance > max['ggF'][1]: max['ggF'][1] = significance
            if improvement != 0:
                impplot_ggF.Fill(mA, ma, improvement)
                sigplot_ggF.Fill(mA, ma, significance)
        elif '2HDMa_bb' in signal:
            ma = int(signal.replace('_','').split('ma')[1])
            mA = int(signal.replace('_','').split('ma')[0].split('mA')[1])
            improvement = significance_df.at[signal, 'Percentage_improvement']
            significance = significance_df.at[signal, 'Significance_BDT']
            if improvement > max['bb'][0]: max['bb'][0] = improvement
            if significance > max['bb'][1]: max['bb'][1] = significance
            if improvement != 0:
                impplot_bb.Fill(mA, ma, improvement)
                sigplot_bb.Fill(mA, ma, significance)
    
    if not os.path.isdir('sigplots'): os.makedirs('sigplots')
# plot significances and improvements for zpthdm
    C2=TCanvas()
    C2.SetCanvasSize(550,395);
    C2.SetLeftMargin(0.135);
    C2.SetRightMargin(0.154);
    C2.SetTopMargin(0.15);

    gStyle.SetPaintTextFormat("4.2f")
    impplot_zpthdm.Draw("COLZTEXT")
    impplot_zpthdm.GetXaxis().SetTitle("m_{Z'} [GeV]")
    impplot_zpthdm.GetYaxis().SetTitle("m_{A} [GeV]")
    impplot_zpthdm.GetZaxis().SetTitle("Significance Ratio BDT / Baseline")
    zmax = max['zpthdm'][0] * 1.1 if max['zpthdm'][0] > 4. else 4.
    impplot_zpthdm.SetAxisRange(0., zmax,"Z")
    AtlasLabel.ATLASLabel(0.135,0.95," Internal")
    AtlasLabel.myText(0.37,0.95,"#sqrt{s} = 13 TeV, 139 fb^{-1}")
    AtlasLabel.myText(0.135,0.88,"Z'2HDM, 2 b-tags, 100 GeV < m_{bb} < 140 GeV")
    C2.Print("sigplots/ZP2HDM_imp.pdf")

    sigplot_zpthdm.Draw("COLZTEXT")
    sigplot_zpthdm.GetXaxis().SetTitle("m_{Z'} [GeV]")
    sigplot_zpthdm.GetYaxis().SetTitle("m_{A} [GeV]")
    sigplot_zpthdm.GetZaxis().SetTitle("Significance with BDT")
    zmax = max['zpthdm'][1] * 1.1 if max['zpthdm'][1] > 10. else 10.
    sigplot_zpthdm.SetAxisRange(0., zmax,"Z")
    AtlasLabel.ATLASLabel(0.135,0.95," Internal")
    AtlasLabel.myText(0.37,0.95,"#sqrt{s} = 13 TeV, 139 fb^{-1}")
    AtlasLabel.myText(0.135,0.88,"Z'2HDM, 2 b-tags, 100 GeV < m_{bb} < 140 GeV")
    C2.Print("sigplots/ZP2HDM_sig.pdf")

# plot significances and improments for 2HDMa
    C2=TCanvas()
    C2.SetCanvasSize(550,395);
    C2.SetLeftMargin(0.135);
    C2.SetRightMargin(0.154);
    C2.SetTopMargin(0.15);
    gStyle.SetPaintTextFormat("4.2f")
    impplot_ggF.Draw("COLZTEXT")
    impplot_ggF.GetXaxis().SetTitle("m_{A} [GeV]")
    impplot_ggF.GetYaxis().SetTitle("m_{a} [GeV]")
    impplot_ggF.GetZaxis().SetTitle("Significance Ratio BDT / Baseline")
    zmax = max['ggF'][0] * 1.1 if max['ggF'][0] > 4. else 4.
    impplot_ggF.SetAxisRange(0., zmax,"Z")
    AtlasLabel.ATLASLabel(0.135,0.95," Internal")
    AtlasLabel.myText(0.37,0.95,"#sqrt{s} = 13 TeV, 139 fb^{-1}")
    AtlasLabel.myText(0.135,0.88,"2HDMa_ggF, 2 b-tags, 100 GeV < m_{bb} < 140 GeV")
    C2.Print("sigplots/2HDMa_ggF_imp.pdf")

    sigplot_ggF.Draw("COLZTEXT")
    sigplot_ggF.GetXaxis().SetTitle("m_{Z'} [GeV]")
    sigplot_ggF.GetYaxis().SetTitle("m_{A} [GeV]")
    sigplot_ggF.GetZaxis().SetTitle("Significance with BDT")
    zmax = max['ggF'][1] * 1.1 if max['ggF'][1] > 10. else 10.
    sigplot_ggF.SetAxisRange(0., zmax,"Z")
    AtlasLabel.ATLASLabel(0.135,0.95," Internal")
    AtlasLabel.myText(0.37,0.95,"#sqrt{s} = 13 TeV, 139 fb^{-1}")
    AtlasLabel.myText(0.135,0.88,"2HDMa_ggF, 2 b-tags, 100 GeV < m_{bb} < 140 GeV")
    C2.Print("sigplots/2HDMa_ggF_sig.pdf")

    impplot_bb.Draw("COLZTEXT")
    impplot_bb.GetXaxis().SetTitle("m_{A} [GeV]")
    impplot_bb.GetYaxis().SetTitle("m_{a} [GeV]")
    impplot_bb.GetZaxis().SetTitle("Significance Ratio BDT / Baseline")
    zmax = max['bb'][0] * 1.1 if max['zpthdm'][0] > 4. else 4.
    impplot_bb.SetAxisRange(0., zmax,"Z")
    AtlasLabel.ATLASLabel(0.135,0.95," Internal")
    AtlasLabel.myText(0.37,0.95,"#sqrt{s} = 13 TeV, 139 fb^{-1}")
    AtlasLabel.myText(0.135,0.88,"2HDMa_bb, 2 b-tags, 100 GeV < m_{bb} < 140 GeV")
    C2.Print("sigplots/2HDMa_bb_imp.pdf")

    sigplot_bb.Draw("COLZTEXT")
    sigplot_bb.GetXaxis().SetTitle("m_{Z'} [GeV]")
    sigplot_bb.GetYaxis().SetTitle("m_{A} [GeV]")
    sigplot_bb.GetZaxis().SetTitle("Significance with BDT")
    zmax = max['bb'][1] * 1.1 if max['bb'][1] > 10. else 10.
    sigplot_bb.SetAxisRange(0., zmax,"Z")
    AtlasLabel.ATLASLabel(0.135,0.95," Internal")
    AtlasLabel.myText(0.37,0.95,"#sqrt{s} = 13 TeV, 139 fb^{-1}")
    AtlasLabel.myText(0.135,0.88,"2HDMa_bb, 2 b-tags, 100 GeV < m_{bb} < 140 GeV")
    C2.Print("sigplots/2HDMa_bb_sig.pdf")


def categorizing(train_sig,region,targetsigs,bkgs,nscan, CrossSections,ulimit):

    f_bkg_mc16a = ROOT.TFile('outputs/model_%s%s/bkg_mc16a.root' % (train_sig,region))
    f_bkg_mc16d = ROOT.TFile('outputs/model_%s%s/bkg_mc16d.root' % (train_sig,region))
    f_bkg_mc16e = ROOT.TFile('outputs/model_%s%s/bkg_mc16e.root' % (train_sig,region))
    t_bkg_mc16a = f_bkg_mc16a.Get(region)
    t_bkg_mc16d = f_bkg_mc16d.Get(region)
    t_bkg_mc16e = f_bkg_mc16e.Get(region)

    h_bkg_mc16a=ROOT.TH1F('h_bkg_mc16a','h_bkg_mc16a',nscan,0,1)
    h_bkg_mc16d=ROOT.TH1F('h_bkg_mc16d','h_bkg_mc16d',nscan,0,1)
    h_bkg_mc16e=ROOT.TH1F('h_bkg_mc16e','h_bkg_mc16e',nscan,0,1)

    h_bkg_mc16a.Sumw2()
    h_bkg_mc16d.Sumw2()
    h_bkg_mc16e.Sumw2()

    if region[0] == 'M': var='m_J'
    else: var='m_jj'
    #Ht='HtRatioMerged<=0.57' if region[0]=='M' else 'HtRatioResolved<=0.37'

    t_bkg_mc16a.Draw("BDT_score>>h_bkg_mc16a","weight*(%s>=100000&&%s<=140000)*1000*36.1"%(var,var))#,Ht))
    t_bkg_mc16d.Draw("BDT_score>>h_bkg_mc16d","weight*(%s>=100000&&%s<=140000)*1000*44.2"%(var,var))#,Ht))
    t_bkg_mc16e.Draw("BDT_score>>h_bkg_mc16e", "weight*(%s>=100000&&%s<=140000)*1000*58.4" % (var, var))  #,Ht))

    xs=1#CrossSections[targetsig1.replace('zp2hdmbbmzp','').replace('mA',',')]

    imax = [0]*len(targetsigs)

    for j in range(len(targetsigs)):
        targetsig = targetsigs[j]
        signal_files=[s for s in os.listdir('outputs/model_%s%s' % (train_sig, region)) if targetsig in s]
        for signal_file in signal_files:
            #print signal_file
            if 'mc16a' in signal_file:
                f_sig_mc16a = ROOT.TFile('outputs/model_%s%s/%s' % (train_sig, region, signal_file))
                t_sig_mc16a = f_sig_mc16a.Get(region)
            elif 'mc16d' in signal_file:
                f_sig_mc16d = ROOT.TFile('outputs/model_%s%s/%s' % (train_sig, region, signal_file))
                t_sig_mc16d = f_sig_mc16d.Get(region)
            else:
                f_sig_mc16e = ROOT.TFile('outputs/model_%s%s/%s' % (train_sig, region, signal_file))
                t_sig_mc16e = f_sig_mc16e.Get(region)

        h_sig_mc16a = ROOT.TH1F(targetsig+'_'+ 'mc16a', targetsig+'_'+'mc16a', nscan, 0, 1)
        h_sig_mc16a.Sumw2()    
        h_sig_mc16d = ROOT.TH1F(targetsig+'_'+ 'mc16d', targetsig+'_'+'mc16d', nscan, 0, 1)
        h_sig_mc16d.Sumw2()
        h_sig_mc16e = ROOT.TH1F(targetsig+'_'+ 'mc16e', targetsig+'_'+'mc16e', nscan, 0, 1)
        h_sig_mc16e.Sumw2()

        for signal_file in signal_files:
            if 'mc16a' in signal_file:
                lumi = '36.1'
                t_sig_mc16a.Draw('BDT_score>>' + targetsig + '_' + 'mc16a', 'weight*1000*' + lumi + '*(%s>=100000&&%s<=140000)'%(var,var))
            if 'mc16d' in signal_file:
                lumi = '44.2'
                t_sig_mc16d.Draw('BDT_score>>' + targetsig + '_' + 'mc16d', 'weight*1000*' + lumi + '*(%s>=100000&&%s<=140000)'%(var,var))
            if 'mc16e' in signal_file:
                lumi = '58.4' if '2HDMa' not in signal_file else '139'
                t_sig_mc16e.Draw('BDT_score>>' + targetsig + '_' + 'mc16e', 'weight*1000*' + lumi + '*(%s>=100000&&%s<=140000)'%(var,var))
        smax=0
        for i in range(1, nscan + 1 if j == 0 else imax[j - 1]):
            nsigl, nsigl_err = sum_hist_integral([hist_integral(h_sig_mc16a, 1, i - 1), hist_integral(h_sig_mc16d, 1, i - 1), hist_integral(h_sig_mc16e, 1, i - 1)], xs)
            #print nsigl, nsigl_err
            nbkgl, nbkgl_err = sum_hist_integral([hist_integral(h_bkg_mc16a, 1, i - 1), hist_integral(h_bkg_mc16d, 1, i - 1), hist_integral(h_bkg_mc16e, 1, i - 1)])
            #print nbkgl, nbkgl_err
            sl,ul=calc_sig(nsigl,nbkgl,nsigl_err,nbkgl_err)
            nsig1, nsig1_err = sum_hist_integral([hist_integral(h_sig_mc16a, i, nscan if j == 0 else imax[j - 1] - 1), hist_integral(h_sig_mc16d, i, nscan if j == 0 else imax[j - 1] - 1), hist_integral(h_sig_mc16e, i, nscan if j == 0 else imax[j - 1] - 1)], xs)
            nbkg1, nbkg1_err = sum_hist_integral([hist_integral(h_bkg_mc16a, i, nscan if j == 0 else imax[j - 1] - 1), hist_integral(h_bkg_mc16d, i, nscan if j == 0 else imax[j - 1] - 1), hist_integral(h_bkg_mc16e, i, nscan if j == 0 else imax[j - 1] - 1)])
            if nbkg1 == 0 or nbkg1_err / nbkg1 > ulimit: continue
            s1, u1 = calc_sig(nsig1, nbkg1, nsig1_err, nbkg1_err)
            s, u = sum_z([sl, s1], [ul, u1])
            if s > smax:
                smax = s
                umax = u
                imax[j] = i
        print '========================================================================='
        print 'The maximal significance:  %f +- %f' %(smax, umax)
        print '#%d boundary:  %f' %(j, (imax[j]-1.)/nscan)
        print '========================================================================='

    return sorted(imax)

def main():

    args=getArgs()
    region=args.region
    signalmodel=args.signalmodel
    nscan=args.nscan
    yieldname=args.baseline

    sigs = []
    for mass in ZP2HDM:
        sigs.append('zp2hdmbbmzp%smA%s'%(mass[0],mass[1]))
    for mass in A2HDM:
        MA = str(mass[0])
        Ma  = str(mass[1])
        for production in ['2HDMa_bb_tb10', '2HDMa_ggF_tb1']:
            sigs.append(production + '_sp035_mA'+ MA + '_ma' + Ma)
    
    targetsigs = []
    for i in range(0,len(signalmodel),3):
        if signalmodel[i] == 'ZP2HDM':
            if [int(signalmodel[i+1]),int(signalmodel[i+2])] in ZP2HDM:
                targetsigs.append('zp2hdmbbmzp%smA%s'%(signalmodel[i+1],signalmodel[i+2]))
            else: raise SystemExit('[%s,%s] are not in the list of ZP2HDM signal masspoints' %(signalmodel[i+1],signalmodel[i+2]))
        elif signalmodel[i] == '2HDMa_ggF':
            if [int(signalmodel[i+1]),int(signalmodel[i+2])] in A2HDM:
                targetsigs.append('2HDMa_ggF_tb1_sp035_mA%s_ma%s'%(signalmodel[i+1],signalmodel[i+2]))
            else: raise SystemExit('[%s,%s] are not in the list of A2HDM signal masspoints' %(signalmodel[i+1],signalmodel[i+2]))
        elif signalmodel[i] == '2HDMa_bb':
            if [int(signalmodel[i+1]),int(signalmodel[i+2])] in A2HDM:
                targetsigs.append('2HDMa_bb_tb10_sp035_mA%s_ma%s'%(signalmodel[i+1],signalmodel[i+2]))
            else: raise SystemExit('[%s,%s] are not in the list of A2HDM signal masspoints' %(signalmodel[i+1],signalmodel[i+2]))
    
    train_sig=''
    for i in range(0, len(args.ZPtHDM), 2):
        mzp = args.ZPtHDM[i]
        mA = args.ZPtHDM[i + 1]
        if [int(mzp),int(mA)] in ZP2HDM:
            train_sig+='zp2hdmbbmzp%smA%s_'%(mzp,mA)
        else:
            raise SystemExit('[%s,%s] are not in the list of ZP2HDM signal masspoints' %(mzp,mA))
    for i in range(0, len(args.tHDMa_ggF), 2):
        mA = args.tHDMa_ggF[i]
        ma = args.tHDMa_ggF[i + 1]
        if [int(mA),int(ma)] in A2HDM:
            train_sig+='2HDMa_ggF_tb1_sp035_mA%s_ma%s_'%(mA,ma)
        else:
            raise SystemExit('[%s,%s] are not in the list of A2HDM signal masspoints' %(mA,ma))
    for i in range(0, len(args.tHDMa_bb), 2):
        mA = args.tHDMa_bb[i]
        ma = args.tHDMa_bb[i + 1]
        if [int(mA),int(ma)] in A2HDM:
            train_sig+='2HDMa_bb_tb10_sp035_mA%s_ma%s_'%(mA,ma)
        else:
            raise SystemExit('[%s,%s] are not in the list of A2HDM signal masspoints' %(mA,ma))

    bkgs = ['Z','W','ttbar','stop','VH','diboson']
    
    CrossSections = GetSigmas({}, "data/sigma_FixedHiggsesTo300_08052017.txt")
    CrossSections = GetSigmas(CrossSections, "data/sigma_2HDMa.txt")

    imax=categorizing(train_sig,region,targetsigs,bkgs, nscan, CrossSections,args.ulimit)

    significance_df = gettingsig(train_sig, region, sigs, bkgs, imax, nscan, targetsigs, yieldname, CrossSections)
    
    plot_improvement(significance_df)

    return

if __name__ == '__main__': 
    main()
