#!/usr/bin/env python
#
#
#
#  Created by Jay Chan
#
#  8.22.2018
#
#  Modified from ttHyy XGBosst anaylsis codes
#
#
#
#
import os
from ROOT import *
from argparse import ArgumentParser
from math import sqrt, log
import math
from pdb import set_trace

gROOT.SetBatch(True)

def getArgs():
    """Get arguments from command line."""
    parser = ArgumentParser()
    parser.add_argument('-r', '--region', action='store', choices=['m', 'm_500_900', 'm_900', 'r'], default='r', help='Region to process')
    parser.add_argument('--signalmodel', action='store', nargs='+', default=['ZP2HDM', 'ZP2HDM', 'ZP2HDM'], help="targeted signal models: 'ZP2HDM', '2HDMa_ggF', '2HDMa_bb'")
    parser.add_argument('-s','--signalmasspoint',type=int,nargs='+',default=[1400,600,1000,600,600,300],help='Masspoint of specified signal model mass point of ZP2HDM.')
    parser.add_argument('-n','--nscan',type=int,default=100,help='number of scan.')
    parser.add_argument('--name',action='store',default='baseline_yields',help='Name of the yields.')
    parser.add_argument('-u','--ulimit',type=float,default=0.2,help='constraint on u.')
    return  parser.parse_args()

def calc_sig(sig, bkg,s_err,b_err):
    ntot = sig + bkg
    if(sig <= 0): return 0, 0
    if(bkg <= 0): return 0, 0
    significance = sig/sqrt(bkg)
    #error on significance
    numer = sqrt((log(ntot/bkg)*s_err)**2 + ((log(1+(sig/bkg)) - (sig/bkg))*b_err)**2)
    uncert = (numer/significance)  
    return significance, uncert

def hist_integral(hist,i,j):
    err = Double()
    if i>j:
        n=0
        err=0
    else: n = hist.IntegralAndError(i,j,err)
    return n, err

def sum_hist_integral(integrals,xs=1):
    # each element in integrals looks like [integral, error]
    sum_integral=np.sum(np.array([integral[0] for integram in integrals]))*xs
    sum_error_squared=np.sum( np.array( [integral[1] for integral in integrals] ) **2 )
    sum_error=sqrt(sum_error_squared)*xs
    return sum_integral, sum_error

def sum_z(significances,uncertainties):
    significance=sqrt(np.sum(np.array(significances)**2))
    uncertainty=sqrt(np.sum((np.array(significances)*np.array(uncertainties))**2))/significance if significance!=0 else 0
    return significance, uncertainty

def GetSigmas(inDict, inFile):

    BRHbb=0.571
    lines = open(inFile,"r").readlines()
    for line in [l for l in lines if l[0]!='#']:
        if not '2HDMa' in line:
            mzp   = line.split()[1]
            ma    = line.split()[2]
            sigma = float(line.split()[5])
            inDict['zp2hdmbbmzp'+mzp+"mA"+ma]=float(sigma)*BRHbb
        else:
            if '2HDMa_ggF' in line:
                name  = line.split()[1].replace('MGPy8EG_A14N30LO_', '').replace('monoHbb_', '')
            elif '2HDMa_bb' in line:
                name  = line.split()[1].replace('MGPy8EG_A14N30LO_', '').replace('monoHbb_', '')
            else: continue
            sigma = float(line.split()[2])
            fe    = float(line.split()[3])
            inDict[name] = sigma * fe  
    return inDict

def gettingsig(region, sigs, bkgs, imax, nscan, yieldname, CrossSections):

    print 'Evaluating significances for all of the signal models...'
    baselinetxt=open('data/yields/%s.txt'%(yieldname),'r')
    baseline_yield={}
    lines = baselinetxt.read().splitlines()
    if region[0] == 'm':
        index=4
        for line in lines:
            if line.startswith('#'): continue
            if line=='':continue
            if line.startswith('-'): continue
            baseline_yield[line.split()[0]]=float(line.split()[index])
    else:
        for line in lines:
            if line.startswith('#'): continue
            if line=='':continue
            if line.startswith('-'): continue
            baseline_yield[line.split()[0]] = [float(line.split()[1]), float(line.split()[2]), float(line.split()[3])]

    if region[0] == 'm': var='m_J'
    else: var='m_jj'
    if not os.path.isdir('significances'):
        print 'INFO: Creating output folder: \"significances\"'
        os.makedirs("significances")
    
    txt=open('significances/MET_%s.txt' % (region), 'w')
    txt.write('#nscan: %d\n'%nscan)
    for j in range(len(imax)):
        txt.write('#imax #%d: %d\n'%(j, imax[j]))
        txt.write('#Bondary #%d: %f\n'%(j, (imax[j]-1.)*(500-150)/nscan + 150 if region[0] == 'r' else (imax[j]-1.)*(1000-500)/nscan + 500))
    txt.write('#Signal_Model    Significance_after_BDT  Uncertainty_after_BDT   Baseline_Significance   Baseline_Uncertainty    Percentage_improvement\n')
    txt.write('_______________________________________________________________________________________________________________________________________\n\n')

    hist = {}
    for sig in sigs:
        hist[sig]=TH1F('hist_%s'%(sig),'hist_%s'%(sig),nscan,150 if region[0] == 'r' else 500,500 if region[0] == 'r' else 1000)
        hist[sig].Sumw2()

    hist['bkg']=TH1F('hist_bkg','hist_bkg',nscan,150 if region[0] == 'r' else 500,500 if region[0] == 'r' else 1000)
    hist['bkg'].Sumw2()

    for bkg in bkgs:
        for app in os.listdir('AppInputs/'+bkg):

            if not app.endswith('.root'): continue
            if not region in app: continue

            f=TFile('AppInputs/'+bkg+'/'+app)
            t=f.Get('AppInput')

            htemp=TH1F('htemp','htemp',nscan,150 if region[0] == 'r' else 500,500 if region[0] == 'r' else 1000)
            htemp.Sumw2()

            if 'mc16a' in app: lumi=36.1
            elif 'mc16d' in app: lumi=44.2
            elif 'mc16e' in app: lumi=58.4
            t.Draw("MetTST_met/1000>>htemp",'weight*(%s>=100000&&%s<=140000)*1000*%f'%(var, var, lumi))

            hist['bkg']+=htemp

    nbkgs, nbkg_err = [], []
    for j in range(len(imax)+1):
        nbkgj,nbkgj_err=sum_hist_integral([hist_integral(hist['bkg'],1 if j == 0 else imax[j-1], (imax[j]-1) if j != len(imax) else nscan+1)])
        print '#%d bin stat. error: %f%%' %(j, nbkgj_err/nbkgj*100 if nbkgj != 0 else 0)
        nbkgs.append(nbkgj)
        nbkg_err.append(nbkgj_err)

    
    nbkg=baseline_yield['bkg_tot']


    for sig in sigs:
        for app in os.listdir('AppInputs/'+sig):

            if not app.endswith('.root'): continue
            if not region in app: continue

            f=TFile('AppInputs/'+sig+'/'+app)
            t=f.Get('AppInput')

            htemp=TH1F('htemp','htemp',nscan,150 if region[0] == 'r' else 500,500 if region[0] == 'r' else 1000)
            htemp.Sumw2()

            if '2HDMa' in sig: lumi=139
            elif 'mc16a' in app: lumi=36.1
            elif 'mc16d' in app: lumi=44.2
            elif 'mc16e' in app: lumi=58.4
            t.Draw("MetTST_met/1000>>htemp",'weight*(%s>=100000&&%s<=140000)*1000*%f'%(var, var, lumi))

            hist[sig]+=htemp

        xs=CrossSections[sig.replace('zp2hdmbbmzp','').replace('mA',',')] if 'zp2hdm' in sig else CrossSections[sig]

        nsigs, nsig_err = [], []
        for j in range(len(imax)+1):
            nsigj, nsigj_err = sum_hist_integral([hist_integral(hist[sig], 1 if j == 0 else imax[j-1],(imax[j]-1) if j != len(imax) else nscan+1)], xs)
            nsigs.append(nsigj)
            nsig_err.append(nsigj_err)

        ss, us = [], []
        for j in range(len(nsigs)):
            sj, uj = calc_sig(nsigs[j], nbkgs[j], nsig_err[j], nbkg_err[j])
            ss.append(sj)
            us.append(uj)
        s, u = sum_z(ss,us)
        if region[0] == 'r':
            nsig=[ i*(CrossSections[sig.replace('zp2hdmbbmzp','').replace('mA',',')] if 'zp2hdm' in sig else CrossSections[sig]) for i in baseline_yield[sig]]
            z2 = 0
            for i in range(3):
                z2 += calc_sig(nsig[i],nbkg[i],sqrt(nsig[i]),sqrt(nbkg[i]))[0]**2
            s0 = sqrt(z2)
        else:
            nsig=(CrossSections[sig.replace('zp2hdmbbmzp','').replace('mA',',')] if 'zp2hdm' in sig else CrossSections[sig])*baseline_yield[sig]
            s0,u0=calc_sig(nsig,nbkg,sqrt(nsig),sqrt(nbkg))
       
        print 'Significance of %s:  %f +- %f         Baseline significance:  %f        Imporvement: %f %%'%(sig,s,abs(u),s0,(s-s0)/s0*100 if s0 !=0 else 0)
        txt.write('%s  %f  %f  %f  %f\n'%(sig,s,abs(u),s0,(s-s0)/s0*100 if s0 !=0 else 0))

def categorizing(region,targetsigs,bkgs,nscan, CrossSections,ulimit, targetmodel):

    xs=1#CrossSections[targetsig1.replace('zp2hdmbbmzp','').replace('mA',',')]

    if region[0] == 'm': var='m_J'
    else: var='m_jj'

    hist={}
    hist['bkg'] = TH1F('hist_bkg', 'hist_bkg', nscan, 150 if region[0] == 'r' else 500, 500 if region[0] == 'r' else 1000)
    hist['bkg'].Sumw2()

    for j in range(len(targetmodel)):
        hist[targetsigs[j]]=TH1F('hist_%s'%(targetsigs[j]),'hist_%s'%(targetsigs[j]),nscan,150 if region[0] == 'r' else 500,500 if region[0] == 'r' else 1000)
        hist[targetsigs[j]].Sumw2()

    for bkg in bkgs:
        for app in os.listdir('AppInputs/'+bkg):

            if not app.endswith('.root'): continue
            if not region in app: continue

            f=TFile('AppInputs/'+bkg+'/'+app)
            t=f.Get('AppInput')

            htemp=TH1F('htemp','htemp',nscan,150 if region[0] == 'r' else 500,500 if region[0] == 'r' else 1000)
            htemp.Sumw2()

            if 'mc16a' in app: lumi=36.1
            elif 'mc16d' in app: lumi=44.2
            elif 'mc16e' in app: lumi=58.4
            t.Draw("MetTST_met/1000>>htemp",'weight*(%s>=100000&&%s<=140000)*1000*%f'%(var, var, lumi))

            hist['bkg']+=htemp

    imax = [0 for j in range(len(targetmodel))]

    for j in range(len(targetmodel)):
        #hist[targetsigs[j]]=TH1F('hist_%s'%(targetsigs[j]),'hist_%s'%(targetsigs[j]),nscan,150 if region[0] == 'r' else 500,500 if region[0] == 'r' else 1000)
        #hist[targetsigs[j]].Sumw2()

        for app in os.listdir('AppInputs/'+targetsigs[j]):

            if not app.endswith('.root'): continue
            if not region in app: continue

            f=TFile('AppInputs/'+targetsigs[j]+'/'+app)
            t=f.Get('AppInput')

            htemp=TH1F('htemp','htemp',nscan,150 if region[0] == 'r' else 500,500 if region[0] == 'r' else 1000)
            htemp.Sumw2()

            if 'mc16a' in app: lumi=36.1
            elif 'mc16d' in app: lumi=44.2
            elif 'mc16e' in app: lumi=58.4
            t.Draw("MetTST_met/1000>>htemp",'weight*(%s>=100000&&%s<=140000)*1000*%f'%(var, var, lumi))

            hist[targetsigs[j]]+=htemp

        smax=0
        umax=0
        nsig1max=0
        nbkg1max=0
        for i in range(1,nscan+1):
            nsigl,nsigl_err=sum_hist_integral([hist_integral(hist[targetsigs[j]],1,i-1)],xs)
            nbkgl,nbkgl_err=sum_hist_integral([hist_integral(hist['bkg'],1,i-1)])
            sl,ul=calc_sig(nsigl,nbkgl,nsigl_err,nbkgl_err)
            nsig1,nsig1_err=sum_hist_integral([hist_integral(hist[targetsigs[j]],i,nscan+1 if j == 0 else imax[j-1]-1)],xs)
            nbkg1,nbkg1_err=sum_hist_integral([hist_integral(hist['bkg'],i,nscan+1 if j == 0 else imax[j-1]-1)])
            if nbkg1 == 0 or nbkg1_err/nbkg1 > ulimit: continue
            if nbkgl == 0 or nbkgl_err/nbkgl > ulimit: continue
            s1,u1=calc_sig(nsig1,nbkg1,nsig1_err,nbkg1_err)
            s,u=sum_z([sl,s1],[ul,u1])
            if s>smax:
                smax=s
                imax[j]=i
                umax=u
                nsig1max=nsig1
                nbkg1max=nbkg1
        #print nsig1max, nbkg1max, nsig2max, nbkg2max,nsig3max, nbkg3max, s1max, s2max, s3max

        print '========================================================================='
        print 'The maximal significance:  %f +- %f' %(smax, umax)
        print '#%d boundary:  %f' %(j, ((imax[j]-1.)*(500-150)/nscan + 150 if region[0] == 'r' else (imax[j]-1.)*(1000-500)/nscan + 500))
        print '========================================================================='

    return sorted(imax)

def main():

    args=getArgs()

    #lumi=args.lumi

    ZP2HDM = [[600, 300], [600, 400], [800, 300], [800, 400], [800, 500], [1000, 400], [1000, 500], [1000, 600], [1200, 500],
              [1200, 600], [1200, 700], [1400, 500], [1400, 600], [1400, 700], [1400, 800], [1600, 300], [1600, 500], [1600, 600],
              [1600, 700], [1600, 800], [1800, 500], [1800, 600], [1800, 700], [1800, 800], [2000, 400], [2000, 500], [2000, 600],
              [2000, 700], [2000, 800], [2200, 300], [2200, 400], [2200, 500], [2200, 600], [2200, 700], [2400, 300], [2400, 400],
              [2400, 500], [2400, 600], [2400, 700], [400, 300], [400, 400], [2600, 300], [2600, 400], [2600, 500], [2600, 600],
              [2600, 700], [2800, 300], [2800, 400], [2800, 500], [2800, 600], [3000, 300], [3000, 400], [3000, 500], [3000, 600]]
    A2HDM =  [[300, 150], [400, 250], [600, 350], [800, 500],
              [1000, 150], [1100, 250], [1200, 350], [1300, 500],
              [1400, 150], [1600, 250], [1600, 350], [1800, 150]]

    sigs = []
    for mass in ZP2HDM:
        sigs.append('zp2hdmbbmzp%smA%s'%(mass[0], mass[1]))

    for mass in A2HDM:

        MA = str(mass[0])
        Ma  = str(mass[1])

        for production in ['2HDMa_bb_tb10', '2HDMa_ggF_tb1']:

            sigs.append(production + '_sp035_mA'+ MA + '_ma' + Ma)

    sigmass=args.signalmasspoint

    targetsigs = []
    for i in range(len(args.signalmodel)):
        if args.signalmodel[i] == 'ZP2HDM':
            targetsigs.append('zp2hdmbbmzp%smA%s'%(sigmass[i*2],sigmass[i*2+1]))
        elif args.signalmodel[i] == '2HDMa_ggF':
            targetsigs.append('2HDMa_ggF_tb1_sp035_mA%s_ma%s'%(sigmass[i*2],sigmass[i*2+1]))
        elif args.signalmodel[i] == '2HDMa_bb':
            targetsigs.append('2HDMa_bb_tb10_sp035_mA%s_ma%s'%(sigmass[i*2],sigmass[i*2+1]))

    bkgs = ['Z', 'W', 'ttbar', 'stop', 'VH', 'diboson']

    if args.region[0] == 'm':
        region = args.region.replace('m', 'merged')
    else:
        region = args.region.replace('r', 'resolved')

    nscan=args.nscan

    CrossSections={}
    CrossSections = GetSigmas(CrossSections, "data/sigma_FixedHiggsesTo300_08052017.txt")
    CrossSections = GetSigmas_2HDMa(CrossSections, "data/sigma_2HDMa.txt")

    imax=categorizing(region, targetsigs, bkgs, nscan, CrossSections, args.ulimit, args.signalmodel)

    yieldname=args.name

    gettingsig(region, sigs, bkgs, imax, nscan, yieldname, CrossSections)

    return



if __name__ == '__main__':
    main()
