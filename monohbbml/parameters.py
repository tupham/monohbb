import os
from argparse import ArgumentParser
from time import time
#import traceback
#import math
import uproot
#from root_numpy import root2array
import numpy as np
#import pickle
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve, auc, confusion_matrix
from sklearn.utils import shuffle
#from keras.optimizers import RMSprop
#from keras.callbacks import EarlyStopping, ModelCheckpoint
#from sklearn.preprocessing import StandardScaler
import xgboost as xgb
from tabulate import tabulate
#from bayes_opt import BayesianOptimization
import matplotlib.pyplot as plt
from root_pandas import read_root, to_root
import ROOT
import pandas as pd

sample_list = {'ttbar': ['ttbar'], 'W': ['Wmunu', 'Wenu', 'Wtaunu'], 'Z': ['Znunu', 'Ztautau', 'Zee', 'Zmumu'],
                   'stop': ['stop'], 'diboson': ['WW', 'ZZ', 'WZ'], 'VH': ['qq', 'ggZ'],
                   'data': ['data15', 'data16', 'data17', 'data18']}
ZP2HDM = [[600, 300], [600, 400], [800, 300], [800, 400], [800, 500], [1000, 400], [1000, 500], [1000, 600],
          [1200, 500],
          [1200, 600], [1200, 700], [1400, 500], [1400, 600], [1400, 700], [1400, 800], [1600, 300], [1600, 500],
          [1600, 600],
          [1600, 700], [1600, 800], [1800, 500], [1800, 600], [1800, 700], [1800, 800], [2000, 400], [2000, 500],
          [2000, 600],
          [2000, 700], [2000, 800], [2200, 300], [2200, 400], [2200, 500], [2200, 600], [2200, 700], [2400, 300],
          [2400, 400],
          [2400, 500], [2400, 600], [2400, 700], [400, 300], [400, 400], [2600, 300], [2600, 400], [2600, 500],
          [2600, 600],
          [2600, 700], [2800, 300], [2800, 400], [2800, 500], [2800, 600], [3000, 200], [3000, 300], [3000, 400],
          [3000, 500], [3000, 600]]
A2HDM = [[300, 150], [400, 250], [600, 350], [800, 500],
         [1000, 150], [1100, 250], [1200, 350], [1300, 500],
         [1400, 150], [1600, 250], [1600, 350], [1800, 150]]

vector_variables={'data':['FatJet_eta', 'Electron_pt', 'TrackJet_eta',
'ForwardJet_m', 'TrackJet_bjet', 'ForwardJet_phi', 'Muon_charge', 'FatJet_phi',
'TrackJet_passDRcut', 'Jet_n_MuonInJet', 'Jet_bjet', 'ForwardJet_eta',
'Electron_phi', 'Electron_signal', 'Electron_eta', 'Jet_pt', 'Muon_signal', 'Muon_phi',
'TrackJet_isAssociated', 'TrackJet_m', 'Electron_charge', 'TrackJet_MV2c10',
'FatJet_n_matchedasstrkjets', 'Jet_eta', 'Jet_passOR', 'FatJet_m', 'Muon_pt',
'Electron_passOR', 'Jet_MV2c10', 'Jet_m', 'TrackJet_phi', 'Jet_phi',
'Muon_eta', 'FatJet_pt', 'Muon_passOR', 'ForwardJet_pt', 'Electron_m', 'TrackJet_pt', 'Muon_m'],

'samples': ['FatJet_eta', 'Jet_phi', 'TrackJet_eta', 'ForwardJet_m',
'TrackJet_bjet', 'ForwardJet_phi', 'Muon_charge', 'FatJet_phi', 'Electron_pt',
'TrackJet_passDRcut', 'Jet_n_MuonInJet', 'Electron_eta', 'ForwardJet_eta',
'Electron_phi', 'Electron_signal', 'Jet_bjet', 'Jet_pt', 'Muon_signal',
'Muon_phi', 'TrackJet_isAssociated', 'TrackJet_m', 'Electron_charge',
'TrackJet_MV2c10', 'FatJet_n_matchedasstrkjets', 'Jet_eta',
'Jet_HadronConeExclTruthLabelID', 'Jet_passOR', 'FatJet_m', 'Muon_pt',
'Electron_passOR', 'Jet_MV2c10', 'Jet_m', 'TrackJet_HadronConeExclTruthLabelID',
'TrackJet_phi', 'Muon_eta', 'FatJet_pt', 'Muon_passOR', 'ForwardJet_pt',
'Electron_m', 'TrackJet_pt', 'Muon_m'],
}

def set_pscore(x):
    if x < 0.11:
        score=0
    elif x < 0.64:
        score=1
    elif x < 0.83:
        score=2
    elif x<0.94:
        score = 3
    else:
        score = 4
    return score

def set_pscore_VR(x):
    if x < 0.05:
        score=0
    elif x < 0.58:
        score=1
    elif x < 0.79:
        score=2
    elif x<0.92:
        score = 3
    else:
        score = 4
    return score

def Data_sample_ID(container):
    if 'data' not in container:
        DSID=container.split('.')[3]
    else:
        DSID=container.split('.')[2].replace('13TeV',container.split('.')[3])
    return DSID

def DSID_to_data_sample_dict(inputdir):
    containers=[container for container in os.listdir(inputdir) if os.path.isdir(inputdir+'/'+container)
                and 'XAMPP' in container and not 'Gamma_Sh' in container
                and len([file for file in os.listdir(inputdir+'/'+container) if '.root' in file]) > 0]
    DSID_dict={}
    DSID_list=set()
    for container in containers:
        dsid=Data_sample_ID(container)
        DSID_list=DSID_list|{dsid}
    for dsid in DSID_list:
        DSID_dict[dsid]=sorted([container for container in containers if Data_sample_ID(container) == dsid])
    # check
    containers=[DSID_dict[key] for key in DSID_dict.keys()]
    containers=[x for y in containers for x in y]
    listlength=len(containers)
    setlength=len(set(containers))
    if listlength!=setlength:
        junk=[]
        print('Duplication!')
        for container in containers:
            if container not in junk:
                junk.append(container)
            else:
                print(container)
    return DSID_dict

def Data_sample_to_DSID_dict(inputdir):
    containers=[container for container in os.listdir(inputdir) if os.path.isdir(inputdir+'/'+container)
                and 'XAMPP' in container and not 'Gamma_Sh' in container
                and len([file for file in os.listdir(inputdir+'/'+container) if '.root' in file]) > 0]
    data_sample_dict={}
    for container in containers:
        data_sample_dict[container]=[]
        DSID=Data_sample_ID(container)
        data_sample_dict[container].append(DSID)
    return data_sample_dict

def Data_sample_to_category_dict(inputdir):
    containers=[container for container in os.listdir(inputdir) if os.path.isdir(inputdir+'/'+container)
                and 'XAMPP' in container and not 'Gamma_Sh' in container
                and len([file for file in os.listdir(inputdir+'/'+container) if '.root' in file]) > 0]
    input_channel_dict={}
    for container in containers:
        input_channel_dict[container]=[]
        if 'zp2hdmbbmzp' in container or '2HDMa' in container:
            category = container.split('.')[4]
            input_channel_dict[container].append(category)
        else:
            for category in sample_list.keys():
                for channel in sample_list[category]:
                    if channel in container:
                        input_channel_dict[container].append(category)
        if len(input_channel_dict[container])!=1:
            print('Inconsistency!!!')
            print(input_channel_dict[container])
            print(container)
            raise SystemExit
    for container in containers:
        input_channel_dict[container]=input_channel_dict[container][0]
    return input_channel_dict

def Root_name(container):
    name=Data_sample_ID(container=container)+'.root'
    if ('0900d' in container or '1000d' in container) and 'mc16' in container:
        name='mc16d_'+name
    elif ('0900a' in container or '1000a' in container) and 'mc16' in container:
        name='mc16a_'+name
    elif ('0900e' in container or '0902e' in container or '1000e' in container) and 'mc16' in container:
        name='mc16e_'+name
    elif 'data15' in container:
        pass
    elif 'data16' in container:
        pass
    elif 'data17' in container:
        pass
    elif 'data18' in container:
        pass
    else:
        print 'WARNING: File does not match a typical file name!!'
    return name

def Data_sample_to_output_name_dict(inputdir):
    containers=[container for container in os.listdir(inputdir) if os.path.isdir(inputdir+'/'+container)
                and 'XAMPP' in container and not 'Gamma_Sh' in container
                and len([file for file in os.listdir(inputdir+'/'+container) if '.root' in file]) > 0]
    Data_sampe_to_output_dict={}
    checklist=[]
    for container in containers:
        Data_sampe_to_output_dict[container]=Root_name(container)
        checklist.append(Data_sampe_to_output_dict[container])
    # check duplication
    duplication_list=[]
    unique_list=[]
    for name in checklist:
        if name not in unique_list:
            unique_list.append(name)
        else:
            duplication_list.append(name)
    if len(duplication_list)!=0:
        print('Duplicated names exist!')
        for name in duplication_list:
            print(name)
            print([key for key in Data_sampe_to_output_dict.keys() if name==Data_sampe_to_output_dict[key]])
    return Data_sampe_to_output_dict

def process_features(region,masspoints,type='weight'):

    print '============ Plotting features ============\n'
    sig = ''
    for i in range(0, len(masspoints), 2):
        mA = masspoints[i]
        mZP = masspoints[i + 1]
        sig += 'zp2hdmbbmzp%smA%s_' % (mA, mZP)

    model = xgb.Booster()
    model.load_model( 'models/%s%s.h5' % (sig, region))

    xgb.plot_importance(booster=model, importance_type=type)

    # plt.rcdefaults()
    fig, ax = plt.subplots()

    importances = model.get_score(importance_type=type).values()
    names = model.get_score(importance_type=type).keys()
    print model.get_score(importance_type=type).items()

    print len(importances), " features used to make BDT cuts"

    print len(names), " names"

    features = sorted(zip(importances, names))
    importances = list(reversed([x for x, y in features]))
    names = list(reversed([y for x, y in features]))

    y_pos = np.arange(len(names))

    ax.barh(y_pos, importances, align='center',
            color='blue', ecolor='black')
    ax.set_yticks(y_pos)
    ax.set_yticklabels(names)
    ax.invert_yaxis()  # labels read top-to-bottom
    ax.set_xlabel('Importance (%s)' % type)
    ax.set_title('Feature Importance %s' % region)

    currentdir=os.getcwd()
    parent = os.path.split(currentdir)[1]
    if not os.path.isdir('feature_plots'):
        os.makedirs('feature_plots/')
    plt.savefig('feature_plots/'  + '%s_%s_%s_feature.pdf' % (parent, sig, region))

    return

def getAUC(y_test, weight, score):
    from sklearn.metrics import roc_curve, auc, confusion_matrix
    fpr, tpr, _ = roc_curve(y_test, score, weight)
    roc_auc = auc(fpr, tpr, reorder=True)
    return roc_auc

def plotROC(y_train, weight_train, score_train, y_val, weight_val, score_val, filename, show=False):

    fpr_train, tpr_train, _ = roc_curve(y_train, score_train, sample_weight=weight_train)
    roc_auc_train = auc(fpr_train, tpr_train, reorder=True)
    fpr_val, tpr_val, _ = roc_curve(y_val, score_val, sample_weight=weight_val)
    roc_auc_val = auc(fpr_val, tpr_val, reorder=True)
    #fpr_test, tpr_test, _ = roc_curve(y_test, score_test, sample_weight=weight_test)
    #roc_auc_test = auc(fpr_test, tpr_test, reorder=True)

    print('Training ROC AUC = %f' % roc_auc_train)
    print('Validation ROC AUC = %f' % roc_auc_val)
    #print('Test ROC AUC = %f' % roc_auc_test)

    fpr_train = 1.0 - fpr_train
    fpr_val = 1.0 - fpr_val
    #fpr_test = 1.0 - fpr_test

    plt.grid(color='gray', linestyle='--', linewidth=1)
    plt.plot(tpr_train, fpr_train, label='Train set, area = %0.6f' % roc_auc_train, color='red', linestyle='dashed')
    plt.plot(tpr_val, fpr_val, label='Val set, area = %0.6f' % roc_auc_val, color='blue', linestyle='dashdot')
    #plt.plot(tpr_test, fpr_test, label='Test set, area = %0.6f' % roc_auc_test, color='red', linestyle='dashed')
    plt.plot([0, 1], [1, 0], linestyle='--', color='black', label='Luck')
    plt.xlabel('Signal acceptance')
    plt.ylabel('Background rejection')
    plt.title('Receiver operating characteristic')
    plt.xlim(0, 1)
    plt.ylim(0, 1)
    plt.xticks(np.arange(0, 1, 0.1))
    plt.yticks(np.arange(0, 1, 0.1))
    plt.legend(loc='lower left', framealpha=1.0)

    #plt.savefig('plots/' + filename + '.png')
    if not os.path.isdir('bdt_plots'):
        os.mkdir('bdt_plots')
    plt.savefig('bdt_plots/' + filename + '.eps')

    if show: plt.show()

    plt.clf()
    return

def statistics(dir):
    if not os.path.isdir(dir): 
        print 'Directory doesnot exist, exitting!!!'
        pass
    else:
        for category in sorted([d for d in os.listdir(dir) if os.path.isdir(dir+'/'+d)]):
            resolved=0
            merged=0
            for root in [file for file in os.listdir(dir + '/' + category) if file.endswith('.root')]:
                t=ROOT.TFile(dir+'/'+category+'/'+root)
                resolved+=t.Get('Resolved').GetEntries()
                merged+=t.Get('Merged').GetEntries()
            print category,':'
            print 'Resolved: ', resolved
            print 'Merged: ', merged
            print '*'*50


    