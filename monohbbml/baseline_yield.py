#!/usr/bin/env python
import os
from ROOT import TH1F, TFile
from argparse import ArgumentParser

def get_yield(category):
    category_yield={}
    category_yield_masswindow={}
    regions=['Merged','Resolved_350_500','Resolved_200_350','Resolved_150_200']
    for root in [r for r in os.listdir('Preselected_categorys/'+category) if r.endswith('.root')]:
        f=TFile('Preselected_categorys/'+category+'/'+root)
        root_base=root.replace('.root','') # e.g. root_base=W_mc16a
        mc=root_base.split('_')[-1]
        category_yield[mc],category_yield_masswindow[mc]={},{}
        for region in regions:
            t=f.Get(region.split('_')[0])
            h=TH1F(root_base,root_base,40,50000,280000)
            h_m=TH1F(root_base+'_m',root_base+'_m',40,100000,140000)
            var='m_J' if region=='Merged' else 'm_jj'
            if '2HDMa' in category:
                lumi=139
            elif 'mc16a' in root:
                lumi=36.1
            elif 'mc16d' in root:
                lumi=44.2
            else:
                lumi=58.4
            if region=='Merged':
                selection='weight*1000*'+str(lumi)+'*(MetTST_met>500000&&HtRatioMerged<=0.57)'
            elif region=='Resolved_350_500':
                selection='weight*1000*'+str(lumi)+'*(MetTST_met<500000&&MetTST_met>350000&&HtRatioResolved<=0.37)'
            elif region=='Resolved_200_350':
                selection='weight*1000*'+str(lumi)+'*(MetTST_met>200000&&MetTST_met<350000&&HtRatioResolved<=0.37)'
            else:
                selection='weight*1000*'+str(lumi)+'*(MetTST_met<200000&&HtRatioResolved<=0.37)'
            t.Draw(var+'>>'+root_base,selection)
            t.Draw(var+'>>'+root_base+'_m',selection)
            category_yield[mc][region]=h.Integral()
            category_yield_masswindow[mc][region]=h.Integral()

    return category_yield, category_yield_masswindow