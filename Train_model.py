#!/usr/bin/env python
import os
from argparse import ArgumentParser
from time import time
#import traceback
#import math
import uproot
#from root_numpy import root2array
import numpy as np
#import pickle
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve, auc, confusion_matrix
from sklearn.utils import shuffle
#from keras.optimizers import RMSprop
#from keras.callbacks import EarlyStopping, ModelCheckpoint
#from sklearn.preprocessing import StandardScaler
import xgboost as xgb
from tabulate import tabulate
#from bayes_opt import BayesianOptimization
import matplotlib.pyplot as plt
from root_pandas import read_root, to_root
import ROOT
import pandas as pd
from monohbbml.parameters import *
from monohbbml.baseline_yield import get_yield

def getArgs():
    parser = ArgumentParser()
    parser.add_argument('--From', action='store', default='Training_data', help='Directory which contains training samples')
    parser.add_argument('--region', action='store', choices=['Merged', 'Merged_500_900', 'Merged_900', 'Resolved'], default='Resolved', help='Region to train on')
    parser.add_argument('--cat', '--categorical', action='store_true', help='Create categorical model')
    parser.add_argument('-s', '--signal', action='store', type=signal_multiplier, default='5', help='Number of signal events to use in training as a multiple of the number of background events. Value of 0 means use all signal events.')
    parser.add_argument('-n', '--name', action='store', default='test', help='Name of output plot.')
    parser.add_argument('-p', '--params', action='store', type=dict, default=None, help='json string.') #type=json.loads
    parser.add_argument('--numRound', action='store', type=int, default=10000, help='Number of rounds.')
    parser.add_argument('--roc', action='store_true', help='Plot ROC')
    parser.add_argument('--ZPtHDM', type=int, nargs='+', default = [], help='Masspoint of training ZP2HDM model samples.')
    parser.add_argument('--tHDMa_ggF', type=int, nargs='+', default = [], help='Masspoint of training 2HDM+a model samples (with ggF production).')
    parser.add_argument('--tHDMa_bb', type=int, nargs='+', default = [], help='Masspoint of training 2HDM+a model samples (with bb production).')
    parser.add_argument('--sf',type=float,default=1.,help='Scale factor of signal to make the signal and background effectively same in size.')
    parser.add_argument('--feature',action='store_true',default=False,help='Plot feature importance')
    parser.add_argument('--getScore_only',action='store_true',default=False)
    parser.add_argument('--train_vars',action='store',nargs='+',default=['MetTST_met', 'N_Jets04', 'HtRatioResolved','Dijet_pt','Dijet_eta','PCS_2','PCS_1','Delta_phi'],help='Training variables')
    parser.add_argument('--train_bkg',action='store',nargs='+',choices=['Z','W','ttbar','VH','diboson','stop'],default=['Z','W','ttbar','VH','diboson','stop'],help='Background categories to include in the training')
    parser.add_argument('--extra_vars',action='store',nargs='+',default=['m_jj','m_J','eventNumber'],help='Extra variables to save in the output files')
    return parser.parse_args()

# Auxillary functions


def signal_multiplier(s):
    s = float(s)
    if s < 0.0:
        raise argparse.ArgumentTypeError('%r must be >= 0!' % s)
    return s

def further_selections(Data, region):
    data=Data
    if region != 'Resolved':
        if region in ['Merged','Merged_900']:
            data=data[data.HtRatioMerged <= 0.57]
        if region == 'Merged_500_900':
            data=data[(data.MetTST_met >  500000) & (data.MetTST_met<=900000)]
        elif region!='Merged':
            data=data[data.MetTST_met >  900000]
    return data

def load_root(categories,region,vars):
    
    args = getArgs()
    inputdir = args.From
    # define an empty dataframes
    df=pd.DataFrame()
    df_mc16a=pd.DataFrame()
    df_mc16d=pd.DataFrame()
    df_mc16e=pd.DataFrame()

    rootkey=region if region[0]=='R' else 'Merged'
    for category in sorted(categories):
        cat_dir=inputdir+'/'+category
        print('Loading %s' % cat_dir)
        for root in sorted([file for file in os.listdir(cat_dir) if file.endswith('.root') and 'empty' not in file]):
            root_dir = cat_dir + '/' + root
            working_data = uproot.open(root_dir)
            root_dirs = [key.split(';')[0] for key in working_data.keys()]
            if not rootkey in root_dirs: continue
            working_data=working_data[rootkey].pandas.df()
            if not set(vars).issubset(set(working_data.keys())):
                print 'WARNING: The following variables are not contained in ntuple: '
                print set(vars)-set(working_data.keys())
                raise SystemExit('Exitting!!!')
            # further selections
            working_data = further_selections(Data=working_data, region=region)
            working_data=working_data[vars]
            if working_data.empty:continue
            if 'mc16a' in root: df_mc16a=df_mc16a.append(working_data,ignore_index=True)
            elif 'mc16d' in root: df_mc16d=df_mc16d.append(working_data,ignore_index=True)
            elif 'mc16e' in root: df_mc16e=df_mc16e.append(working_data,ignore_index=True)
            else: print 'Data sample name, %s, does not follow naming system.' % root
            df=df.append(working_data,ignore_index=True)

    return df, df_mc16a, df_mc16d, df_mc16e

def train_model(params, args, region, sigs, bkgs, train_vars,extra_vars):
    args=getArgs()
    vars=extra_vars|train_vars
    print 'Preparing training samples...'
    # load data
    sig, sig_mc16a, sig_mc16d, sig_mc16e = load_root(sigs,region,vars)
    bkg, bkg_mc16a, bkg_mc16d, bkg_mc16e = load_root(bkgs,region,vars)
    sig=sig.astype('float')
    bkg=bkg.astype('float')

    # append an outcome column to sig and bkg; y=1 for sig and 0 for bkg
    sig['Y']=1
    bkg['Y']=0

    # split data into train, and test samples
    # method 1: By eventNumber:
    print('Splitting data.')
    train_sig, test_sig = sig[sig.eventNumber%100<50], sig[sig.eventNumber%100>=50]
    train_bkg, test_bkg = bkg[bkg.eventNumber%100<50], bkg[bkg.eventNumber%100>=50]
    # method 2: by a pseudo-random number generator
    #train_sig, test_sig = train_test_split(sig,shuffle=True,train_size=0.5)
    #train_bkg, test_bkg = train_test_split(bkg,shuffle=True,train_size=0.5)

    # calculate the training weight
    SF=args.sf
    if SF == -1: SF = 1.*train_sig.shape[0]/train_bkg.shape[0]
    train_sig_wt = train_sig.weight * ( train_sig.shape[0] + train_bkg.shape[0] ) * SF / ( train_sig.weight.sum() * (1+SF) )
    train_bkg_wt = train_bkg.weight * ( train_sig.shape[0] + train_bkg.shape[0] ) * SF / ( train_bkg.weight.sum() * (1+SF) )
    test_sig_wt = test_sig.weight * ( train_sig.shape[0] + train_bkg.shape[0] ) * SF / ( train_sig.weight.sum() * (1+SF) )
    test_bkg_wt = test_bkg.weight * ( train_sig.shape[0] + train_bkg.shape[0] ) * SF / ( train_bkg.weight.sum() * (1+SF) )

    # then assign to weight column
    train_sig=train_sig.assign(weight=train_sig_wt)
    train_bkg= train_bkg.assign(weight=train_bkg_wt)
    test_sig=test_sig.assign(weight=test_sig_wt)
    test_bkg=test_bkg.assign(weight=test_bkg_wt)

    # concatenate training signal and training background to form train set. At this point, they still contain eventNumber, Y, and weight and all the extraneous variables. We don't want these
    train_set=pd.concat([train_sig,train_bkg],ignore_index=True)
    test_set=pd.concat([test_sig,test_bkg],ignore_index=True)

    # organize data for training
    print('Organizing data for training.')
    # randomise the data frames
    #train_set = shuffle(train_set,random_state=1756)
    #test_set = shuffle(test_set,random_state=1756)

    print('\nTraining dataframe sample:')
    print(train_set.head(20))
    print('\nTesting dataframe sample:')
    print(test_set.head(20))

    # pop 'weight' and 'Y' columns from the datasets and assign to corresponding arrays
    print '\nStripping the weight column off to input into training...'
    y_train_weight = train_set.pop('weight').to_numpy()
    y_test_weight   = test_set.pop('weight').to_numpy()

    print '\nStripping the outcome column off to input into training...'
    y_train=train_set.pop('Y').to_numpy()
    y_train=y_train.astype(np.uint8)
    y_test=test_set.pop('Y').to_numpy()
    y_test=y_test.astype(np.uint8)

    # Selecting only the columns with the training variables to throw into training
    print '\nStripping off unnecessary columns...'
    print 'These dataframes will go into training...'
    X_train=train_set[train_vars].astype(np.float32)
    X_test=test_set[train_vars].astype(np.float32)
    print('Training dataframe sample:')
    print(X_train.head(20))
    print('\nValidation dataframe sample:')
    print(X_test.head(20))

    headers = ['Sample', 'Total', 'Training', 'Validation']
    sample_size_table = [
        ['Signal'    , sig.shape[0], train_sig.shape[0], test_sig.shape[0]],
        ['Background', bkg.shape[0], train_bkg.shape[0], test_bkg.shape[0]],
    ]
    print tabulate(sample_size_table, headers=headers, tablefmt='simple')

    # convert to DMatrix
    dTrain = xgb.DMatrix(data=X_train, label=y_train, weight=y_train_weight,feature_names=X_train.keys())
    dTest = xgb.DMatrix(data=X_test, label=y_test, weight=y_test_weight,feature_names=X_test.keys())

    # set up training parameters
    print('Train model.')
    num_round=args.numRound
    param = {'colsample_bytree': 0.601636189527514, 'silent': 1, 'eval_metric': ['auc','logloss'], 'max_delta_step': 3.633816495872836, 'nthread': 4, 'min_child_weight': 4, 'subsample': 0.6588843327224047, 'eta': 0.019251809428140865, 'objective': 'binary:logistic', 'alpha': 0.2894769437092044, 'max_depth': 7, 'gamma': 0.12186533874627076, 'booster': 'gbtree'}

    # use customised parameters if given
    if params:
        for key in params:
            param[key] = params[key]
    print(param)

    # train a model on the dTrain matrix
    evallist  = [(dTrain, 'train'), (dTest, 'eval')]
    evals_result = {}
    eval_result_history = []
    bst = xgb.train(param, dTrain, num_round, evals=evallist, early_stopping_rounds=20, evals_result=evals_result)

    # test model
    print('Testing model...')
    score = bst.predict(dTest)
    aucValue = getAUC(y_test, y_test_weight, score)
    print("param: %s, Val AUC: %s" % (param, aucValue))
    if args.roc:
        score_train = bst.predict(dTrain)
        plotROC(y_train, y_train_weight, score_train, y_test, y_test_weight, score, 'roc_%s'%(region), show=False)

    print("======Reversing training and validation samples======")
    evallist  = [(dTest, 'train'), (dTrain, 'eval')]
    evals_result = {}
    eval_result_history = []
    bst_r = xgb.train(param, dTest, num_round, evals=evallist, early_stopping_rounds=20, evals_result=evals_result)

    # test model
    print('Testing model...')
    score = bst_r.predict(dTrain)
    aucValue = getAUC(y_train, y_train_weight, score)
    print("param: %s, Val AUC: %s" % (param, aucValue))
    if args.roc:
        score_train = bst.predict(dTest)
        plotROC(y_test, y_test_weight, score_train, y_train, y_train_weight, score, 'roc_%s_r'%(region), show=False)

    # save model
    if not os.path.isdir('models'):
        os.makedirs('models')
    print('=============================================================')
    print('Saving model')
    signame=''
    for sig in sigs:
        signame+=sig+'_'
    bst.save_model('models/%s%s.h5' % (signame,region))
    bst_r.save_model('models/%s%s_r.h5' % (signame,region))

    return bst, bst_r

def main():
    args=getArgs()
    params=args.params
    train_vars=set(args.train_vars)
    bkgs=args.train_bkg
    region=args.region
    extra_vars = set(args.extra_vars) | {'eventNumber', 'weight'}
    inputdir = args.From
    # train models
    print '=============================================================================='
    sigs=[]
    model='model_'
    for i in range(0,len(args.ZPtHDM),2):
        sigs.append('zp2hdmbbmzp%smA%s'%(args.ZPtHDM[i],args.ZPtHDM[i+1]))
        model+='zp2hdmbbmzp%smA%s'%(args.ZPtHDM[i],args.ZPtHDM[i+1])+'_'
    for i in range(0,len(args.tHDMa_ggF),2):
        sigs.append('2HDMa_ggF_tb1_sp035_mA%s_ma%s'%(args.tHDMa_ggF[i],args.tHDMa_ggF[i+1]))
        model+='2HDMa_ggF_tb1_sp035_mA%s_ma%s'%(args.tHDMa_ggF[i],args.tHDMa_ggF[i+1])+'_'
    for i in range(0,len(args.tHDMa_bb),2):
        sigs.append('2HDMa_bb_tb10_sp035_mA%s_ma%s'%(args.tHDMa_bb[i],args.tHDMa_bb[i+1]))
        model+='2HDMa_bb_tb10_sp035_mA%s_ma%s'%(args.tHDMa_bb[i],args.tHDMa_bb[i+1])+'_'

    model+=region
    print 'INFO:  Training as signal on:  ', sigs
    print 'INFO:  Training as background on:  ', bkgs
    print 'INFO:  Training variables:  ', train_vars
    print '------------------------------------------------------------------------------'  

    # start training
    if not args.getScore_only:
        print 'Training '+model
        bst, bst_r = train_model(params, args, region, sigs, bkgs,train_vars,extra_vars)
        print '------------------------------------------------------------------------------'
        print 'Finishing training.\n'
    else:
        bst,bst_r=xgb.Booster(),xgb.Booster()
        bst.load_model('models'+'/'+model.replace('model_','')+'.h5')
        bst_r.load_model('models'+'/'+model.replace('model_','')+'_r.h5')

    if args.feature:
        print 'Plotting feature importance...'
        xgb.plot_importance(bst,height=0.4)
        plt.show()
        if not os.path.isdir('feature_plots'):
            os.makedirs('feature_plots/')
        plt.savefig('feature_plots/'  + model+ '_feature.pdf' )
    
    
    outputdir='outputs'+'/'+model
    if not os.path.isdir(outputdir):
        print 'INFO: Creating an output folder at %s.' % outputdir
        os.makedirs(outputdir)

    print 'Calculating BDT score for all individual background signals'
    bkg_tot_mc16a, bkg_tot_mc16d, bkg_tot_mc16e = pd.DataFrame(), pd.DataFrame(), pd.DataFrame() 
    for category in ['Z','W','ttbar','VH','diboson','stop']:
        #print 'Calculating baseline yield for %s' % category
        #baseline_yield=get_yield(category)
        print 'Calculating BDT score for %s' % category
        bkg, bkg_mc16a, bkg_mc16d, bkg_mc16e = load_root([category], region, train_vars|extra_vars)
        bkg_tot_mc16a=pd.concat([bkg_tot_mc16a,bkg_mc16a],axis=0,ignore_index=True)
        bkg_tot_mc16d=pd.concat([bkg_tot_mc16d,bkg_mc16d],axis=0,ignore_index=True)
        bkg_tot_mc16e=pd.concat([bkg_tot_mc16e,bkg_mc16e],axis=0,ignore_index=True)
        for df in zip([bkg_mc16a, bkg_mc16d, bkg_mc16e], ['mc16a', 'mc16d', 'mc16e']):
            if df[0].empty: continue
            mc=df[1]
            #baseline_yield_df=pd.DataFrame(baseline_yield[mc])
            dEvents=xgb.DMatrix(df[0][train_vars],feature_names=list(df[0][train_vars].keys()))
            score_df=pd.DataFrame(data={'score':bst.predict(dEvents),'score_r':bst_r.predict(dEvents)},index=df[0].index)
            dataframe=pd.concat([df[0],score_df],axis=1)
            dataframe['BDT_score']=dataframe.apply(lambda x: x.score if x.eventNumber%100>50 else x.score_r,axis=1)
            dataframe.drop(columns=['score','score_r'],inplace=True)
            name=category+'_'+mc+'.root'
            print 'Saving %s to output folder.' % name
            print '='*100
            dataframe.to_root(outputdir+'/'+name,key=region)
            #baseline_yield_df.to_root(outputdir+'/'+name,key='baseline',mode='a')
    print 'Calculating BDT score for combined background signals'
    for df in zip([bkg_tot_mc16a, bkg_tot_mc16d, bkg_tot_mc16e], ['mc16a', 'mc16d', 'mc16e']):
        if df[0].empty: continue
        dEvents=xgb.DMatrix(df[0][train_vars],feature_names=list(df[0][train_vars].keys()))
        score_df=pd.DataFrame(data={'score':bst.predict(dEvents),'score_r':bst_r.predict(dEvents)},index=df[0].index)
        dataframe=pd.concat([df[0],score_df],axis=1)
        dataframe['BDT_score']=dataframe.apply(lambda x: x.score if x.eventNumber%100>50 else x.score_r,axis=1)
        dataframe.drop(columns=['score','score_r'],inplace=True)
        name='bkg'+'_'+df[1]+'.root'
        print 'Saving %s to output folder.' % name
        print '='*100
        dataframe.to_root(outputdir+'/'+name,key=region)

    print 'Calculating BDT score for ZP2HDM signals'
    for category in [cat for cat in os.listdir(inputdir) if 'zp2hdmbbmzp' in cat]:
        #print 'Calculating baseline yield for %s' % category
        #baseline_yield=get_yield(category)
        print 'Calculating BDT score for %s' % category
        bkg, bkg_mc16a, bkg_mc16d, bkg_mc16e = load_root([category], region, train_vars|extra_vars)
        for df in zip([bkg_mc16a, bkg_mc16d, bkg_mc16e], ['mc16a', 'mc16d', 'mc16e']):
            if df[0].empty: continue
            mc=df[1]
            #baseline_yield_df=pd.DataFrame(baseline_yield[mc])
            dEvents=xgb.DMatrix(df[0][train_vars],feature_names=list(df[0][train_vars].keys()))
            score_df=pd.DataFrame(data={'score':bst.predict(dEvents),'score_r':bst_r.predict(dEvents)},index=df[0].index)
            dataframe=pd.concat([df[0],score_df],axis=1)
            dataframe['BDT_score']=dataframe.apply(lambda x: x.score if x.eventNumber%100>50 else x.score_r,axis=1)
            dataframe.drop(columns=['score','score_r'],inplace=True)
            name=category+'_'+mc+'.root'
            print 'Saving %s to output folder.' % name
            print '='*100
            dataframe.to_root(outputdir+'/'+name,key=region)
            #baseline_yield_df.to_root(outputdir+'/'+name,key='baseline',mode='a')

    print 'Calculating BDT score for 2HDMa_bb signals'
    for category in [cat for cat in os.listdir(inputdir) if '2HDMa_bb' in cat]:
        #print 'Calculating baseline yield for %s' % category
        #baseline_yield=get_yield(category)
        print 'Calculating BDT score for %s' % category
        bkg, bkg_mc16a, bkg_mc16d, bkg_mc16e = load_root([category], region, train_vars|extra_vars)
        for df in zip([bkg_mc16e], ['mc16e']):
            if df[0].empty: continue
            mc=df[1]
            #baseline_yield_df=pd.DataFrame(baseline_yield[mc])
            dEvents=xgb.DMatrix(df[0][train_vars],feature_names=list(df[0][train_vars].keys()))
            score_df=pd.DataFrame(data={'score':bst.predict(dEvents),'score_r':bst_r.predict(dEvents)},index=df[0].index)
            dataframe=pd.concat([df[0],score_df],axis=1)
            dataframe['BDT_score']=dataframe.apply(lambda x: x.score if x.eventNumber%100>50 else x.score_r,axis=1)
            dataframe.drop(columns=['score','score_r'],inplace=True)
            name=category+'_'+mc+'.root'
            print 'Saving %s to output folder.' % name
            print '='*100
            dataframe.to_root(outputdir+'/'+name,key=region)
            #baseline_yield_df.to_root(outputdir+'/'+name,key='baseline',mode='a')


    print 'Calculating BDT score for 2HDMa_ggF signals'
    for category in [cat for cat in os.listdir(inputdir) if '2HDMa_ggF' in cat]:
        #print 'Calculating baseline yield for %s' % category
       # baseline_yield=get_yield(category)
        print 'Calculating BDT score for %s' % category
        bkg, bkg_mc16a, bkg_mc16d, bkg_mc16e = load_root([category], region, train_vars|extra_vars)
        for df in zip([bkg_mc16e], ['mc16e']):
            if df[0].empty: continue
            mc=df[1]
            #baseline_yield_df=pd.DataFrame(baseline_yield[mc])
            dEvents=xgb.DMatrix(df[0][train_vars],feature_names=list(df[0][train_vars].keys()))
            score_df=pd.DataFrame(data={'score':bst.predict(dEvents),'score_r':bst_r.predict(dEvents)},index=df[0].index)
            dataframe=pd.concat([df[0],score_df],axis=1)
            dataframe['BDT_score']=dataframe.apply(lambda x: x.score if x.eventNumber%100>50 else x.score_r,axis=1)
            dataframe.drop(columns=['score','score_r'],inplace=True)
            name=category+'_'+mc+'.root'
            print 'Saving %s to output folder.' % name
            print '='*100
            dataframe.to_root(outputdir+'/'+name,key=region)
            #baseline_yield_df.to_root(outputdir+'/'+name,key='baseline',mode='a')
    return

if __name__ == '__main__':
    main()
