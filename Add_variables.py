import os
import math
from argparse import ArgumentParser
import numpy as np
from time import time
import uproot
import pandas as pd
from root_pandas import *
import ROOT
from monohbbml.parameters import *

def getArgs():
    parser=ArgumentParser()
    parser.add_argument('--From', action='store', default='Preselected_samples', help='Directory of the root files to append new variables')
    parser.add_argument('--Saveto',action='store',default='Training_data')
    parser.add_argument('--PCS',action='store_true',help='Add pseudo-continuous scores')
    parser.add_argument('--mode',action='store',choices=['append','recreate'],default='append')
    return parser.parse_args()

def add_pcs(Dataframe,Working_data,mode,region):
    '''Define a module to calculate and add new variables to the dataframe using the variables from Working_data'''
    if mode=='append' and region =='Resolved' and set(['Dijet_pt','Dijet_eta','Delta_phi','PCS_1','PCS_2']).issubset(set(Dataframe.keys())):
        print 'PCS variables sont dans le root file deja. Faire pas calculs.'
        return Dataframe
    elif mode=='append' and region =='Merged' and set(['Delta_phi','DeltaRjj','fatjet_pt','fatjet_eta','fatjet_phi','fatjet_E']).issubset(set(Dataframe.keys())):
        print 'PCS variables sont dans le root file deja. Faire pas calculs.'
        return Dataframe
    else:
        if Working_data.shape[0]!=Dataframe.shape[0]:
            print 'WARNING: La dimension de la dataframe est en contradiction avec la dimension de la working_data'
            print 'Dataframe: %s' % str(Dataframe.shape)
            print 'Working_data: %s' % str(Working_data.shape)
            raise SystemExit()
        # set the index to column 'MyeventNumber' defined in Initial_wrangle. Both dataframe and working_data now have the same index. 
        dataframe=Dataframe.set_index('MyeventNumber')
        working_data=Working_data.set_index('MyeventNumber')
        indices=working_data.index
        print 'Processing %d events...' % len(indices)
        if region == 'Resolved':
            new_vars=pd.DataFrame(columns=['Dijet_pt','Dijet_eta','Delta_phi','PCS_1','PCS_2'],index=indices,data=-999,dtype='float')
            for index in indices:
                ej=zip(working_data.at[index,'Jet_pt'],working_data.at[index,'Jet_eta'],working_data.at[index,'Jet_phi'],working_data.at[index,'Jet_m'],working_data.at[index,'Jet_bjet'],working_data.at[index,'Jet_MV2c10'])
                #print [working_data.at[index,'Jet_pt'],working_data.at[index,'Jet_eta'],working_data.at[index,'Jet_phi'],working_data.at[index,'Jet_m'],working_data.at[index,'Jet_bjet'],working_data.at[index,'Jet_MV2c10']]
                ej=[i for i in ej]
                ej = sorted(ej, key=lambda x: x[0], reverse=True)
                ej = sorted(ej, key=lambda x: x[4], reverse=True)
                # only select the first two jets
                if len(ej)>=2:
                    ej=[ej[0],ej[1]]
                    # calculate the psedo-continuous b-tagging scores from the first two leading Bjets
                    new_vars.at[index,'PCS_1']=set_pscore(ej[0][-1])
                    new_vars.at[index,'PCS_2']=set_pscore(ej[1][-1])
                    # compute the dijet variables and delta_phi
                    j1p=ROOT.Math.LorentzVector("ROOT::Math::PtEtaPhiM4D<float>")(ej[0][0],ej[0][1],ej[0][2],ej[0][3])
                    j2p=ROOT.Math.LorentzVector("ROOT::Math::PtEtaPhiM4D<float>")(ej[1][0],ej[1][1],ej[1][2],ej[1][3])
                    DiJetp=j1p+j2p
                    new_vars.at[index,'Dijet_pt'],new_vars.at[index,'Dijet_eta'],dijet_phi=DiJetp.Pt(),DiJetp.Eta(),DiJetp.Phi()
                    new_vars.at[index,'Delta_phi']=abs(ROOT.TVector2.Phi_mpi_pi(dataframe.at[index,'MetTST_phi'] - dijet_phi))
                else:
                    ef=zip(working_data.at[index,'ForwardJet_pt'],working_data.at[index,'ForwardJet_eta'],working_data.at[index,'ForwardJet_phi'],working_data.at[index,'ForwardJet_m'])
                    ef=[i for i in ef]
                    ef = sorted(ef, key=lambda x: x[0], reverse=True)
                    ej+=ef
                    if len(ej)>=2:
                        j1p=ROOT.Math.LorentzVector("ROOT::Math::PtEtaPhiM4D<float>")(ej[0][0],ej[0][1],ej[0][2],ej[0][3])
                        j2p=ROOT.Math.LorentzVector("ROOT::Math::PtEtaPhiM4D<float>")(ej[1][0],ej[1][1],ej[1][2],ej[1][3])
                        DiJetp=j1p+j2p
                        new_vars.at[index,'Dijet_pt'],new_vars.at[index,'Dijet_eta'],dijet_phi=DiJetp.Pt(),DiJetp.Eta(),DiJetp.Phi()
                        new_vars.at[index,'Delta_phi']=abs(ROOT.TVector2.Phi_mpi_pi(dataframe.at[index,'MetTST_phi'] - dijet_phi))
            # concatenate the new frame to working frame
            dataframe=dataframe.assign(Dijet_pt=new_vars.Dijet_pt)
            dataframe=dataframe.assign(Dijet_eta=new_vars.Dijet_eta)
            dataframe=dataframe.assign(PCS_1=new_vars.PCS_1)
            dataframe=dataframe.assign(PCS_2=new_vars.PCS_2)
            dataframe=dataframe.assign(Delta_phi=new_vars.Delta_phi)
        else:
            new_vars=pd.DataFrame(columns=['Delta_phi','DeltaRjj','fatjet_pt','fatjet_eta','fatjet_phi','fatjet_E'],index=indices,data=-999,dtype='float')
            for index in indices:
                ej=zip(working_data.at[index,'TrackJet_pt'],working_data.at[index,'TrackJet_eta'],working_data.at[index,'TrackJet_phi'],working_data.at[index,'TrackJet_m'],working_data.at[index,'TrackJet_isAssociated'],working_data.at[index,'TrackJet_bjet'])
                en=[i for i in ej if i[4]!=1]
                ej=[i for i in ej if i[4]==1]
                ej = sorted(ej, key=lambda x: x[0], reverse=True)
                ej = sorted(ej, key=lambda x: x[5], reverse=True)
                en = sorted(en, key=lambda x: x[0], reverse=True)
                ej+=en
                if len(ej)>=2:
                    j1p=ROOT.TLorentzVector()
                    j2p=ROOT.TLorentzVector()
                    j1p.SetPtEtaPhiM(ej[0][0],ej[0][1],ej[0][2],ej[0][3])
                    j2p.SetPtEtaPhiM(ej[1][0],ej[1][1],ej[1][2],ej[1][3])
                    DiJetp=j1p+j2p
                    new_vars.at[index,'DeltaRjj']=DeltaRjj=j1p.DeltaR(j2p)
                ej=zip(working_data.at[index,'FatJet_pt'],working_data.at[index,'FatJet_eta'],working_data.at[index,'FatJet_phi'],working_data.at[index,'FatJet_m'])
                ej=[i for i in ej]
                ej=sorted(ej, key=lambda x: x[0], reverse=True)
                if len(ej)>=1:
                    fatjet=ROOT.Math.LorentzVector("ROOT::Math::PtEtaPhiM4D<float>")(ej[0][0],ej[0][1],ej[0][2],ej[0][3])
                    new_vars.at[index,'fatjet_pt'],new_vars.at[index,'fatjet_eta'],new_vars.at[index,'fatjet_phi'],new_vars.at[index,'fatjet_E']=fatjet.Pt(),fatjet.Eta(),fatjet.Phi(),fatjet.E()
                    new_vars.at[index,'Delta_phi']=ROOT.TVector2.Phi_mpi_pi(dataframe.at[index,'MetTST_phi'] - fatjet.Phi())
            dataframe=dataframe.assign(Delta_phi=new_vars.Delta_phi)
            dataframe=dataframe.assign(DeltaRjj=new_vars.DeltaRjj)
            dataframe=dataframe.assign(fatjet_pt=new_vars.fatjet_pt)
            dataframe=dataframe.assign(fatjet_eta=new_vars.fatjet_eta)
            dataframe=dataframe.assign(fatjet_phi=new_vars.fatjet_phi)
            dataframe=dataframe.assign(fatjet_E=new_vars.fatjet_E)
        dataframe.reset_index(inplace=True)
        print 'Finishing'
        return dataframe

def main():
    args=getArgs()
    workdir,mode,savedir=args.From,args.mode,args.Saveto
    root_files=[]
    for category in [d for d in os.listdir(workdir) if os.path.isdir(workdir+'/'+d)]:
        root_files+=[workdir+'/'+category+'/'+file for file in os.listdir(workdir+'/'+category) if (file.endswith('.root') and 'empty' not in file) ]
    print '%d root files existent.' % len(root_files)
    #with pd.HDFStore('Preselected_samples'+'/'+'Vector_like.h5',mode='r') as hdf:
    count=0
    start = time()
    if args.PCS:
        for root_file in root_files:
            print "========================================================="
            print "Ouvrir %s" % root_file
            category=root_file.split('/')[1]
            type=root_file.split('/')[-1].split('_')[-1].replace('.root','')
            dataframes={}
            for region in ['Resolved', 'Merged']:
                try:
                    dataframes[region] = read_root(root_file, key=region)
                except: pass
            
            if not os.path.isdir(os.path.split(root_file.replace(workdir,savedir))[0]):
                os.makedirs(os.path.split(root_file.replace(workdir, savedir))[0])
            if os.path.isfile(root_file.replace(workdir,savedir)): os.remove(root_file.replace(workdir,savedir))
            
            for region in dataframes:
                if mode == 'append' and set(['Dijet_pt', 'Dijet_eta', 'PCS_1', 'PCS_2', 'Delta_phi']).issubset(set(dataframes[region].keys())): continue
                h5dir = region + '/' + category + '/' + type
                working_data = pd.read_hdf(workdir + '/Vector_like.h5', mode='r', key=h5dir)
                print 'Calculating new variables in the %s region...' % 'resolved' if region == 'Resolved' else 'merged'
                dataframes[region] = add_pcs(dataframes[region], working_data, mode, region=region)
                if not os.path.isfile(root_file.replace(workdir,savedir)): 
                    dataframes[region].to_root(path=root_file.replace(workdir, savedir), key=region, mode='w')
                else:
                    dataframes[region].to_root(path=root_file.replace(workdir, savedir), key=region, mode='a')
            count+=1
            
            
            print '%d Files processed' % count
            print '%d seconds elapsed' % (time() - start)
            print "========================================================="
        print 'Finishing adding pseudo-continuous scores.'
        print '%d files processed in %d seconds.' %(count,time()-start)

if __name__=='__main__':
    main()
