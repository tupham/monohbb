#!/usr/bin/env python
import os
from ROOT import TH1F, TFile
from argparse import ArgumentParser
import pandas as pd
from monohbbml.parameters import *
import uproot


def getArgs():
    """Get arguments from command line."""
    parser = ArgumentParser()
    parser.add_argument('-n', '--Saveas', action='store', required=True, help='Nice name for output text file.')
    parser.add_argument('--From', action='store',default='Preselected_samples',help='Directory of the root files')
    return  parser.parse_args()

def get_yield(sigs,bkgs,inputdir,verbose=True):
    yields={}
    windows=['total','mbb_100_140']
    for window in windows:
        yields[window]={}
        df_sig=sigs.set_index('category')
        df_bkg=bkgs.set_index('category')
        regions=list(df_sig.keys())
        for region in regions:
            for df in [df_sig,df_bkg]:
                for category in list(df.index):                  
                    for root in os.listdir(inputdir + '/' + category):
                        if 'empty' in root: continue
                        testkeys = [key.split(';')[0] for key in uproot.open(inputdir + '/' + category + '/' + root).keys()]
                        if 'Resolved' in region and 'Resolved' not in testkeys: continue
                        if 'Merged' in region and 'Merged' not in testkeys: continue
                        f=TFile(inputdir+'/'+category+'/'+root)
                        t=f.Get('Merged') if region == 'Merged' else f.Get('Resolved')
                        app_base=root.replace('.root','') # e.g. app_base=W_mc16a
                        xlow=50000 if window=='total' else 100000
                        xhigh=280000 if window=='total' else 140000
                        h=TH1F(app_base,app_base,40,xlow,xhigh)
                        var='m_J' if region=='Merged' else 'm_jj'
                        if '2HDMa' in category:
                            lumi=139
                        elif 'mc16a' in root:
                            lumi=36.1
                        elif 'mc16d' in root:
                            lumi=44.2
                        else:
                            lumi=58.4
                        if region=='Merged':
                            selection='weight*1000*'+str(lumi)+'*(MetTST_met>500000)'
                        elif region=='Resolved_350_500':
                            selection='weight*1000*'+str(lumi)+'*(MetTST_met<500000&&MetTST_met>350000)'
                        elif region=='Resolved_200_350':
                            selection='weight*1000*'+str(lumi)+'*(MetTST_met>200000&&MetTST_met<350000)'
                        else:
                            selection='weight*1000*'+str(lumi)+'*(MetTST_met<200000)'
                        t.Draw(var+'>>'+app_base,selection)
                        df.at[category,region]+=h.Integral()
        df_bkg.loc['bkg_tot']=df_bkg.apply(lambda x: np.sum(x),axis=0, result_type='expand')
        yields[window]['sig']=df_sig.reset_index()
        yields[window]['bkg'] = df_bkg.reset_index()
    if verbose:
        for window in yields.keys():
            print '\nYield in %s window' % window
            print yields[window]['sig']
            print yields[window]['bkg']
    return yields
        
def main():

    args = getArgs()
    name = args.Saveas
    inputdir = args.From
    
    if not '.h5' in name: name+='.h5'
    sigs = []
    for mass in ZP2HDM:
        sigs.append('zp2hdmbbmzp%smA%s'%(mass[0],mass[1]))

    for mass in A2HDM:
        MA = str(mass[0])
        Ma  = str(mass[1])
        for production in ['2HDMa_bb_tb10', '2HDMa_ggF_tb1']:
            sigs.append(production + '_sp035_mA'+ MA + '_ma' + Ma)

    sig_df=pd.DataFrame(dtype=float)
    sig_df['category']=sorted(sigs)
    bkgs = ['Z','W','ttbar','stop','VH','diboson']
    bkg_df=pd.DataFrame(dtype=float)
    bkg_df['category']=sorted(bkgs)
    for region in sorted(['Merged','Resolved_350_500','Resolved_200_350','Resolved_150_200']):
        sig_df[region]=0.
        bkg_df[region]=0.
            
    yields=get_yield(sig_df,bkg_df,inputdir)

    if not os.path.isdir('data/yields'):
        os.makedirs('data/yields')
    if os.path.isfile('data/yields/'+name): os.remove('data/yields/'+name)
    for window in yields.keys():
        for type in yields[window].keys():
            key=window + '/'+type
            yields[window][type].to_hdf('data/yields/'+name,key=key,mode='a')


if __name__ == '__main__':
    main()