#!/bin/bash
#########################################################################
#                                                                       #
#    The example wrapper for the training and categorization task.      #
#                                                                       #
#    Please contact ~jay.chan@cern.ch~ in case there is any issue.      #
#                                                                       #
#########################################################################

############################
#  Training the BDT models
############################
#python train_model.py --From Training_data_3Bjet --roc --tHDMa_bb 1400 150 1000 150 300 150 400 250 1100 250 --feature --train_bkg Z W ttbar # The three training signal points for resolved region (non-boosted region) are (1400, 600), (1000, 600) and (600, 300)
#python train_bdt.py -r r --tHDMa_ggF 300 150 1200 350 --save --roc  # Uncomment this line for training with the 2HDM+a signal points for resolved region. Here (300, 150) and (1200, 350) are used
#python train_bdt.py -r r --ZPtHDM 1400 600 1000 600 600 300 --tHDMa_ggF 300 150 1200 350 --save --roc # train mixed models together
#python train_bdt.py -r m_500_900 --ZPtHDM 2000 500 1800 600 1600 600 --save --roc # The BDT in the second merged region

#############################
#  Plot feature importance
#############################
#python plot_features.py -r r -m 1400 600 1000 600 600 300
#python plot_features.py -r m_500_900 -m 2000 500 1800 600 1600 600

###############################
#  Plot the BDT distribution
###############################
#python plot_score.py -r r --ZPtHDM 1400 600 1000 600 600 300 --zpthdm 1400 600 1000 600 600 300 --thdma_ggF 1200 350 300 150
#python plot_score.py -r m_500_900 --ZPtHDM 2000 500 1800 600 1600 600 --zpthdm 2000 500 1600 600 --thdma_ggF 1800 150 1600 350

###########################################################
#  Optimizing the BDT/MET boundaries for resolved/merged region
###########################################################
python categorization.py --From Training_data_3Bjet --region Resolved --tHDMa_bb 1400 150 1000 150 300 150 400 250 1100 250 --signalmodel ZP2HDM 1400 600 ZP2HDM 1000 600 ZP2HDM 600 300 # 4-step categorization with mixed models for the benchmark signals
#python categorization.py -r r --ZPtHDM 1400 600 1000 600 600 300 --signalmodel ZP2HDM ZP2HDM ZP2HDM -s 1400 600 1000 600 600 300 --skip # the original 3-step categoriation
#python categorization.py -r r --tHDMa_ggF 300 150 1200 350 --signalmodel 2HDMa_ggF 2HDMa_ggF 2HDMa_ggF -s 1200 350 600 350 300 150
#python categorization.py -r r --ZPtHDM 1400 600 1000 600 600 300 --signalmodel ZP2HDM 2HDMa_ggF ZP2HDM -s 1400 600 1200 350 1000 600 --skip
#python categorization.py -r m_500_900 --ZPtHDM 2000 500 1800 600 1600 600 --signalmodel 2HDMa_ggF ZP2HDM -s 1800 150 1600 600 # 2-step categorization in the second merged region
#python categorization_MET.py -r m_900 # This line is only for getting the significance without any further splitting

############################################
#  Plot the significance for each signal point
#############################################
#python plotCombine.py --ZPtHDM 1400 600 1000 600 600 300 --signalmodel ZP2HDM ZP2HDM ZP2HDM -s 1400 600 1000 600 600 300
#python plotCombine.py --ZPtHDM 1400 600 1000 600 600 300 --signalmodel ZP2HDM 2HDMa_ggF ZP2HDM 2HDMa_ggF -s 1400 600 1200 350 1000 600 300 150
#python plotCombine_2hdma.py -p ggF --ZPtHDM 1400 600 1000 600 600 300 --signalmodel ZP2HDM ZP2HDM ZP2HDM -s 1400 600 1000 600 600 300
#python plotCombine_2hdma.py -p ggF --ZPtHDM 1400 600 1000 600 600 300 --signalmodel ZP2HDM 2HDMa_ggF ZP2HDM 2HDMa_ggF -s 1400 600 1200 350 1000 600 300 150
#python plotCombine_2hdma.py -p bb --ZPtHDM 1400 600 1000 600 600 300 --signalmodel ZP2HDM ZP2HDM ZP2HDM -s 1400 600 1000 600 600 300
#python plotCombine_2hdma.py -p bb --ZPtHDM 1400 600 1000 600 600 300 --signalmodel ZP2HDM 2HDMa_ggF ZP2HDM 2HDMa_ggF -s 1400 600 1200 350 1000 600 300 150
