#!/usr/bin/env python
#
#
#
#  Created by Jay Chan
#
#  8.28.2018
#
#
#
#
#
import os
from ROOT import *
from argparse import ArgumentParser
from AtlasStyle import *

def getArgs():
    """Get arguments from command line."""
    parser = ArgumentParser()
    parser.add_argument('-r', '--region', action='store', choices=['Merged', 'Merged_500_900', 'Resolved'], default='Resolved', help='Region to process')
    parser.add_argument('--ZPtHDM', type=int, nargs='+', default = [], help='Masspoint of training ZP2HDM model samples.')
    parser.add_argument('--tHDMa_ggF', type=int, nargs='+', default = [], help='Masspoint of training ZP2HDM model samples.')
    parser.add_argument('--tHDMa_bb', type=int, nargs='+', default = [], help='Masspoint of training ZP2HDM model samples.')
    parser.add_argument('--zpthdm',type=int, nargs='+', default = [], help='Signal point to plot.')
    parser.add_argument('--thdma_ggF',type=int, nargs='+', default = [], help='Signal point to plot.')
    parser.add_argument('--thdma_bb',type=int, nargs='+', default = [], help='Signal point to plot.')
    return  parser.parse_args()

def SetStyle(hist,i, j=1):

    LineColor = [kRed, kGreen+2, kOrange+2, kBlue+2, kViolet]
    #LineStyle = [1, 2, 3, 4, 5]
    hist.Scale(1/hist.Integral())
    hist.SetLineColor(LineColor[i])
    hist.SetLineStyle(j)
    hist.SetLineWidth(4)
    hist.SetMarkerSize(0)

    return

def Leg(hists,x0,y1):

    n=len(hists)
    l=TLegend(x0,y1-n*0.05,x0+0.46,y1)
    l.SetFillColor(0)
    l.SetLineColor(1)
    l.SetTextSize(0.035)
    l.SetShadowColor(0)
    l.SetFillStyle(0)
    l.SetBorderSize(0)
    for category in hists:
        if not category == 'bkg' and 'zp2hdmbbmzp2' in category:
            name = category.replace('zp2hdmbb','')
            l.AddEntry(hists[category],name)
    for category in hists:
        if not category == 'bkg' and 'zp2hdmbbmzp1' in category:
            name = category.replace('zp2hdmbb','')
            l.AddEntry(hists[category],name)
    for category in hists:
        if not category == 'bkg' and not 'zp2hdmbbmzp1' in category and not 'zp2hdmbbmzp2' in category and 'zp2hdmbbmzp' in category:
            name = category.replace('zp2hdmbb','')
            l.AddEntry(hists[category],name)
    for category in hists:
        if not category == 'bkg' and not 'zp2hdmbbmzp' in category and '2HDMa_ggF_tb1_sp035_mA1' in category:
            name = category.replace('2HDMa_ggF_tb1_sp035_', '').replace('_', '')
            l.AddEntry(hists[category],name)
    for category in hists:
        if not category == 'bkg' and not 'zp2hdmbbmzp' in category and not '2HDMa_ggF_tb1_sp035_mA1' in category and '2HDMa_ggF_tb1_sp035_mA' in category:
            name = category.replace('2HDMa_ggF_tb1_sp035_', '').replace('_', '')
            l.AddEntry(hists[category],name)
    for category in hists:
        if not category == 'bkg' and not 'zp2hdmbbmzp' in category and not '2HDMa_ggF' in category and '2HDMa_bb_tb10_sp035_mA1' in category:
            name = category.replace('2HDMa_bb_tb10_sp035_', '').replace('_', '')
            l.AddEntry(hists[category],name)
    for category in hists:
        if not category == 'bkg' and not 'zp2hdmbbmzp' in category and not '2HDMa_ggF' in category and not '2HDMa_bb_tb10_sp035_mA1' in category and '2HDMa_bb_tb10_sp035_mA' in category:
            name = category.replace('2HDMa_bb_tb10_sp035_', '').replace('_', '')
            l.AddEntry(hists[category],name)
    l.AddEntry(hists['bkg'],'bkg')
    return l



def plot_var(sigs, bkgs, region, train_sig, branch, var, lend, rend):

    hist={}
    i=0
    for sig in sigs:
        hist[sig]=TH1F('hist_%s_%s'%(var,sig),'hist_%s_%s'%(var,sig),20,lend,rend)
        hist[sig].Sumw2()
    hist['bkg']=TH1F('hist_%s_bkg'%(var),'hist_%s_bkg'%(var),20,lend,rend)
    hist['bkg'].Sumw2()

    for sig in sigs:
        for app in os.listdir('outputs/model_%s%s'%(train_sig,region)):

            if not sig in app: continue
            f=TFile('outputs/model_%s%s/%s'%(train_sig,region,app))
            t=f.Get(region)

            htemp=TH1F('htemp','htemp',20,lend,rend)

            if 'mc16a' in app: lumi=36.1
            elif 'mc16d' in app: lumi=44.2
            elif 'mc16e' in app and '2HDMa' in app: lumi = 139 
            else: lumi = 58.4
            t.Draw("%s>>htemp"%(branch),'weight*%f'%(lumi))

            hist[sig]+=htemp

        SetStyle(hist[sig],i, 1 if 'zp2hdm' in sig else (2 if 'ggF' in sig else 3))

        i+=1

    for bkg in bkgs:
        for app in os.listdir('outputs/model_%s%s'%(train_sig,region)):

            if not bkg in app: continue

            f=TFile('outputs/model_%s%s/%s'%(train_sig,region,app))
            t=f.Get(region)

            htemp=TH1F('htemp','htemp',20,lend,rend)

            if 'mc16a' in app: lumi=36.1
            elif 'mc16d' in app: lumi=44.2
            elif 'mc16e' in app: lumi=58.4
            t.Draw("%s>>htemp"%(branch),'weight*%f'%(lumi))

            hist['bkg']+=htemp

    hist['bkg'].Scale(1/hist['bkg'].Integral())
    #hist_bkg_sid.SetFillColorAlpha(kBlue+3, 0.25)
    hist['bkg'].SetLineColor(kBlue)
    hist['bkg'].SetMarkerSize(0)
    hist['bkg'].SetLineWidth(4)
    #hist_bkg_sid.SetFillStyle(3554)

    C1=TCanvas()
    #C1.Range(0,0,1,1)
    C1.SetCanvasSize(400,370)
    C1.SetLeftMargin(0.135)
    C1.SetRightMargin(0.05) #0.05
    C1.SetTopMargin(0.05)
    C1.SetBottomMargin(0.135)

    THSvar=THStack(var,'')

    for category in hist:
        THSvar.Add(hist[category],"hist E1")

    THSvar.Draw("Nostack")

    max_value = THSvar.GetMaximum('NoStack')
    hist['bkg'].GetYaxis().SetRangeUser(0, max_value * 1.4)

    AtlasLabel.ATLASLabel(0.17,0.88," Internal")
    AtlasLabel.myText(0.17,0.88-0.063,"#sqrt{s} = 13 TeV, 140 fb^{-1}")

    THSvar.GetXaxis().SetTitle("%s%s"%(var,' [GeV]' if '_pt' in branch or '_E' in branch or 'met' in branch else ''))
    #jet1_pt.GetXaxis().SetTitleOffset(1.5)
    THSvar.GetYaxis().SetTitle("1/N dN/dX")
    #jet1_pt.GetYaxis().SetTitleOffset(1.5)

    l=Leg(hist,0.56,0.93)
    l.Draw("SAME")

    if not os.path.isdir('bdt_plots'):
        os.makedirs('bdt_plots')
    C1.Print('bdt_plots/model_%s%s.pdf'%(train_sig,region))

    return


def main():

    gROOT.SetBatch(True)

    args=getArgs()

    region = args.region

    bkgs=['Z','W','ttbar']
    sigs=[]
    for i in range(0,len(args.zpthdm),2):
        sig='zp2hdmbbmzp%smA%s'%(args.zpthdm[i],args.zpthdm[i+1])
        sigs.append(sig)

    for i in range(0,len(args.thdma_ggF),2):
        sig='2HDMa_ggF_tb1_sp035_mA%s_ma%s'%(args.thdma_ggF[i],args.thdma_ggF[i+1])
        sigs.append(sig)

    for i in range(0,len(args.thdma_bb),2):
        sig='2HDMa_bb_tb10_sp035_mA%s_ma%s'%(args.thdma_bb[i],args.thdma_bb[i+1])
        sigs.append(sig)

    train_sig=''
    for i in range(0,len(args.ZPtHDM),2):
        train_sig += 'zp2hdmbbmzp%smA%s_'%(args.ZPtHDM[i],args.ZPtHDM[i+1])

    for i in range(0,len(args.tHDMa_ggF),2):
        train_sig += '2HDMa_ggF_tb1_sp035_mA%s_ma%s_'%(args.tHDMa_ggF[i],args.tHDMa_ggF[i+1])

    for i in range(0,len(args.tHDMa_bb),2):
        train_sig += '2HDMa_bb_tb10_sp035_mA%s_ma%s_'%(args.tHDMa_bb[i],args.tHDMa_bb[i+1])

    #plotVars(bkgs+sigs,region,train_sig)
    plot_var(sigs, bkgs, region, train_sig, 'BDT_score', 'O_{BDT}', 0, 1)

    return

if __name__ == '__main__':
    main()
