#!/usr/bin/env python
#
#
#
#  Created by Jay Chan
#
#  8.28.2018

import os
from ROOT import TFile, TH1F, TCanvas, gStyle, THStack, kRed, kGreen, kBlue, kOrange, TLegend
from argparse import ArgumentParser
from AtlasStyle import *

def getArgs():
    """Get arguments from command line."""
    parser = ArgumentParser()
    parser.add_argument('-r', '--region', action='store', choices=['m', 'r'], default='r', help='Region to process')
    # the mass points that go into the current bdt are [1400 600 1000 600 600 300]
    parser.add_argument('-m','--masspoint', type=int, nargs='*', default=[2600,300], help='Masspoint of training ZP2HDM model samples.')
    #parser.add_argument('-s','--signal',type=int, nargs='*',help='Signal point to plot.')
    # get the boundaries of the bdt, the highest contains the events that maximise the sensitivity
    parser.add_argument('-b','--boundaries',type=float,nargs=2,required=True,help='Boundaries')
    parser.add_argument('--MT',action='store_true',default=False,help='plot mT')
    return  parser.parse_args()

def SetStyle(hist,i):

    FillColor=[0,0,0,0,0,0,0]
    LineColor=[kGreen+3,kBlue,kBlue+3,kRed+2,kOrange,kGreen,1]
    #hist.SetFillColorAlpha(FillColor[i],1)
    hist.SetLineColor(LineColor[i])
    hist.SetMarkerSize(0)
    if i==2: hist.SetFillStyle(3554)

    return

def Leg(hists,x0,y0):

    l=TLegend(x0,y0,x0+0.46,y0+0.25)
    l.SetFillColor(0)
    l.SetLineColor(1)
    l.SetTextSize(0.035)
    l.SetShadowColor(0)
    l.SetFillStyle(0)
    l.SetBorderSize(0)
    for category in hists:
        l.AddEntry(hists[category],category)
    return l

def plot_var(categories,region,train_sig,branch,var,nbin,lend,rend,legx0,legy0,boundaries):

    hist={}
    exist={}
    i=0

    # given the list of categories, which is just to specify the folder to look for data files
    for category in categories:
        hist[category]=TH1F('hist_%s_%s'%(var,category),'hist_%s_%s'%(var,category),nbin,lend,rend)

    for category in categories:
        exist[category]=False
        for app in os.listdir('outputs/model_%s%s/%s'%(train_sig,region,category)):

            exist[category]=True

            f=TFile('outputs/model_%s%s/%s/%s'%(train_sig,region,category,app))
            t=f.Get('test')

            htemp=TH1F('htemp','htemp',nbin,lend,rend)

            if 'mc16a' in app: lumi=36.1
            elif 'mc16d' in app: lumi=43.7
            t.Draw("%s>>htemp"%(branch),"weight*(eventNumber%%100>=0&&bdt_score>=%f&&bdt_score<%f)*1000*%f"%(boundaries[0],boundaries[1],lumi))


            hist[category]+=htemp
        print category, hist[category].GetEntries()
        SetStyle(hist[category],i)

        i+=1

    C1=TCanvas()
    #C1.Range(0,0,1,1)
    C1.SetCanvasSize(500,370)
    C1.SetLeftMargin(0.135)
    C1.SetRightMargin(0.05)
    C1.SetTopMargin(0.05)
    C1.SetBottomMargin(0.135)

    THSvar=THStack(var,'')

    for category in categories:
        if not exist[category]: continue
        if hist[category].Integral() != 0:
            #hist[category].Scale(1/hist[category].Integral())
            print var

            THSvar.Add(hist[category],"HIST")

    THSvar.Draw('nostack')
    THSvar.GetXaxis().SetTitle(var)
    #jet1_pt.GetXaxis().SetTitleOffset(1.5)
    THSvar.GetYaxis().SetTitle("dN/dX")
    THSvar.SetMaximum(1.3*THSvar.GetMaximum())
    #jet1_pt.GetYaxis().SetTitleOffset(1.5)
    AtlasLabel.ATLASLabel(legx0,legy0+0.27," Internal")
    AtlasLabel.myText(legx0,legy0-0.063+0.27,"#sqrt{s} = 13 TeV, 79.8 fb^{-1}")

    l=Leg(hist,legx0,legy0-0.067)
    l.Draw("SAME")


    if not os.path.isdir('mass_plots'):
        os.makedirs('mass_plots')
    if not os.path.isdir('mass_plots/model_%s%s'%(train_sig,region)):
        os.makedirs('mass_plots/model_%s%s'%(train_sig,region))
    C1.Print('mass_plots/model_%s%s/%s_%s_%s.pdf'%(train_sig,region,var,round(boundaries[0],2),round(boundaries[1],2)))

    return


def main():

    args=getArgs()

    if args.region == 'm':
        region = 'merged'
    elif args.region == 'r':
        region = 'resolved'

    bkgs=['Z','W','ttbar','diboson','stop','VH']
    #sigs=[]
    #if args.signal:
        #for i in range(0,len(args.signal),2):
            #sig='zp2hdmbbmzp%smA%s'%(args.signal[i],args.signal[i+1])
            #sigs.append(sig)

    # defining the training signals, concatenate the mass points from argument.
    train_sig=''
    for i in range(0,len(args.masspoint),2):
        train_sig += 'zp2hdmbbmzp%smA%s_'%(args.masspoint[i],args.masspoint[i+1])

    #plotVars(bkgs+sigs,region,train_sig)
    if region == 'merged':
        var='m_J'
        branch='m_J/1000'
        maxmbb=270
    else:
        var = 'm_jj' #getruwqerg
        branch = 'm_jj/1000'
        maxmbb=280
    plot_var(bkgs,region,train_sig,branch,var,30,50,maxmbb,.65,.63,args.boundaries)
    plot_var(bkgs,region,train_sig,'MetTST_met/1000','MET',2000/50,0,2000,.65,.63,args.boundaries)
    if args.MT:
        plot_var(bkgs,region,train_sig,'MT/1000','MT',3000/100,0,3000,.65,.63,args.boundaries)

    return

if __name__ == '__main__':
    main()
