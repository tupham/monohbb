#!/usr/bin/env python
#
#
#
#  Created by Jay Chan
#
#  8.28.2018
#
#
#
#
#
import os
from ROOT import TH2F, TCanvas, gStyle, gROOT
from argparse import ArgumentParser
from AtlasStyle import *
from math import sqrt

def getArgs():
    parser = ArgumentParser()
    parser.add_argument('--ZPtHDM', type=int, nargs='+', default=[1400,600,1000,600,600,300], help='Masspoint of training ZP2HDM model samples.')
    parser.add_argument('--tHDMa_ggF', type=int, nargs='+', default=[],
                        help='Masspoint of training 2HDM+a model samples (with ggF production).')
    parser.add_argument('--tHDMa_bb', type=int, nargs='+', default=[],
                        help='Masspoint of training 2HDM+a model samples (with bb production).')
    parser.add_argument('--signalmodel', action='store', nargs='+', default=['ZP2HDM', '2HDMa_ggF', 'ZP2HDM', '2HDMa_ggF'],
                        help="targeted signal models: 'ZP2HDM', '2HDMa_ggF', '2HDMa_bb'")
    parser.add_argument('-s','--signalmasspoint', type=int, nargs='+', default=[1400, 600, 1200, 350 ,1000 ,600 ,300 ,150],
                        help='Masspoint of specified signal model mass point of ZP2HDM.')
    return parser.parse_args()


def main():
    args=getArgs()
    ZPtHDM, tHDMa_ggF, tHDMa_bb, signalmodel, signalmasspoint = args.ZPtHDM, args.tHDMa_ggF, args.tHDMa_bb, args.signalmodel, args.signalmasspoint

    if len(signalmasspoint)/2!=len(signalmodel):
        raise SystemExit('Number of signal masspoints, %d, does not match number of signal models, %d' % (len(signalmasspoint)/2,len(signalmodel)))

    gROOT.SetBatch(True)

    impplot = TH2F('impplot', 'impplot', 14,300., 3100., 7, 150., 850.)
    impplot_r = TH2F('impplot', 'impplot', 14, 300.,3100., 7, 150., 850.)
    impplot_m = TH2F('impplot', 'impplot', 14, 300.,3100., 7, 150., 850.)
    sigplot = TH2F('impplot', 'impplot', 14, 300.,3100., 7, 150., 850.)

    #model='model_zp2hdmbbmzp2600mA300_zp2hdmbbmzp2200mA600_zp2hdmbbmzp1600mA600_merged'
    #model='model_zp2hdmbbmzp2600mA300_merged'
    #catmethod='targetsig_zp2hdmbbmzp2600mA300'
    #catmethod='targetsig_zp2hdmbbmzp2600mA300_zp2hdmbbmzp2200mA600_zp2hdmbbmzp1600mA600'
    #sigtxt = open('significances/'+model+'/'+catmethod+'.txt','r')
    sigtxt = open('significances/MET_merged_900.txt','r')
    lines = sigtxt.read().splitlines()

    sig_m={}

    for line in lines:
        if not 'zp2hdmbbmzp' in line: continue
        info=str(line).split()
        name=info[0]
        mZP=name.replace('zp2hdmbbmzp','').split('mA')[0]
        mA=name.replace('zp2hdmbbmzp','').split('mA')[1]
        sig_m[name]=[float(info[1]),float(info[3])]

    model='model_'
    for i in range(0,len(ZPtHDM),2):
        model+='zp2hdmbbmzp%dmA%d_' % (ZPtHDM[i],ZPtHDM[i+1])
    model+='resolved'
    print 'Trained model: ', model ,'\n'
    catmethod='targetsig'
    for i in range(len(signalmodel)):
        if signalmodel[i] == 'ZP2HDM':
            catmethod+='_zp2hdmbbmzp%dmA%d' % (signalmasspoint[2*i],signalmasspoint[2*i+1])
        elif signalmodel[i] == '2HDMa_ggF':
            catmethod+='_2HDMa_ggF_tb1_sp035_mA%d_ma%d' % (signalmasspoint[2*i],signalmasspoint[2*i+1])
        else:
            catmethod += '_2HDMa_bb_tb1_sp035_mA%d_ma%d' % (signalmasspoint[2 * i], signalmasspoint[2 * i + 1])
    print 'Categorization method: ', catmethod, '\n'
    #model='model_zp2hdmbbmzp1400mA600_zp2hdmbbmzp1000mA600_zp2hdmbbmzp600mA300_resolved'
    #model='model_2HDMa_ggF_tb1_sp035_mA300_ma150_2HDMa_ggF_tb1_sp035_mA1200_ma350_resolved'
    #catmethod='targetsig_zp2hdmbbmzp1400mA600_zp2hdmbbmzp1000mA600_zp2hdmbbmzp600mA300'
    #catmethod='targetsig_2HDMa_ggF_tb1_sp035_mA1200_ma350_2HDMa_ggF_tb1_sp035_mA600_ma350_2HDMa_ggF_tb1_sp035_mA300_ma150'
    #catmethod='targetsig_zp2hdmbbmzp1400mA600_2HDMa_ggF_tb1_sp035_mA1200_ma350_zp2hdmbbmzp1000mA600'
    #catmethod='targetsig_zp2hdmbbmzp1400mA600_2HDMa_ggF_tb1_sp035_mA1200_ma350_zp2hdmbbmzp1000mA600_2HDMa_ggF_tb1_sp035_mA300_ma150'
    sigtxt = open('significances/'+model+'/'+catmethod+'.txt','r')
    lines = sigtxt.read().splitlines()

    sig_r={}

    for line in lines:
        if not 'zp2hdmbbmzp' in line: continue
        info=str(line).split()
        name=info[0]
        mZP=name.replace('zp2hdmbbmzp','').split('mA')[0]
        mA=name.replace('zp2hdmbbmzp','').split('mA')[1]
        sig_r[name]=[float(info[1]),float(info[3])]

    sig_com={}
    for name in sig_m:
        sig_com[name]=[sqrt(sig_m[name][0]**2+sig_r[name][0]**2),sqrt(sig_m[name][1]**2+sig_r[name][1]**2)]


    txt_com=open('significances/ZPtHDM_ratio_combined.txt','w')
    txt_com.write('Name Ratio\n')
    txt_m=open('significances/ZPtHDM_ratio_merged.txt','w')
    txt_m.write('Name Ratio\n')
    txt_r = open('significances/ZPtHDM_ratio_resolved.txt','w')
    txt_r.write('Name Ratio\n')
    for name in sig_com:
        mZP=name.replace('zp2hdmbbmzp','').split('mA')[0]
        mA=name.replace('zp2hdmbbmzp','').split('mA')[1]
        mZP=float(mZP)
        mA=float(mA)
        significance=float(sig_com[name][0])
        baseline=float(sig_com[name][1])
        improvement=significance/baseline
        txt_com.write(name+' '+str(improvement)+'\n')
        improvement_r = float(sig_r[name][0])/float(sig_r[name][1])
        txt_r.write(name + ' ' + str(improvement_r)+'\n')
        improvement_m = float(sig_m[name][0])/float(sig_m[name][1])
        txt_m.write(name + ' ' + str(improvement_m)+'\n')

        impplot.Fill(mZP,mA,improvement)
        impplot_r.Fill(mZP,mA,improvement_r)
        impplot_m.Fill(mZP,mA,improvement_m)
        sigplot.Fill(mZP,mA,significance)

    if not os.path.isdir('sigplots'):
        print 'INFO: Creating new folder: \"sigplots\"'
        os.makedirs("sigplots")


    C2=TCanvas()
    #C2.Range(0,0,1,1)
    C2.SetCanvasSize(550,395);
    C2.SetLeftMargin(0.135);
    C2.SetRightMargin(0.154);
    C2.SetTopMargin(0.15);
    #C1.SetBottomMargin(0.135);
    gStyle.SetPaintTextFormat("4.2f")
    impplot.Draw("COLZTEXT")
    impplot.GetXaxis().SetTitle("m_{Z'} [GeV]")
    impplot.GetYaxis().SetTitle("m_{A} [GeV]")
    impplot.GetZaxis().SetTitle("Significance Ratio BDT / Baseline")
    impplot.SetAxisRange(0., 4.,"Z")
    AtlasLabel.ATLASLabel(0.135,0.95," Internal")
    AtlasLabel.myText(0.37,0.95,"#sqrt{s} = 13 TeV, 139 fb^{-1}")
    AtlasLabel.myText(0.135,0.88,"Z'2HDM, 2 b-tags, 100 GeV < m_{bb} < 140 GeV")
    C2.Print("sigplots/ZP2HDM_imp.pdf")

    impplot_r.Draw("COLZTEXT")
    impplot_r.GetXaxis().SetTitle("m_{Z'} [GeV]")
    impplot_r.GetYaxis().SetTitle("m_{A} [GeV]")
    impplot_r.GetZaxis().SetTitle("Significance Ratio BDT / Baseline")
    impplot_r.SetAxisRange(0., 4.,"Z")
    AtlasLabel.ATLASLabel(0.135,0.95," Internal")
    AtlasLabel.myText(0.37,0.95,"#sqrt{s} = 13 TeV, 139 fb^{-1}")
    AtlasLabel.myText(0.135,0.88,"Z'2HDM, 2 b-tags resolved, 100 GeV < m_{bb} < 140 GeV")
    C2.Print("sigplots/ZP2HDM_Resolved_imp.pdf")

    impplot_m.Draw("COLZTEXT")
    impplot_m.GetXaxis().SetTitle("m_{Z'} [GeV]")
    impplot_m.GetYaxis().SetTitle("m_{A} [GeV]")
    impplot_m.GetZaxis().SetTitle("Significance Ratio BDT / Baseline")
    impplot_m.SetAxisRange(0., 4.,"Z")
    AtlasLabel.ATLASLabel(0.135,0.95," Internal")
    AtlasLabel.myText(0.37,0.95,"#sqrt{s} = 13 TeV, 139 fb^{-1}")
    AtlasLabel.myText(0.135,0.88,"Z'2HDM, 2 b-tags merged, 100 GeV < m_{bb} < 140 GeV")
    C2.Print("sigplots/ZP2HDM_Merged_imp.pdf")


    sigplot.Draw("COLZTEXT")
    sigplot.GetXaxis().SetTitle("m_{Z'} [GeV]")
    sigplot.GetYaxis().SetTitle("m_{A} [GeV]")
    sigplot.GetZaxis().SetTitle("Significance with BDT")
    sigplot.SetAxisRange(0., 10.,"Z")
    AtlasLabel.ATLASLabel(0.135,0.95," Internal")
    AtlasLabel.myText(0.37,0.95,"#sqrt{s} = 13 TeV, 139 fb^{-1}")
    AtlasLabel.myText(0.135,0.88,"Z'2HDM, 2 b-tags, 100 GeV < m_{bb} < 140 GeV")
    C2.Print("sigplots/ZP2HDM_sig.pdf")


    return

if __name__ == '__main__':
    main()
